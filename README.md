<pre style="line-height: 1.2;">


</pre>
Deep learning method for inferring GRNs
================================================================================================

## Summary

<p align="center"><img src="figs/nn_GRN_TFA_modeling_v6.svg" width="80%" /></p>

Install latest version by cloning this repository
```
git clone https://gitlab.com/Xparx/supirfactor.git
cd supirfactor
```
and then in the supirfactor directory:
```
pip install -e .
```

## Usage
<mark>Will be subject to change.</mark>

First loading the required libraries
```
from supirfactor.setup import dataloader, prior as setup_prior, run as setup_run
import supirfactor.run as sf_run
from supirfactor import erv
import supirfactor.postprocess as sf_post
```

SupirFactor requires a dataset with gene columns and samples rows
and a prior structure, a matrix with transcription factor (TF)
columns and genes rows.

An [AnnData](https://anndata.readthedocs.io) object works well for loading the data.
```
data_file = 'path/to/data_file.h5ad'
adata = dataloader.load_expression_data(data_file)
```
To load the prior we use the `adata` to determine what features are included.
```
prior_file = 'path/to/prior.tsv'
prior, __ = setup_prior.prior_loader(prior_file, gene_names=adata.var_names.tolist())
```

If a gold standard of interaction exists it can also be used,
```
import pandas as pd
gold_standard = pd.read_csv(gold_standard_file, sep='\t', index_col=0)
```

Next, we setup the prior. If we want to run an evaluation of our
prior and benchmark quality we hold some of the genes out of the
prior (`cv_prior = True`).
```
cv_prior = False
use_prior, use_gs, use_prior_map = setup_prior.split(prior, gold_standard=gold_standard, cv_prior=cv_prior, fraction_gs=0.2)
```

Setting up the model,
```
model_class = 'Hierarchical'
model = setup_model(use_prior, model_class=model_class, device='cuda', pi_structure=[128])
```

SupirFactor has both a Hierarchical and a Shallow `model_class`. The
only difference is the default settings.
The Shallow model will not use an activation function by default otherwise `ReLU` is used.
The depth of the model is determined by the variable `pi_structure`
which is a list of integers determining the depth and with of the bottleneck layers.

Setup run configurations;
```
run_configs = setup_run.defaults(model)
```

Setup the data. If evaluating reconstruction performance `validation_size` should be >0.
```
sample_sets = dataloader.split_samples(adata, validation_size=0.1)
```

and setting up the dataloaders
```
randomize_data = False
preprocessor=None
loaders = dataloader.setup_loaders(adata, batch_size=512, **sample_sets, randomize_data=randomize_data, verbose=True)
```
by default SupirFactor will use the `RobustMinScaler` feature normalization.

Finally we can train the model
```
tr_loss = sf_run.fit(model, loaders[0], epochs=200, validation_loader=loaders[1], verbose=True, **run_configs)
```

To look at the loss
```
tr_loss = sf_post.loss2df(tr_loss)
__ = tr_loss.plot.scatter(x='epoch', y='R2')
__ = tr_loss.plot.scatter(x='epoch', y='trl')
__ = tr_loss.plot.scatter(x='epoch', y='vl')
```

To store the model and the prior used which will help with determine gene inputs and TF identities.
```
data_path = './my_data/directory/'
output_dir = os.path.join(data_path, 'supirfactor_model')
sf_post.save_model(model, output_dir=output_dir, dataframes={'prior_used': use_prior})
# sf_post.save_model(swa_model, output_dir=output_dir)  # if swa model is used.
```
The model will be saved with the date and hash defining the file
name. The above command should print the path created `date_from_stored`.

To load the model;
```
model_path = os.path.join(output_path, date_from_stored)
model = sf_post.load_model(os.path.join(model_path, 'Hierarchical_model_{model_hash}.pt'), device='cuda')
# swa_model = sf_post.load_model(os.path.join(model_path, 'SWAG_Hierarchical_model_{model_hash}.pt'), device='cuda') # if swa model is used.
prior_used = pd.read_csv(os.path.join(model_path, 'prior_used.tsv'), sep='\t', index_col=0)
```

To generate a representation of the GRN we use the `erv` module
```
model_erv = erv.ERV(model, loaders[1])
```

to determine what layers in the model we want to query we can look at the `model_erv`
```
model_erv._valid_keys
```

the output will depend on the `pi_structure` variable set above.
Here we chose an auto-encoder with an extra layer for bottleneck TFs extending the valid_keys to include `W`, `0` and `Theta`.
To get the network we want to query `0` in this case as these are defined by input layer.
```
grn = model_erv['0_erv_df']
```

`grn` is a pandas DataFrame with TF columns and gene rows. To annotate them we use our prior and data.
```
grn.columns = use_prior.columns
grn.index = adata.var_names
```

Importance of TF -> gene interactions will be determined by the
ERV values `]0, 1]` with higher meaning more influential for
predicting the target.

### Compute TFA

We can also compute transcription factor activity estimates for any
given dataset.  The same preprocessing should be supplied here as
when the data was setup for the run above.  The latent activity
(LA) can be computed for any layer in the model. Here we compute
the LA on the output of the initial embedding,
i.e. the TFs generated by mapping through `W`.
```
LA = sf_post.generate_latent_embedding(model, adata, batch_size=512, return_nodes={'W': 'TFA'})
```

to access and annotate it we do,
```
TFA = sf_post.to_anndata(LA['TFA'], var_names=use_prior.columns.tolist(), obsm=adata.obsm, uns=adata.uns, obs=adata.obs)
```

Now we can use the same embedding as we have for our original
`Anndata` object or use the standard approaches in
[scanpy](https://scanpy.readthedocs.io) to visualize TF activities
and compare it to the corresponding gene if it is available in the original data.

```
basis = 'umap'
tf_list = ['TF1', 'TF2']
__ = sc.pl.embedding(TFA, color=tf_list, cmap='plasma')
__ = sc.pl.embedding(adata, color=tf_list, cmap='viridis')
```
