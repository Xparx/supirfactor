from setuptools import setup

DISTNAME = 'SupirFactor'
VERSION = '0.20'
DESCRIPTION = "Using deep learning to infer gene regulatory network structure."
# with open('README.rst') as f:
#     LONG_DESCRIPTION = f.read()
MAINTAINER = 'Andreas Tjarnberg'
MAINTAINER_EMAIL = 'andreas.tjarnberg@fripost.org'
URL = 'https://gitlab.com/Xparx/supirfactor'
DOWNLOAD_URL = ''
LICENSE = 'LGPL'


setup(name=DISTNAME,
      version=VERSION,
      description=DESCRIPTION,
      url=URL,
      author=MAINTAINER,
      author_email=MAINTAINER_EMAIL,
      license=LICENSE,
      packages=['supirfactor'],
      python_requires='>=3.7',
      install_requires=[
          'torch>=2',
          'torchvision',
          'gpytorch',
          'scipy',
          'scikit-learn',
          'inferelator',
          'pandas',
          'scanpy',
          'matplotlib',
          'seaborn',
          'configobj',
      ],
      zip_safe=False)
