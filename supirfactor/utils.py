import numpy as _np
import pandas as _pd
import torch as _torch
from torch import nn as _nn
import time as _time
import sklearn.utils.sparsefuncs as _sparsefuncs


def linear_lsp(LSP_df, **kwargs):
    def linear_function(x, k=0.25, m=0.075):
        return k*x + m # Example: y = 2x + 1

    LSP_df['LSP_bound'] = LSP_df['expected_density'].apply(linear_function, **kwargs)
    return LSP_df


def rolling_lsp(LSP_df, window=100, min_periods=0, std_factor=1):
    rolling_lsp = LSP_df.sort_values('expected_density')['LSP_score'].rolling(window, min_periods=min_periods).apply(_np.median)
    LSP_df['median_LSP'] = rolling_lsp
    rolling_lsp = LSP_df.sort_values('expected_density')['LSP_score'].rolling(window, min_periods=min_periods).mean()
    LSP_df['mean_LSP'] = rolling_lsp
    rolling_lsp = LSP_df.sort_values('expected_density')['LSP_score'].rolling(window, min_periods=min_periods).std()
    LSP_df['std_LSP'] = rolling_lsp

    LSP_df['LSP_bound'] = LSP_df['median_LSP'] + LSP_df['std_LSP']*std_factor
    # LSP_df = LSP_df[(LSP_df['LSP_score'] > LSP_df['bound_LSP'])]
    return LSP_df


def sparse2sparse(X):
    from torch_sparse import SparseTensor as _ST  # , SparseStorage as _SS
    from torch import tensor as _tensor

    X = X.tocoo()

    return _ST(row=_tensor(X.row, dtype=_torch.long),
               col=_tensor(X.col, dtype=_torch.long),
               value=_tensor(X.data, dtype=_torch.float, requires_grad=False))


def shift2median(X):

    from scipy.sparse import vstack

    X = X.tocsc().copy()
    medians = _sparsefuncs.csc_median_axis_0(X)

    X = X.T.copy()

    Xshifted = []
    for c, m in zip(X, medians):
        if m > 0:
            c.data = c.data - m
            c.data = c.data * (c.data > 0)
        Xshifted.append(c)

    Xshifted = vstack(Xshifted).T.tocsr()
    Xshifted.eliminate_zeros()

    return Xshifted


def mem_efficient_density_estimate(adata, step=1000, **kwargs):
    from tqdm import tqdm

    all_vars = []
    maxi = adata.shape[1]
    for pset in tqdm(_np.arange(0, maxi, step, dtype=int)):
        tmp = adata[:, pset:min([pset + step, maxi])]
        var = add_density_estimate(tmp, copy=True, **kwargs)
        all_vars.append(var)

    all_vars = _pd.concat(all_vars)
    adata.var[all_vars.columns] = all_vars


def add_density_estimate(adata,
                         layers='counts',
                         th=0,
                         distance_key='',
                         median_shifted=False,
                         mtmethod='hs',
                         copy=False,
                         ):

    """
    links:
    - https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6781572
    """

    from scipy.stats import binomtest
    from statsmodels.stats.multitest import multipletests
    import warnings as _warnings
    try:
        from sparse_dot_mkl.sparse_dot import dot_product_mkl as _dot_product
    except ImportError:
        # _warnings.filterwarnings("ignore")

        def _dot_product(A, B, **kwargs):
            return A @ B

        _warnings.warn("Couldn't find the intel math kernel library (MKL).\nThis may slow down computations significantly\nMake sure to add the mkl libraries to the LD_LIBRARY_PATH to avoid slowing down computations.")

    _np.seterr(divide='ignore')
    dk = distance_key
    if distance_key and ~distance_key.endswith('_'):
        distance_key = distance_key + '_'

    distance_key = distance_key + 'distances'

    if (layers is None) or (layers == 'X'):
        Xth = adata.X.copy() if copy else adata.X
    else:
        Xth = adata.layers[layers].copy() if copy else adata.layers[layers]

    # A = (adata.obsp[distance_key].astype(bool).astype(int) @ (X > th).astype(int))
    # B = A.multiply((X > th).astype(int))
    # adata.var['density'] = B.sum(0).A1

    # Boolean matrix where genes are expressed.
    Xth.data = (Xth.data > th).astype(int)  # generates a 0,1 matrix.
    Xth.eliminate_zeros()

    if median_shifted:
        Xth = shift2median(Xth)

    # print('Starting density estimates.', flush=True)
    # Count matrix projecting Boolean expression onto Neigbours.
    A = _dot_product(adata.obsp[distance_key].astype(bool).astype(int), Xth, cast=True)

    # print('computed dotproduct.', flush=True)
    var = None
    if not copy:
        # Only count if neigbour cell had expression.
        ifk_density = [i.multiply(j).sum() for i, j in zip(A.T, Xth.T)]
        adata.var['ifk_density'] = ifk_density
        # adata.var['ifk_density'] = A.multiply(Xth).sum(0).A1

        # Total neighbors count for all cells.
        adata.var['knn_density'] = A.sum(0).A1

        # Regular cell count of expression.
        adata.var['N_dense'] = Xth.sum(0).A1

        k = (adata.uns['neighbors' if not dk else dk.replace('distances', '')]['params']['n_neighbors'] - 1)
        CC = (adata.var['ifk_density']) / (adata.var['N_dense'] * k)

        # Count if neighbour cell had expression normalized by
        # if all cells where expressd in the neighborhood.
        adata.var['density_norm'] = CC

        # Background expected density per gene.
        adata.var['expected_density'] = adata.var['N_dense'] / adata.shape[0]

        # Difference in relative local density to gene background density.
        adata.var['LSP_score'] = (adata.var['ifk_density'] / adata.var['knn_density']) - adata.var['expected_density']

        # Relative local density.
        adata.var['local_density'] = adata.var['ifk_density'] / adata.var['knn_density']

        adata.var['density_norm'] = adata.var['density_norm'].fillna(0)
        adata.var['expected_density'] = adata.var['expected_density'].fillna(0)

        stats = adata.var.apply(lambda x: binomtest((_np.round(x['density_norm'] * k)).astype(int),
                                                    # int(k),
                                                    int(k + 1),  # Maybe needed? This seems to happen when I specify n_pcs...
                                                    x['expected_density'],
                                                    alternative='greater'),
                                axis=1)

        adata.var['density_p'] = stats.apply(lambda x: x.pvalue)
        adata.var['density_q'] = multipletests(adata.var['density_p'].values, method=mtmethod)[1]
        adata.var['density_logp'] = -_np.log10(adata.var['density_p'])
        # return multipletests(adata.var['density_p'].values, method=mtmethod)[1]
        # adata.var['dens_ci'] = stats.apply(lambda x: x.)
    else:
        var = adata.var.copy()

        ifk_density = [i.multiply(j).sum() for i, j in zip(A.T, Xth.T)]
        var['ifk_density'] = ifk_density
        # adata.var['ifk_density'] = A.multiply(Xth).sum(0).A1

        # Total neighbors count for all cells.
        var['knn_density'] = A.sum(0).A1

        # Regular cell count of expression.
        var['N_dense'] = Xth.sum(0).A1

        k = (adata.uns['neighbors' if not dk else dk.replace('distances', '')]['params']['n_neighbors'] - 1)
        CC = (var['ifk_density']) / (var['N_dense'] * k)

        # Count if neighbour cell had expression normalized by
        # if all cells where expressd in the neighborhood.
        var['density_norm'] = CC

        # Background expected density per gene.
        var['expected_density'] = var['N_dense'] / adata.shape[0]

        # Difference in relative local density to gene background density.
        var['LSP_score'] = (var['ifk_density'] / var['knn_density']) - var['expected_density']

        # Relative local density.
        var['local_density'] = var['ifk_density'] / var['knn_density']

        var['density_norm'] = var['density_norm'].fillna(0)
        var['expected_density'] = var['expected_density'].fillna(0)

        stats = var.apply(lambda x: binomtest((_np.round(x['density_norm'] * k)).astype(int),
                                              # int(k),
                                              int(k + 1),  # Maybe needed?
                                              x['expected_density'],
                                              alternative='greater'),
                          axis=1)

        var['density_p'] = stats.apply(lambda x: x.pvalue)
        var['density_q'] = multipletests(var['density_p'].values, method=mtmethod)[1]
        var['density_logp'] = -_np.log10(var['density_p'])

    _np.seterr(divide='ignore')
    return var


def optimize_linear(grad, eps, norm=_np.inf):
    """
    Taken from: https://github.com/cleverhans-lab/cleverhans.

    Solves for the optimal input to a linear function under a norm constraint.
    Optimal_perturbation = argmax_{eta, ||eta||_{norm} < eps} dot(eta, grad)
    :param grad: Tensor, shape (N, d_1, ...). Batch of gradients
    :param eps: float. Scalar specifying size of constraint region
    :param norm: np.inf, 1, or 2. Order of norm constraint.
    :returns: Tensor, shape (N, d_1, ...). Optimal perturbation
    """

    red_ind = list(range(1, len(grad.size())))
    avoid_zero_div = _torch.tensor(1e-12, dtype=grad.dtype, device=grad.device)
    if norm == _np.inf:
        # Take sign of gradient
        optimal_perturbation = _torch.sign(grad)
    elif norm == 1:
        abs_grad = _torch.abs(grad)
        sign = _torch.sign(grad)
        red_ind = list(range(1, len(grad.size())))
        abs_grad = _torch.abs(grad)
        ori_shape = [1] * len(grad.size())
        ori_shape[0] = grad.size(0)

        max_abs_grad, _ = _torch.max(abs_grad.view(grad.size(0), -1), 1)
        max_mask = abs_grad.eq(max_abs_grad.view(ori_shape)).to(_torch.float)
        num_ties = max_mask
        for red_scalar in red_ind:
            num_ties = _torch.sum(num_ties, red_scalar, keepdim=True)
        optimal_perturbation = sign * max_mask / num_ties
        # TODO integrate below to a test file
        # check that the optimal perturbations have been correctly computed
        opt_pert_norm = optimal_perturbation.abs().sum(dim=red_ind)
        assert _torch.all(opt_pert_norm == _torch.ones_like(opt_pert_norm))
    elif norm == 2:
        square = _torch.max(avoid_zero_div, _torch.sum(grad ** 2, red_ind, keepdim=True))
        optimal_perturbation = grad / _torch.sqrt(square)
        # TODO integrate below to a test file
        # check that the optimal perturbations have been correctly computed
        opt_pert_norm = (
            optimal_perturbation.pow(2).sum(dim=red_ind, keepdim=True).sqrt()
        )
        one_mask = (square <= avoid_zero_div).to(_torch.float) * opt_pert_norm + (
            square > avoid_zero_div
        ).to(_torch.float)
        assert _torch.allclose(opt_pert_norm, one_mask, rtol=1e-05, atol=1e-08)
    else:
        raise NotImplementedError(
            "Only L-inf, L1 and L2 norms are " "currently implemented."
        )

    # Scale perturbation to be the solution for the norm=eps rather than
    # norm=1 problem
    scaled_perturbation = eps * optimal_perturbation
    return scaled_perturbation


def fast_gradient_method(model, x, eps,
                         metadata={},
                         norm=_np.inf,
                         clip_min=None,
                         clip_max=None,
                         y=None,
                         loss_fn=_nn.MSELoss(),
                         targeted=False,
                         sanity_checks=False,
                         ):
    """
    PyTorch implementation of the Fast Gradient Method. Taken from:
    https://github.com/cleverhans-lab/cleverhans.
    Read more here: https://towardsdatascience.com/adversarial-attack-and-defense-on-neural-networks-in-py_torch-82b5bcd9171

    :param model: a callable that takes an input tensor and returns the model output.
    :param x: input tensor.
    :param metadata: (optional) A dict with model forward metadata, default {}.
    :param eps: epsilon (input variation parameter); see https://arxiv.org/abs/1412.6572.
    :param norm: Order of the norm (mimics NumPy). Possible values: np.inf, 1 or 2.
    :param clip_min: (optional) float. Minimum float value for adversarial example components.
    :param clip_max: (optional) float. Maximum float value for adversarial example components.
    :param y: (optional) Tensor with response variable. If targeted is true,
              then provide the target label. Otherwise, only provide this parameter if
              you'd like to use true labels when crafting adversarial samples.
              Otherwise, model predictions are used as labels to avoid the
              "label leaking" effect (explained in this paper:
              https://arxiv.org/abs/1611.01236). Default is None.
    :param targeted: (optional) bool. Is the attack targeted or untargeted?
              Untargeted, the default, will try to make the label incorrect.
              Targeted will instead try to move in the direction of being more like y.
    :param sanity_checks: bool, if True, include asserts (Turn them off to use less runtime /
              memory or for unit tests that intentionally pass strange input)
    :return: a tensor for the adversarial example
    """

    if metadata is None:
        metadata = {}

    if norm not in [_np.inf, 1, 2]:
        raise ValueError(
            "Norm order must be either np.inf, 1, or 2, got {} instead.".format(norm)
        )
    if eps < 0:
        raise ValueError(
            "eps must be greater than or equal to 0, got {} instead".format(eps)
        )
    if eps == 0:
        return x

    if clip_min is not None and clip_max is not None:
        if clip_min > clip_max:
            raise ValueError(
                "clip_min must be less than or equal to clip_max, got clip_min={} and clip_max={}".format(
                    clip_min, clip_max
                )
            )

    asserts = []

    # If a data range was specified, check that the input was in that range
    if clip_min is not None:
        assert_ge = _torch.all(
            _torch.ge(x, _torch.tensor(clip_min, device=x.device, dtype=x.dtype))
        )
        asserts.append(assert_ge)

    if clip_max is not None:
        assert_le = _torch.all(
            _torch.le(x, _torch.tensor(clip_max, device=x.device, dtype=x.dtype))
        )
        asserts.append(assert_le)

    # x needs to be a leaf variable, of floating point type and have requires_grad being True for
    # its grad to be computed and stored properly in a backward call
    x = x.clone().detach().to(_torch.float).requires_grad_(True)

    # Compute loss
    loss = loss_fn(model(x, **metadata), y if y is not None else x)
    # If attack is targeted, minimize loss of target label rather than maximize loss of correct label
    if targeted:
        loss = -loss

    # Define gradient of loss wrt input
    loss.backward()
    optimal_perturbation = optimize_linear(x.grad, eps, norm=norm)

    # Add perturbation to original example to obtain adversarial example
    adv_x = x + optimal_perturbation

    # If clipping is needed, reset all values outside of [clip_min, clip_max]
    if (clip_min is not None) or (clip_max is not None):
        if clip_min is None or clip_max is None:
            raise ValueError(
                "One of clip_min and clip_max is None but we don't currently support one-sided clipping"
            )
        adv_x = _torch.clamp(adv_x, clip_min, clip_max)

    if sanity_checks:
        assert _np.all(asserts)
    return adv_x


def fit_adv(model, train_loader, optimizer, scheduler_fn, lossf,
            device=None,
            validation_loader=None,
            scheduler_kwargs={'T_max': 10},
            epochs=100,
            swa_start=0.9,
            swa_model=None,
            swa_scheduler=None,
            swa_scheduler_kwargs={'anneal_strategy': "cos", 'anneal_epochs': 10},
            verbose=False,
            ):

    verboseprint = print if verbose else lambda *a, **k: None

    if device is None:
        device = next(model.parameters()).device
    else:
        device = device
        model = model.to(device)

    start_time = _time.time()

    lr_scheduler = scheduler_fn(optimizer, **scheduler_kwargs)

    # swa_start = _np.inf
    if swa_model is not None:
        swa_lr = optimizer.param_groups[0]['lr']
        swa_start = int(epochs * swa_start + 1)
        swa_scheduler = swa_scheduler(optimizer, **swa_scheduler_kwargs, swa_lr=swa_lr)

    var = None
    train_loss = []
    # old_tr_loss = _np.inf
    for epoch in range(epochs):

        losses = {}
        losses['epoch'] = epoch

        tr_loss = train_epoch(model, train_loader, optimizer, device, lossf=lossf)

        if (epoch > swa_start) and (swa_model is not None):
            swa_scheduler.step()
            swa_model.collect_model(model)
        else:
            lr_scheduler.step()

        if validation_loader is not None:
            val_loss, R2, var = validation_epoch(model, validation_loader, device, lossf=lossf, init_var=var)
        else:
            val_loss, R2, var = (_np.nan, _np.nan, _np.nan)

        train_loss.append([epoch, tr_loss, val_loss, R2])

        if swa_model is not None:
            swa_model.sample(0.0)
            _swagu.bn_update(train_loader, swa_model, device=device)

        _extime = _time.time() - start_time

        curr = _time.ctime()
        verboseprint(f"epoch: {epoch+1}/{epochs}, {((epoch+1)/epochs)*100:.2f}%, t-loss = {tr_loss:6.3g}, v-loss = {val_loss:6.3g}, v-R2 = {R2:4.3g}, run time: {_extime/60:.2f}min, " + curr + ' ' * 5, end='\r')

    return train_loss
