import os as _os
import numpy as _np
import pandas as _pd
import scanpy as _sc
import torch as _torch
import hashlib as _hashlib
from anndata import AnnData as _AnnData
from inferelator.postprocessing.model_metrics import CombinedMetric as _CM
from torchvision.models.feature_extraction import create_feature_extractor as _create_feature_extractor, get_graph_node_names as _get_graph_node_names
from scipy.sparse import csr_matrix as _csr_matrix, isspmatrix as _is_sparse
from scanpy.neighbors import _compute_connectivities_umap, _get_indices_distances_from_dense_matrix
from .setup.dataloader import post_processing_loader as _ppl


def loss2df(loss, columns=['epoch', 'trl', 'conl', 'tr1l', 'tr2l', 'vl', 'R2']):

    df = _pd.DataFrame(loss)
    if df.shape[1] != 7:
        columns = _np.array(columns)
        columns = columns[[0, 1, -2, -1]]

    df.columns = columns

    return df


def mcc(pred, gs):

    pred = pred.reindex_like(gs).copy().fillna(0)
    pred = pred.astype(bool).values.ravel()
    gs = gs.astype(bool).values.ravel().copy()

    TP = (pred & gs).sum().astype(_np.float64)
    TN = (~(pred & gs)).sum().astype(_np.float64)
    FP = (pred & ~gs).sum().astype(_np.float64)
    FN = (~pred & gs).sum().astype(_np.float64)

    MCCdn = (TP + FP) * (TP + FN) * (TN + FP) * (TN + FN)

    MCCn = TP * TN - FP * FN

    if MCCdn == 0:
        print('MCC denominator is 0.')
        MCC = 0
    else:
        MCC = MCCn / _np.sqrt(MCCdn)

    F1 = 2 * TP / (2 * TP + FP + FN)

    ACC = (TP + TN) / (TP + TN + FP + FN)

    return MCC, F1, ACC


def compute_performance_opt_mcc(df, metric='MCC'):

    df_tot = df.copy()
    nlinks = df_tot.shape[0]
    tot_ntfs = df_tot['regulator'].unique().shape[0]
    tot_targets = df_tot['target'].unique().shape[0]

    df = df.dropna()

    if df.empty:
        return (0, 0, 0, 0, 0, 0, 0)
    else:
        optmetric = df[metric].idxmax()

    optconf = df.loc[optmetric]['combined_confidences']

    df_tot = df_tot[(df_tot['combined_confidences'] > optconf)]

    opt_nlinks = df_tot.shape[0] / nlinks
    opt_ntfs = df_tot['regulator'].unique().shape[0] / tot_ntfs
    opt_targets = df_tot['target'].unique().shape[0] / tot_targets

    sizegs = df['gold_standard'].sum()

    gsopt = df[df['combined_confidences'] > optconf]['gold_standard'].sum()

    fracgs = gsopt / sizegs

    opt_precision = df.loc[optmetric]['precision']

    opt_recall = df.loc[optmetric]['recall']

    return (optconf, fracgs, opt_recall, opt_precision, opt_nlinks, opt_ntfs, opt_targets)


def eval_model_performance(erv, prior, gold_standard=None, metric='MCC', losses=None, use_relmax=False):
    """
    Evaluate performance on a gold standard if provided else self reference.

    Arguments:
        erv (supirfactor.erv.ERV): An instantiated ERV object.
        prior (pd.DataFrame): The prior used in the model.
    Optional:
        gold_standard (pd.DataFrame): A gold standard dataframe with indices from prior.
        metric (str): What metric should be optimized against.
            Any of the metrics availible in the inferelator (MCC, F1, ACC), Default MCC.
        losses (pd.DataFrame, None): Losses from a supirfactor training run, Default None.
        use_relmax (bool): If the GRN should be scaled to TF importance per gene, Default False.
    """

    if '0' in erv._valid_keys.keys():
        if use_relmax:
            grn = erv._to_df(erv._relmax_matrix(erv['0_erv']), transform=True)
        else:
            grn = erv['0_erv_df'] * erv['0_mask_df']
    else:
        if use_relmax:
            grn = erv._to_df(erv._relmax_matrix(erv['Theta_erv']), transform=True)
        else:
            grn = erv['Theta_erv_df'] * erv['Theta_mask_df']

    grn.columns = prior.columns
    grn.index = prior.index

    if gold_standard is None:
        gold_standard = prior.copy()
        cv_prior = 'false'

    else:
        ps = prior.reindex_like(gold_standard)
        overlap = ps.astype(bool).astype(int).sum().sum()
        if overlap == 0:
            cv_prior = 'true'
        else:
            cv_prior = 'false'

    MCC, F1, ACC = mcc(grn.reindex_like(gold_standard).fillna(0).copy(), gold_standard.abs())

    cmm = _CM([grn.abs()], gold_standard.abs())
    optconf, fracgs, recall, prec, opt_nlinks, opt_ntfs, opt_ntargets = compute_performance_opt_mcc(cmm.confidence_data, metric=metric)

    active_tfs = (grn.astype(bool).sum(1) > 0).sum() / grn.shape[0]
    nnzout = (grn != 0).sum().sum()
    outs = _np.prod(grn.shape)

    oscr = cmm.all_scores()

    nAcc = (grn.reindex_like(gold_standard).fillna(0).astype(bool) & gold_standard.astype(bool)).sum().sum()
    totngs = gold_standard.astype(bool).sum().sum()

    performance = _pd.Series(dtype='object')
    performance['model'] = erv.state
    performance['type'] = erv.model._get_name()
    performance['arch'] = ','.join([f'{k}:{v}' for k, v in erv._valid_keys.items()])
    performance['AUPR'] = oscr['AUPR']
    performance['MCC'] = MCC
    performance['OPT_MCC'] = oscr['MCC']
    performance['F1'] = F1
    performance['OPT_F1'] = oscr['F1']
    performance['ACC'] = ACC
    performance['OPT_PREC'] = prec
    performance['OPT_RECALL'] = recall
    performance['OPT_CONF'] = optconf
    performance['OPT_FRAC_GS'] = fracgs
    performance['OPT_NLinks'] = float(opt_nlinks)
    performance['OPT_NTFs'] = float(opt_ntfs)
    performance['OPT_NTargets'] = float(opt_ntargets)

    performance['nnz'] = int(nnzout)
    performance['nnz frac'] = nnzout / float(outs)
    performance['nnz tf'] = float(active_tfs)
    performance['N GS'] = totngs
    performance['N ACC'] = nAcc
    performance['GS frac'] = float(nAcc / totngs)
    performance['CV prior'] = cv_prior

    if losses is not None:
        if not isinstance(losses, _pd.DataFrame):
            losses = loss2df(losses)

        mi = losses['epoch'].max()
        R2 = losses.loc[mi]['R2']

        performance['R2'] = R2
        performance['epochs'] = mi

    return performance


def to_anndata(data_tensor, var_names=None, sparse=False, **kwargs):

    X = data_tensor if isinstance(data_tensor, _np.ndarray) else data_tensor.cpu().numpy()

    if sparse:
        X = _csr_matrix(X)

    adata = _AnnData(X, **kwargs)
    if var_names is not None:
        adata.var_names = var_names

    return adata


def _reorder_from_inds(matrix, map_indx):

    __ = _np.zeros_like(matrix)
    for row, ind in enumerate(map_indx):
        __[ind, :] = matrix[row]

    return __


@_torch.inference_mode()
def compute_activity_w_batch_loader(model, data_loader, return_nodes={'W': 'TFA'}, device=None, eps=None):
    """
    Function for generating a latent activity for a node in the network.
    Due to how I setup the model I have to use a workaround until there is a solution for
    https://github.com/pytorch/vision/issues/6817#issuecomment-1501226056.
    """

    if device is None:
        device = next(model.buffers()).device
        model = model.eval()
    else:
        model = model.eval().to(device)

    # features = generate_latent_extractor(model, return_nodes=return_nodes).eval()

    activities = {v: [] for k, v in return_nodes.items()}
    order_idx = []
    for batch_features in data_loader:
        inputs = batch_features.inp.to(device, non_blocking=True)

        for k, v in return_nodes.items():
            output = model.LA(inputs, output=k, **batch_features.metadata)
            # outputs = features(inputs, **batch_features.metadata)
            activities[v].append(output)

        # for key in outputs.keys():
        #     activities[key].append(outputs[key])

        order_idx.extend(batch_features.sample_idx)

    for k, v in activities.items():
        v = _torch.vstack(v).detach().cpu().numpy()
        v = _reorder_from_inds(v, order_idx)
        eps = _np.finfo(v.dtype).eps if eps is None else eps
        v[_np.abs(v) < eps] = 0
        activities[k] = v

    return activities


def generate_latent_extractor(model, return_nodes={'W': 'TFA'}):

    train_nodes, vali_nodes = _get_graph_node_names(model)
    keys = ['.'.join(i.split('.')[:-1]) for i in vali_nodes]
    keys = list(dict.fromkeys(keys))
    if keys[0] == '':
        keys = keys[1:]

    if (return_nodes is None) or (return_nodes == {}):
        return keys

    return _create_feature_extractor(model, return_nodes=return_nodes)


def generate_latent_embedding(model, data, batch_size=1024, use_layer=None, device=None, shuffle_train=False, return_nodes={'W': 'TFA'}, **kwargs):

    if isinstance(data, _torch.utils.data.dataloader.DataLoader):
        data_loader = data
    else:
        data_loader, __, __ = _ppl(data, batch_size=batch_size, use_layer=use_layer, shuffle_train=shuffle_train, **kwargs)

    activities = compute_activity_w_batch_loader(model, data_loader, return_nodes=return_nodes, device=device)

    return activities


def unfold_tfa(TFA, sep=';'):
    """
    Takes an anndata object and looks at var_names. if IDs are aggregated will unfolad them to single observations and duplicate their values.
    """

    var_names = TFA.var_names

    group_counter = 1
    unstacked_data = {}
    groups = {}
    for var in var_names:
        data = TFA[:, var].X.flatten()
        if sep in var:
            group = group_counter
            group_counter += 1

        else:
            group = 0

        for name in var.split(sep):
            unstacked_data[name] = data
            groups[name] = group

    new_data = _pd.DataFrame(unstacked_data)
    tf_groups = _pd.Series(groups)
    tf_groups.name = 'tf_group'

    unfolded_TFA = to_anndata(new_data.values, var_names=new_data.columns, obsm=TFA.obsm, uns=TFA.uns, obs=TFA.obs)
    unfolded_TFA.var['tf_group'] = tf_groups

    return unfolded_TFA


def fix_df_index_from_mapper(df, mapper, sep=';', columns='Regulator'):
    """
    Accepts a df with column ('Regulator', by default) containing IDs and replaces
    IDs based on mapper where IDs might be separated with a char (';') by default.
    """

    if isinstance(columns, str):
        columns = [columns]

    id_map = {}
    for col in columns:
        id_map[col] = []

    for r, row in df.iterrows():
        for col in columns:
            ids = row[col].split(sep)
            id_map[col].append(f'{sep}'.join(mapper[mapper.index.isin(ids)].tolist()))

    return id_map


@_torch.inference_mode()
def save_model(model, output_dir=None, add_name='', model_key=None, verbose=True, dataframes={}):
    """
    Function for saving a pytorch model.

    Arguments:
        model (supirfactor.model):
        output_dir (str): directory to store the model in
            (Default './supirfactor_output' when None is provided).
        add_name (str): string to add to the filename, (Default '').
        model_key (str, None): Key to differentiate model realisations,
            if None will use the has from the model.
        verbose (bool): Will print if directory is created if True, Default True.
    """
    from datetime import datetime
    now = datetime.now()
    dt_string = now.strftime("%Y%m%d_%H%M")

    if output_dir is None:
        output_dir = 'supirfactor_output'

    output_dir = _os.path.join(output_dir, dt_string)

    mclass = model._get_name()
    if hasattr(model, 'base'):
        model.sample(0)
        mclass = model._get_name()
        mclass = mclass + '_' + model.base._get_name()

    if model_key is None:
        model_key = _hashlib.sha1(str(model.state_dict()).encode('utf-8')).hexdigest()[:8]

    if not _os.path.exists(output_dir):
        _os.makedirs(output_dir, exist_ok=True)
        if verbose:
            print(f'Results path did not exist. Creating:\n{output_dir}.')

    if not add_name.startswith('_') and (add_name != ''):
        add_name = "_" + add_name

    store_path = _os.path.join(output_dir, f'{mclass}_model_{model_key}{add_name}.pt')
    _torch.save(model, store_path)

    for k, v in dataframes.items():
        v.to_csv(_os.path.join(output_dir, f'{k}.tsv'), sep='\t')


def load_model(path, device=None):
    """A wrapper around torch.load, accepts a path to file and loads it."""

    from glob import glob

    if _os.path.isdir(path):
        return glob(_os.path.join(path, '*'))

    return _torch.load(path, map_location=device)


def transform2distance(graph, make_sparse=True, threshold=0, is_distance=False):
    """
    ERV measures influences and can be viewed as a similarity with 1 being the most similar. To convert to distance we subtract 1, ie. distance = 1 - ERV.
    """

    graph = (graph > threshold) * graph

    if make_sparse:
        graph = _csr_matrix(graph)

    if not is_distance:
        if _is_sparse(graph):
            n_neighbors = max([i.data.shape[0] for i in graph]) + 1
            graph.data = 1 - graph.data
        else:
            n_neighbors = max(graph.astype(bool).sum(1)) + 1
            graph = 1 - graph

    return graph, n_neighbors


def convert_graph2DnC(graph, N=None, nobs=None):

    if isinstance(graph, _pd.DataFrame):
        graph = graph.values

    if nobs is None:
        nobs = graph.shape[0]

    if _is_sparse(graph):
        I, D = get_indices_distances_from_sparse_matrix(graph, N)
    else:
        if N is None:
            N = int(graph.astype(bool).sum(1).max())

        I, D = _get_indices_distances_from_dense_matrix(graph, N)

    DD, CC = _compute_connectivities_umap(I, D, nobs, N)

    return DD, CC


def get_indices_distances_from_sparse_matrix(D, n_neighbors):
    # The default from scanpy isn't working as we have a varied number of neighbours.
    # TODO: MAYBE I need to transpose distances?
    indices = _np.zeros((D.shape[0], n_neighbors), dtype=int)
    distances = _np.zeros((D.shape[0], n_neighbors), dtype=D.dtype)

    n_neighbors_m1 = n_neighbors - 1
    for i in range(indices.shape[0]):
        neighbors = D[i].nonzero()  # 'true' and 'spurious' zeros
        indices[i, 0] = i
        distances[i, 0] = 0
        # account for the fact that there might be more than n_neighbors
        # due to an approximate search
        # [the point itself was not detected as its own neighbor during the search]
        if len(neighbors[1]) > n_neighbors_m1:
            sorted_indices = _np.argsort(D[i][neighbors].A1)[:n_neighbors_m1]
            indices[i, 1:] = neighbors[1][sorted_indices]
            distances[i, 1:] = D[i][
                neighbors[0][sorted_indices], neighbors[1][sorted_indices]]
        else:
            indices[i, 1:(len(neighbors[1]) + 1)] = neighbors[1]
            distances[i, 1:(len(neighbors[1]) + 1)] = D[i][neighbors]

    return indices, distances


def add_gene2gene(adata, DD, CC, graph=None, graph_name='gene_network', random_state=None):
    """
    Untested, And unknown if functional. MAYBE I need to transpose distances above?
    """

    __ = _sc.AnnData()

    adata.varp[f'{graph_name}_distances'] = DD
    adata.varp[f'{graph_name}_connectivities'] = CC
    # adata.varp[f'{graph_name}_graph'] = graph
    adata.var[f'{graph_name}_n'] = DD.astype(bool).sum(1).A1

    _sc.tl.leiden(__, adjacency=CC, random_state=random_state)
    adata.var[f'{graph_name}_leiden'] = __.obs['leiden'].values.copy()
    adata.uns[f'{graph_name}_leiden'] = __.uns['leiden']

    adata.var[f'{graph_name}_leiden'] = adata.var[f'{graph_name}_leiden'].astype(str)
    adata.var.loc[DD.astype(bool).sum(1).A1 == 0, f'{graph_name}_leiden'] = None

    adata.uns[f'{graph_name}'] = {}
    adata.uns[f'{graph_name}']['params'] = {}  # Currently a placeholder.
    adata.uns[f'{graph_name}']['connectivities_key'] = f'{graph_name}_connectivities'
    adata.uns[f'{graph_name}']['distances_key'] = f'{graph_name}_distances'
    # adata.uns[f'{graph_name}']['network_key'] = f'{graph_name}_graph'
