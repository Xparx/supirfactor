# Bare bone implementation of the SWA-Gaussian method: https://github.com/wjmaddox/swa_gaussian.
import torch
import numpy as _np
from torch.distributions.normal import Normal
import gpytorch
from gpytorch.lazy import RootLazyTensor, DiagLazyTensor, AddedDiagLazyTensor
from gpytorch.distributions import MultivariateNormal
from ... import get_device
from ..utils import flatten, unflatten_like


def swag_parameters(module, params, no_cov_mat=True):
    for name in list(module._parameters.keys()):
        if module._parameters[name] is None:
            continue
        data = module._parameters[name].data
        module._parameters.pop(name)
        module.register_buffer(f"{name}_mean", data.new(data.size()).zero_())
        module.register_buffer(f"sq_mean_{name}", data.new(data.size()).zero_())

        if no_cov_mat is False:
            module.register_buffer(f"cov_mat_sqrt_{name}", data.new_empty((0, data.numel())).zero_())

        params.append((module, name))


class SWAG(torch.nn.Module):
    """
    Implementation of SWAG, https://github.com/wjmaddox/swa_gaussian

    Args:
        base (torch model.class):
        no_cov_mat (bool): If we should skip covariance map. Very expencive memory wise.
            Default True.
        max_num_models (int): number of models to use for the swag estimate, (default 10).
        var_clamp (float): To truncate variance to avoid division by zero, Default 1e-30.
        model_args (tuple): Arguments passed to model constructor, default ().
        model_kwargs (dict): Key word arguments passed to the model constructor, default {}.
    """

    def __init__(self, base,
                 no_cov_mat=True,
                 max_num_models=10,
                 var_clamp=1e-30,
                 model_args=(),
                 model_kwargs={},
                 device=None):

        super().__init__()

        self.register_buffer("n_models", torch.zeros([1], dtype=torch.long))
        self.params = list()

        if device is None:
            self.device = get_device()
        else:
            self.device = device

        self.no_cov_mat = no_cov_mat
        self.max_num_models = max_num_models

        self.var_clamp = var_clamp

        self.base = base(*model_args, **model_kwargs)
        self.base_parameters = list(dict(self.base.named_parameters()).keys())

        self.base.apply(
            lambda module: swag_parameters(
                module=module, params=self.params, no_cov_mat=self.no_cov_mat
            )
        )

    def forward(self, *args, **kwargs):
        return self.base(*args, **kwargs)

    def sample(self, scale=1.0, cov=False, seed=None, block=False, fullrank=True):
        if seed is not None:
            torch.manual_seed(seed)
            torch.cuda.manual_seed(seed)

        if not block:
            self.sample_fullrank(scale, cov, fullrank)
        else:
            raise ValueError('Currently block sampling is not supported.')
            self.sample_blockwise(scale, cov, fullrank)

    def sample_blockwise(self, scale, cov, fullrank):
        for module, name in self.params:
            mean = module.__getattr__(f"{name}")

            sq_mean = module.__getattr__(f"sq_mean_{name}")
            eps = torch.randn_like(mean)

            var = torch.clamp(sq_mean - mean ** 2, self.var_clamp)

            scaled_diag_sample = scale * torch.sqrt(var) * eps

            if cov is True:
                cov_mat_sqrt = module.__getattr__(f"cov_mat_sqrt_{name}")
                eps = cov_mat_sqrt.new_empty((cov_mat_sqrt.size(0), 1)).normal_()
                cov_sample = (scale / ((self.max_num_models - 1) ** 0.5)) * cov_mat_sqrt.t().matmul(eps).view_as(mean)

                if fullrank:
                    w = mean + scaled_diag_sample + cov_sample
                else:
                    w = mean + scaled_diag_sample

            else:
                w = mean + scaled_diag_sample

            module.__setattr__(name, w)

    def sample_fullrank(self, scale, cov, fullrank):
        scale_sqrt = scale ** 0.5

        mean_list = []
        sq_mean_list = []

        if cov:
            cov_mat_sqrt_list = []

        for (module, name) in self.params:
            mean = module.__getattr__(f"{name}_mean")
            sq_mean = module.__getattr__(f"sq_mean_{name}")

            if cov:
                cov_mat_sqrt = module.__getattr__(f"cov_mat_sqrt_{name}")
                cov_mat_sqrt_list.append(cov_mat_sqrt.cpu())

            mean_list.append(mean.cpu())
            sq_mean_list.append(sq_mean.cpu())

        mean = flatten(mean_list)
        sq_mean = flatten(sq_mean_list)

        # draw diagonal variance sample
        var = torch.clamp(sq_mean - mean ** 2, self.var_clamp)
        var_sample = var.sqrt() * torch.randn_like(var, requires_grad=False)

        # if covariance draw low rank sample
        if cov:
            cov_mat_sqrt = torch.cat(cov_mat_sqrt_list, dim=1)

            cov_sample = cov_mat_sqrt.t().matmul(
                cov_mat_sqrt.new_empty(
                    (cov_mat_sqrt.size(0),), requires_grad=False
                ).normal_()
            )
            cov_sample /= (self.max_num_models - 1) ** 0.5

            rand_sample = var_sample + cov_sample
        else:
            rand_sample = var_sample

        # update sample with mean and scale
        sample = mean + scale_sqrt * rand_sample
        sample = sample.unsqueeze(0)

        # unflatten new sample like the mean sample
        samples_list = unflatten_like(sample, mean_list)

        for (module, name), sample in zip(self.params, samples_list):
            # print(name)
            module.__setattr__(name, sample.to(self.device))
            # if 'orig' in name:
            #     # mask = module.__getattr__(name.replace('orig', 'mask'))
            #     # w = torch.mul(sample.to(self.device), mask.to(self.device))

            #     # module.__setattr__(name, w)
            #     # module.__setattr__(name.replace('_orig', ''), w)
            #     module.__setattr__(name, sample.to(self.device))
            # else:
            #     module.__setattr__(name, sample.to(self.device))

    @torch.inference_mode()
    def collect_model(self, base_model):
        for (module, name), base_param in zip(self.params, base_model.parameters()):
            mean = module.__getattr__(f"{name}_mean")
            sq_mean = module.__getattr__(f"sq_mean_{name}")

            # first moment
            mean = mean * self.n_models.detach() / (
                self.n_models.detach() + 1.0
            ) + base_param.data / (self.n_models.detach() + 1.0)

            # second moment
            sq_mean = sq_mean * self.n_models.detach() / (
                self.n_models.detach() + 1.0
            ) + base_param.data ** 2 / (self.n_models.detach() + 1.0)

            # square root of covariance matrix
            if self.no_cov_mat is False:
                cov_mat_sqrt = module.__getattr__(f"cov_mat_sqrt_{name}")

                # block covariance matrices, store deviation from current mean
                dev = (base_param.data - mean).view(-1, 1)
                cov_mat_sqrt = torch.cat((cov_mat_sqrt, dev.view(-1, 1).t()), dim=0)

                # remove first column if we have stored too many models
                if (self.n_models.detach() + 1) > self.max_num_models:
                    cov_mat_sqrt = cov_mat_sqrt[1:, :]
                module.__setattr__(f"cov_mat_sqrt_{name}", cov_mat_sqrt)

            module.__setattr__(f"{name}_mean", mean)
            module.__setattr__(f"sq_mean_{name}", sq_mean)

        # self.generate_logconf()
        self.n_models.add_(1)

    def load_state_dict(self, state_dict, strict=True):
        if not self.no_cov_mat:
            n_models = state_dict["n_models"].item()
            rank = min(n_models, self.max_num_models)
            for module, name in self.params:
                mean = module.__getattr__(f"{name}_mean")
                module.__setattr__(
                    f"cov_mat_sqrt_{name}",
                    mean.new_empty((rank, mean.numel())).zero_(),
                )
        super(SWAG, self).load_state_dict(state_dict, strict)

    def export_numpy_params(self, export_cov_mat=False):
        mean_list = []
        sq_mean_list = []
        cov_mat_list = []

        for module, name in self.params:
            mean_list.append(module.__getattr__(f"{name}_mean").cpu().numpy().ravel())
            sq_mean_list.append(
                module.__getattr__(f"sq_mean_{name}").cpu().numpy().ravel()
            )

            if export_cov_mat:
                cov_mat_list.append(
                    module.__getattr__(f"cov_mat_sqrt_{name}").cpu().numpy().ravel()
                )
        mean = _np.concatenate(mean_list)
        sq_mean = _np.concatenate(sq_mean_list)
        var = sq_mean - _np.square(mean)

        if export_cov_mat:
            return mean, var, cov_mat_list
        else:
            return mean, var, None

    def import_numpy_weights(self, w):
        k = 0
        for module, name in self.params:
            mean = module.__getattr__(f"{name}_mean")
            s = _np.prod(mean.shape)
            module.__setattr__(name, mean.new_tensor(w[k: k + s].reshape(mean.shape)))
            k += s

    def generate_mean_var_covar(self):
        mean_list = []
        var_list = []
        cov_mat_root_list = []
        for module, name in self.params:
            mean = module.__getattr__(f"{name}_mean")
            sq_mean = module.__getattr__(f"sq_mean_{name}")

            mean_list.append(mean)
            var_list.append(sq_mean - mean ** 2.0)

            if not self.no_cov_mat:
                cov_mat_sqrt = module.__getattr__(f"cov_mat_sqrt_{name}")
                cov_mat_root_list.append(cov_mat_sqrt)

        return mean_list, var_list, cov_mat_root_list

    def compute_ll_for_block(self, vec, mean, var, cov_mat_root):
        vec = flatten(vec)
        mean = flatten(mean)
        var = flatten(var)

        cov_mat_lt = RootLazyTensor(cov_mat_root.t())
        var_lt = DiagLazyTensor(var + 1e-6)
        covar_lt = AddedDiagLazyTensor(var_lt, cov_mat_lt)
        qdist = MultivariateNormal(mean, covar_lt)

        return qdist
        with gpytorch.settings.num_trace_samples(1) and gpytorch.settings.max_cg_iterations(25):
            return qdist.log_prob(vec.cpu())

    def block_logdet(self, var, cov_mat_root):
        var = flatten(var)

        cov_mat_lt = RootLazyTensor(cov_mat_root.t())
        var_lt = DiagLazyTensor(var + 1e-6)
        covar_lt = AddedDiagLazyTensor(var_lt, cov_mat_lt)

        return covar_lt.log_det()

    def block_logll(self, param_list, mean_list, var_list, cov_mat_root_list):
        full_logprob = 0
        for i, (param, mean, var, cov_mat_root) in enumerate(
            zip(param_list, mean_list, var_list, cov_mat_root_list)
        ):
            # print('Block: ', i)
            block_ll = self.compute_ll_for_block(param, mean, var, cov_mat_root)
            full_logprob += block_ll

        return full_logprob

    def full_logll(self, param_list, mean_list, var_list, cov_mat_root_list):
        cov_mat_root = torch.cat(cov_mat_root_list, dim=1)
        mean_vector = flatten(mean_list)
        var_vector = flatten(var_list)
        param_vector = flatten(param_list)
        return self.compute_ll_for_block(
            param_vector, mean_vector, var_vector, cov_mat_root
        )

    def compute_logdet(self, block=False):
        _, var_list, covar_mat_root_list = self.generate_mean_var_covar()

        if block:
            full_logdet = 0
            for (var, cov_mat_root) in zip(var_list, covar_mat_root_list):
                block_logdet = self.block_logdet(var, cov_mat_root)
                full_logdet += block_logdet
        else:
            var_vector = flatten(var_list)
            cov_mat_root = torch.cat(covar_mat_root_list, dim=1)
            full_logdet = self.block_logdet(var_vector, cov_mat_root)

        return full_logdet

    def diag_logll(self, param_list, mean_list, var_list):
        logprob = 0.0
        for param, mean, scale in zip(param_list, mean_list, var_list):
            logprob += Normal(mean, scale).log_prob(param).sum()
        return logprob

    def diag_logll_full(self, param_list, mean_list, var_list):
        logprob = []
        for param, mean, scale in zip(param_list, mean_list, var_list):
            logprob.append(Normal(mean, torch.clamp(scale.sqrt(), _np.sqrt(self.var_clamp))).log_prob(param))
        return logprob

    def compute_logprob(self, vec=None, block=False, diag=False):
        mean_list, var_list, covar_mat_root_list = self.generate_mean_var_covar()

        if vec is None:
            param_list = [getattr(param, name).cpu() for param, name in self.params]
        else:
            param_list = unflatten_like(vec, mean_list)

        if diag or self.no_cov_mat:
            return self.diag_logll(param_list, mean_list, var_list)
        elif block is True:
            return self.block_logll(
                param_list, mean_list, var_list, covar_mat_root_list)
        else:
            return self.full_logll(param_list, mean_list, var_list, covar_mat_root_list)
