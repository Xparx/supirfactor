import torch as _torch
_DTYPE = _torch.float


def get_device(device=None):

    if device is None:
        if _torch.cuda.is_available():
            device = 'cuda:0'
        else:
            device = 'cpu'
    else:
        if not (_torch.cuda.is_available() and device.startswith('cuda')):
            device = 'cpu'

    return _torch.device(device)
