import numpy as _np
import pandas as _pd
import scanpy as _sc
from warnings import warn as _warn


def geosketch(data,
              num_cells=None,
              num_pc=50,
              use_rep='pca',
              return_index=False,
              seed=None,
              recompute_pca=False,
              **kwargs
              ):
    """
    geosketch from https://github.com/brianhie/geosketch.

    Parameters
    ----------
    data: AnnData object
        data to be sub-sampled
    num_cells: None,
    num_pc: 50,
    return_index: False,
    seed: None, For geosketch.
    Returns
    -------
    AnnData
        Subsampled cells from input data.
    """

    try:
        from geosketch import gs
    except ImportError:
        raise ImportError('This function require geosketch, see: https://github.com/brianhie/geosketch')

    input_cells = data.shape[0]

    if num_cells is None:
        num_cells = int(input_cells * 0.1)
    else:
        if num_cells < 1:
            num_cells = int(input_cells * num_cells)

    if use_rep == 'pca': 
        if ('pca' not in data.uns) or recompute_pca:
            _warn('PCA not computed or set to be recomputed. Computing with given arguments.')
            _sc.pp.pca(data, n_comps=num_pc, **kwargs)

        if data.obsm['X_pca'].shape[1] < num_pc:
            _warn('Number of PCs are less than specified. Recomputing PCA.')
            _sc.pp.pca(data, n_comps=num_pc, **kwargs)

        x_dimred = data.obsm['X_pca'][:, :num_pc] * data.uns['pca']['variance'][:num_pc]
    elif use_rep in data.obsm:
        x_dimred = data.obsm[use_rep]
    else:
        except:
            raise ValueError(f"{use_rep} not availible in the obsm field.")

    sketch_index = gs(x_dimred, num_cells, replace=False)
    if return_index:
        anti_index = [i for i in range(input_cells) if i not in sketch_index]
        return sketch_index, anti_index
    x_matrix = data[sketch_index, :].copy()

    return x_matrix


def revigo(userData, url="http://revigo.irb.hr/Revigo", output_raw=False, **kwargs):
    '''
    Python function for programtic access to Revigo
    http://dx.doi.org/10.1371/journal.pone.0021800 (http://revigo.irb.hr/).
    Adapted from http://revigo.irb.hr/Examples/Code/revigo_restful.py.txt.

    Cutoff determines how large would you like the resulting list to be:
    Large (0.9)  Medium (0.7)  Small (0.5)  Tiny (0.4) Warning

    Parameters
    ----------
    userData (list): list of GO terms.
    **kwargs: Will override the default payload = {'cutoff':'0.5', 'valueType':'pvalue', 'speciesTaxon':'0', 'measure':'SIMREL', 'goList':userData}.
        Check wepage for overview of arguments.

    Returns
    -------
    text
    '''

    import requests

    # Read enrichments file
    if isinstance(userData, str):
        userData = open(userData, 'r').read()
    elif isinstance(userData, _pd.DataFrame):
        userData = userData.to_string(index=False, header=False)
    else:
        raise ValueError('type for userdata is unknow. Needs to be pandas DataFrame with GO term, p_value as columns or link to file.')

    payload = {'cutoff': '0.5',
               'valueType': 'pvalue',
               'speciesTaxon': '0',
               'measure': 'SIMREL',
               'goList': userData}

    payload = {**kwargs, **payload}

    # Submit job to Revigo
    r = requests.post(url, data=payload)

    if output_raw:
        return r.text

    output_df = _pd.concat(_pd.read_html(r.text))

    return output_df


def gprofiler_from_network(gene_network, regulator='Regulator', target='Target', values='ERV', background=None, organism="hsapiens", **kwargs):

    if (regulator not in gene_network.columns) or (target not in gene_network.columns):
        raise ValueError(f'One of {regulator} or {target} is not in network columns. Is the network in long form (required)?')

    gq_list = gene_network.groupby(regulator)[target].apply(list).to_dict()

    if background is None:
        background = gene_network[target].unique()

    enrich = gprofiler_from_lists(gq_list, background=background, organism=organism, **kwargs)

    return enrich


def gprofiler_from_lists(gq_list, background=None, organism="hsapiens", **kwargs):
    """
    Uses gprofiler to auto compute enrichments and organise results.

    gq_list: can be a list or a dict with list and query keys.
    background: is the genes used as global list.
    """

    try:
        from gprofiler import GProfiler
    except ImportError:
        raise ImportError('This function require gprofiles, see: https://pypi.org/project/gprofiler-official')

    if ~isinstance(background, list) or (background is not None):
        background = list(background)

    gp = GProfiler(return_dataframe=True)
    enrich = gp.profile(organism=organism,
                        query=gq_list,
                        background=background,
                        **kwargs
                        )

    enrich['GeneRatio'] = enrich['intersection_size'] / enrich['term_size']
    enrich['-log(adj_p_value)'] = -_np.log10(enrich['p_value'])

    TP = enrich['intersection_size']
    FN = enrich['term_size'] - enrich['intersection_size']
    FP = enrich['query_size'] - enrich['intersection_size']
    enrich['Jaccard'] = (TP / (TP + FP + FN)).astype(float)
    enrich['F1'] = (2 * TP / (2 * TP + FP + FN)).astype(float)
    enrich['PPV'] = (TP / (TP + FP)).astype(float)

    return enrich


def peak2gene_map(gene_bed, peak_bed, distance=1e5, w=1e5):
    """
    fgrep -w gene datafiles/genes.gtf | sed 's/[";]//g;' | awk '{OFS="\t"; print $1, $3, $4-1,$5,$12,0,$7,$10,$16,$14}' > genes.bed

    sort -k1,1 -k2,2n genes.bed > genes.sorted.bed
    # bedtools closest -a myfile.bed -b gene.sorted.bed
    bedtools window -a myfile.bed -b genes.sorted.bed -w 100000

    """

    try:
        from pybedtools import BedTool
    except ImportError:
        raise ImportError('This function require pybedtools, see: https://daler.github.io/pybedtools\n and bedtools, see: https://bedtools.readthedocs.io')

    regions = BedTool(peak_bed)
    genes = BedTool(gene_bed)

    # intergenic_regions = regions.subtract(genes)
    nearby = regions.window(genes, w=w)

    return nearby.to_dataframe(header=None, disable_auto_names=True)


def gtf2tsv(filepath):
    """
    !fgrep -w gene genes.gtf | sed 's/[";]//g;' | awk '{OFS="\t"; print $1, $4-1, $5,$12, 0, $7, $10, $16, $3, $14}' > genes.bed
!sort -k1,1 -k2,2n -k3,3n genes.bed > genes.sorted.bed

    """

    pass
