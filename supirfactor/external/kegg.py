import pandas as _pd
try:
    from Bio.KEGG import REST as _REST
except ImportError:
    raise ImportError('This module require biopython, see: https://biopython.org/wiki/Download')


def download_all_kegg_pathways(species_code='mmu', to_df=True, return_list=False):
    """
    https://python.hotexamples.com/examples/Bio.KEGG/REST/-/python-rest-class-examples.html
    """
    pathways_str = _REST.kegg_list("pathway", species_code).read()
    pathways = {p.split('\t')[0]: {'name': p.split('\t')[1]} for p in pathways_str.rstrip().split('\n')}

    if return_list:
        return pathways

    pathways = get_genes_for(pathways)
    if to_df:
        pathways = _pd.DataFrame(pathways).T
        pathways.index.name = 'Pathway'

    return pathways


def get_genes_for(pathways):
    for pathway in pathways:
        pathways[pathway]['geneid'] = set()
        pathways[pathway]['gene_symbol'] = set()
        pathways[pathway]['compound'] = set()
        pathways[pathway]['compound_desc'] = set()
        pathway_file = _REST.kegg_get(pathway).read()  # query and read each pathway
        # iterate through each KEGG pathway file, keeping track of which section
        # of the file we're in, only read the gene in each pathway
        current_section = None
        for line in pathway_file.rstrip().split("\n"):
            section = line[:12].strip()  # section names are within 12 columns
            if not section == "":
                current_section = section
            if current_section == "GENE":
                try:
                    gene_identifiers, _ = line[12:].split("; ")[:2]
                    geneid, gene_symbol = gene_identifiers.split()
                    pathways[pathway]['geneid'].add(int(geneid))
                    pathways[pathway]['gene_symbol'].add(gene_symbol)
                except:
                    pass  # print('Discarded:', line);
            if current_section == 'COMPOUND':
                try:
                    compound, description = line[12:].split('  ')
                    pathways[pathway]['compound'].add(compound)
                    pathways[pathway]['compound_desc'].add(description)
                except:
                    pass  # print('Discarded:', line);

        pathways[pathway]['geneid'] = ';'.join([str(i) for i in list(pathways[pathway]['geneid'])])
        pathways[pathway]['gene_symbol'] = ';'.join(list(pathways[pathway]['gene_symbol']))
        pathways[pathway]['compound'] = ';'.join(list(pathways[pathway]['compound']))
        pathways[pathway]['compound_desc'] = ';'.join(list(pathways[pathway]['compound_desc']))

    return pathways


def parse_kegg_page(compound_file, store_data, compound):

    store_data[compound]['all_names'] = set()
    store_data[compound]['formula'] = set()

    current_section = None
    for line in compound_file.rstrip().split("\n"):
        section = line[:12].strip()  # section names are within 12 columns
        if not section == "":
            current_section = section

        # if current_section == 'ENTRY':
        #     line[12:].split()[0]
        if current_section == "FORMULA":
            try:
                formula = line[12:].strip()
                store_data[compound]['formula'].add(formula)
            except:
                pass  # print('Discarded:', line);
        if current_section == 'NAME':
            try:
                nn = line[12:].strip().replace(';', '')
                store_data[compound]['all_names'].add(nn)
            except:
                pass  # print('Discarded:', line);

    store_data[compound]['formula'] = ';'.join([str(i) for i in list(store_data[compound]['formula'])])
    store_data[compound]['all_names'] = ';'.join(list(store_data[compound]['all_names']))


def get_formula_for_compounds(compound_list, store_data, step=10):
    import time
    start = time.time()
    e = 0

    llist = len(compound_list)
    pre_step = 0
    for i in range(step, llist, step):
        compounds = compound_list[pre_step:i]
        before = compounds.copy()
        compounds = [i for i in compounds if (i not in store_data)]
        if compounds == []:
            print(f'all of {before} already in dict.\n Moving on.')
            continue

        pre_step = i
        compound_file = _REST.kegg_get(compounds).read()  # query and read each compound
        compound_files = compound_file.split('///')
        if compound_files[-1] == '\n':
            compound_files = compound_files[:-1]

        for j, c_file in enumerate(compound_files):
            e = e + 1
            store_data[compounds[j]] = {}
            parse_kegg_page(c_file, store_data, compounds[j])
            end = time.time()
            print(f"{end - start:.2f}s, {e/len(compound_list)*100:.2f}% done." + 10 * ' ', flush=True, end='\r')


def get_genes_for_compounds(compounds, store_data):
    """
    Untested.
    """
    import time
    start = time.time()
    e = 0
    for compound in compounds:
        e = e + 1
        if compound in store_data:
            print(f"{compound} data already exist.", end=', ')
            continue

        store_data[compound] = {}
        store_data[compound]['name'] = compounds[compound]['name']
        store_data[compound]['all_names'] = set()
        store_data[compound]['formula'] = set()
        compound_file = _REST.kegg_get(compound).read()  # query and read each compound
        # iterate through each KEGG compound file, keeping track of which section
        # of the file we're in, only read the gene in each compound
        current_section = None
        for line in compound_file.rstrip().split("\n"):
            section = line[:12].strip()  # section names are within 12 columns
            if not section == "":
                current_section = section
            if current_section == "FORMULA":
                try:
                    formula = line[12:].strip()
                    store_data[compound]['formula'].add(formula)
                except:
                    pass  # print('Discarded:', line);
            if current_section == 'NAME':
                try:
                    nn = line[12:].strip().replace(';', '')
                    store_data[compound]['all_names'].add(nn)
                except:
                    pass  # print('Discarded:', line);

        store_data[compound]['formula'] = ';'.join([str(i) for i in list(store_data[compound]['formula'])])
        store_data[compound]['all_names'] = ';'.join(list(store_data[compound]['all_names']))

        end = time.time()
        print(f"{end - start:.2f}s, {e/len(compounds)*100:.2f}% done" + 10 * ' ', flush=True, end='\r')


def download_all_kegg(klass='compound'):
    """
    https://python.hotexamples.com/examples/Bio.KEGG/REST/-/python-rest-class-examples.html
    """

    compounds_str = _REST.kegg_list(klass).read()
    compounds = {p.split('\t')[0]: {'name': p.split('\t')[1]} for p in compounds_str.rstrip().split('\n')}
    return compounds


def build_compound_file():

    comps = download_all_kegg()
    fetched_compounds = {}
    get_formula_for_compounds(list(comps.keys()), fetched_compounds)
    __ = _pd.DataFrame(fetched_compounds).T
    __.index.name = 'compound'

    return __
