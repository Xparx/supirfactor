from torch.nn.modules.loss import _Loss
from torch.nn import *
import torch as _torch
from torch.nn import ReLU, Module, MSELoss, L1Loss
from torch import Tensor as _T
import warnings as _warnings


class ReLUgradat0(_torch.autograd.Function):
    """
    source: https://pytorch.org/tutorials/beginner/examples_autograd/two_layer_net_custom_function.html
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    @staticmethod
    def forward(ctx, input):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        ctx.save_for_backward(input)
        return input.clamp(min=0)

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        input, = ctx.saved_tensors
        grad_input = grad_output.clone()
        grad_input[input < 0] = 0
        return grad_input


class ReLU0(ReLU):

    def forward(self, input: _T) -> _T:
        return ReLUgradat0.apply(input)


class SparseLoss(Module):
    """Documentation for SparseLoss

    """
    def __init__(self, lam=1, reduction='mean'):
        super().__init__()

        if reduction != 'mean':
            raise ValueError(f"Currenly only works for reduction = 'mean'. Now it's reduction={reduction}.")

        self._lambda = lam
        self.mseloss = MSELoss(reduction=reduction)
        # self.sparseloss = L1Loss(reduction=reduction)

    def forward(self, output, target):

        loss = self.mseloss(output, target)
        spred = (output != 0).int()
        sparse = (target != 0).int()
        # bloss = self.sparseloss(spred, sparse)
        bloss = _torch.mean((spred != sparse).float())
        # bloss = _torch.mean()

        return loss + self._lambda * bloss


class TweedieLoss(_Loss):
    """TweedieLoss

    """

    __constants__ = ['reduction']

    def __init__(self, p=1.5, reduction: str = 'mean'):
        super().__init__(reduction)

        assert 1 <= p < 2, "p must be in range [1, 2]"
        self.p = p

    def forward(self, output, target):

        loss = tweedie_loss(output, target, self.p, self.reduction)
        return loss


def tweedie_loss(
        input: _T,
        target: _T,
        p: float = 1.5,
        reduction: str = "mean",
) -> _T:
    r"""tweedie_loss(input, target, size_average=None, reduction='mean') -> Tensor

    Measures the element-wise tweedie loss error.
    """

    if not (target.size() == input.size()):
        _warnings.warn(
            "Using a target size ({}) that is different to the input size ({}). "
            "This will likely lead to incorrect results due to broadcasting. "
            "Please ensure they have the same size.".format(target.size(), input.size()),
            stacklevel=2,
        )

    expanded_input, expanded_target = _torch.broadcast_tensors(input, target)

    n1 = (1 - p)
    if n1 == 0:
        n1 = 1

    n2 = (2 - p)
    if n2 == 0:
        n2 = 1

    # a = expanded_target * _torch.exp(expanded_input * (1 - p)) / (1 - p)
    # b = _torch.exp(expanded_input * (2 - p)) / (2 - p)
    a = expanded_target * _torch.pow(expanded_input, (1 - p)) / n1
    b = _torch.pow(expanded_input, (2 - p)) / n2
    loss = -a + b

    if reduction == 'mean':
        return loss.mean()
    elif reduction == 'sum':
        return loss.sum()
    else:
        return loss


def _tweedie_deviance_score(preds: _T, targets: _T,
                            power: float = 0.0):
    """Update and returns variables required to compute Deviance Score for the given power.

    Check for same shape of input tensors.

    Args:
        preds: Predicted tensor
        targets: Ground truth tensor
        power: see :func:`tweedie_deviance_score`

    Example:
        >>> targets = torch.tensor([1.0, 2.0, 3.0, 4.0])
        >>> preds = torch.tensor([4.0, 3.0, 2.0, 1.0])
        >>> _tweedie_deviance_score_update(preds, targets, power=2)
        (tensor(4.8333), tensor(4))

    """
    if not (targets.size() == preds.size()):
        _warnings.warn(
            "Using a target size ({}) that is different to the input size ({}). "
            "This will likely lead to incorrect results due to broadcasting. "
            "Please ensure they have the same size.".format(targets.size(), preds.size()),
            stacklevel=2,
        )

    zero_tensor = _torch.zeros(preds.shape, device=preds.device)

    if 0 < power < 1:
        raise ValueError(f"Deviance Score is not defined for power={power}.")

    if power == 0:
        deviance_score = _torch.pow(targets - preds, exponent=2)
    elif power == 1:
        # Poisson distribution
        if _torch.any(preds <= 0) or _torch.any(targets < 0):
            raise ValueError(
                f"For power={power}, 'preds' has to be strictly positive and 'targets' cannot be negative."
            )

        res = targets * _torch.log(targets / preds)
        res[targets == 0] = 0.0
        deviance_score = 2 * (res + preds - targets)

    elif power == 2:
        # Gamma distribution
        if _torch.any(preds <= 0) or _torch.any(targets <= 0):
            raise ValueError(f"For power={power}, both 'preds' and 'targets' have to be strictly positive.")

        deviance_score = 2 * (_torch.log(preds / targets) + (targets / preds) - 1)
    else:
        if power < 0:
            if _torch.any(preds <= 0):
                raise ValueError(f"For power={power}, 'preds' has to be strictly positive.")
        elif 1 < power < 2:
            if _torch.any(preds <= 0) or _torch.any(targets < 0):
                raise ValueError(
                    f"For power={power}, 'targets' has to be strictly positive and 'preds' cannot be negative."
                )
        else:
            if _torch.any(preds <= 0) or _torch.any(targets <= 0):
                raise ValueError(f"For power={power}, both 'preds' and 'targets' have to be strictly positive.")

        term_1 = _torch.pow(_torch.max(targets, zero_tensor), 2 - power) / ((1 - power) * (2 - power))
        term_2 = targets * _torch.pow(preds, 1 - power) / (1 - power)
        term_3 = _torch.pow(preds, 2 - power) / (2 - power)
        deviance_score = 2 * (term_1 - term_2 + term_3)

    # sum_deviance_score = _torch.sum(deviance_score)
    # num_observations = torch.tensor(torch.numel(deviance_score), device=preds.device)

    return deviance_score
