import torch as _torch
import numpy as _np
import pandas as _pd
import hashlib as _hashlib
from torch import nn as _nn
import torch.nn.utils.prune as _prune
from collections import UserDict as _UserDict
from .run import compute_out_feature_loss as _feature_loss
from . import _DTYPE
from torchvision.models.feature_extraction import get_graph_node_names as _get_graph_node_names


class ERV(_UserDict):
    """
    Explained Relative Variance (ERV).

    Arguments:
        model (supirfactor.models model): Model to use to compute ERV.
        dataloader (supirfactor.Dataloader): Typically the validation set.
    Optional:
        relmax (float): If ervs should be computed and scaled to relative maximum
            for each output feature. Values used is in ]0, 1]. If 0 will not rescale.
            If > 0 will rescale values and use these new values for thresholding.
            Default 0.
        eps (float, None): Precision to be used when thresholding values.
            If None will be torch.finfo(ERV.dtype).eps. Default None.
        threshold (bool): If erv values should be thresholded at `eps`, default True.
        verbose (bool): Unused, default False.
        err_func (torch.nn.(loss_function)): What loss function to use. Needs to be
            provided as the loss class instantiated object with reduction='none'.
            Default nn.MSELoss(reduction='none').
        device (torch.device, None): Torch device, cpu or cuda,
            if None will use the model device. Default None.
        **kwargs: Arguments provided to compute base error. Not used.

    from supirfactor import erv

    model_erv = erv.ERV(model, dataloader)

    # For a deep model:
    GRN = model_erv['0_erv_df']

    # For a shallow model:
    GRN = model_erv['Theta_erv_df']

    # To check valid keys.
    model_erv._valid_keys

    """
    dtype = _DTYPE

    def __init__(self, model, dataloader,
                 relmax=0,
                 eps=None,
                 threshold=True,
                 verbose=False,
                 err_func=_nn.MSELoss(reduction='none'),
                 device=None,
                 **kwargs
                 ):
        super().__init__()

        if device is None:
            self.device = next(model.buffers()).device
        else:
            self.device = device
        self.model = model.to(self.device)

        self.dataloader = dataloader
        self.relmax = relmax
        self.threshold = threshold
        self.verbose = verbose
        self.eps = _torch.finfo(ERV.dtype).eps if eps is None else eps
        self._err_func = err_func
        self.state = _hashlib.sha1(str(self.model.state_dict()).encode('utf-8')).hexdigest()[:8]

        self.set_valid_keys()
        self.set_base_err(**kwargs)

    def _check_model(self):

        model_hash = _hashlib.sha1(str(self.model.state_dict()).encode('utf-8')).hexdigest()[:8]
        if not (model_hash == self.state):
            raise ValueError('Model has changed. Re-initailize ERV object with new model.')

        return True

    def set_valid_keys(self):
        """
        Extract valid layers to compute ERV on for a SupirFactor model.
        """

        # __ = [i for i, j in self.model.named_children()]
        # if 'Pi' in [i for i, j in self.model.named_children()]:
        #     ___ = [i for i, j in self.model.Pi.named_children()]
        #     __.extend(___)
        #     __ = _np.array(__)
        #     __ = __[__ != 'Pi']
        # self._valid_keys = [str(i) for i in __]

        # Ugly solution but has to work for now. Problem is that named buffers are only valid for the registered buffers and it happens to coincide with what I need.
        # buffnames = {name.split('.linear')[0].split('.')[-1]: buff.shape[1] for name, buff in self.model.named_buffers() if (('prior_mask' not in name) and ('context' not in name))}
        buffnames = {name.split('.linear')[0].split('.')[-1]: buff.shape[-1] for name, buff in self.model.named_buffers() if (('linear' in name) and ('context' not in name))}

        self._valid_keys = buffnames

    def set_base_err(self, **kwargs):

        self._check_model()

        loss, rss, var = _feature_loss(self.model, self.dataloader, self.device, lossf=self._err_func, silence=None, **kwargs)

        self.data['err_base'] = loss
        self.data['var'] = var

    def compute_feature_loss_for(self, layer_key, **kwargs):

        self._check_model()

        buffnames = self._valid_keys

        if layer_key not in buffnames:
            raise ValueError(f"{layer_key} is not valid.\nValid keys are {buffnames.keys()}")

        nf = buffnames[layer_key]

        ERR = []
        for feat in range(nf):

            silence = (layer_key, feat)

            loss, rss, var = _feature_loss(self.model, self.dataloader, self.device, lossf=self._err_func, init_var=self['var'], silence=silence, **kwargs)

            ERR.append(loss)

        ERR = _torch.stack(ERR, 1)
        self.data[layer_key] = ERR

    def compute_intermediate_loss_for(self, layer_keys, **kwargs):
        self._check_model()
        self._check_if_intermediate(layer_keys)

        buffnames = self._valid_keys
        keys = layer_keys.split('.')

        nf = [buffnames[l_key] for l_key in keys]

        ERR = []
        for f1 in range(nf[0]):

            for f2 in range(nf[1]):

                silence = (keys, [f1, f2])

                loss, rss, var = _feature_loss(self.model, self.dataloader, self.device, lossf=self._err_func, init_var=self['var'], silence=silence, **kwargs)

                ERR.append(loss.sum())

        self.data[layer_keys] = _torch.reshape(_torch.tensor(ERR), nf)

    def _get_return_state(self, key):

        return_df = False
        if key.endswith('_df'):
            return_df = True
            key = key.replace('_df', '')

        return_state = 'err'
        if key.lower().endswith('_erv'):
            return_state = 'erv'
            key = key.replace('_erv', '').replace('_ERV', '')

        elif key.lower().endswith('_uv'):
            return_state = 'uv'
            key = key.replace('_uv', '').replace('_UV', '')

        elif key.lower().endswith('_mask'):
            return_state = 'mask'
            key = key.replace('_mask', '').replace('_MASK', '')

        elif key.lower().endswith('_sign'):
            return_state = 'sign'
            key = key.replace('_sign', '').replace('_SIGN', '')

        return key, return_state, return_df

    def _check_key_valid(self, key):

        key, return_state, return_df = self._get_return_state(key)
        if (key not in ['var', 'err_base']) and (key not in self._valid_keys):
            raise KeyError(f"'{key}' must be part of valid keys: {str(list(self._valid_keys.keys()))} or ['err_base', 'var']")

        return key, return_state, return_df

    def _check_if_intermediate(self, key):

        ret_state = False
        if '.' in key:
            keys = key.split('.')
            if len(keys) != 2:
                raise KeyError(f'{keys} must be 2 valid keys.')
            for k in keys:
                if k in ['err_base', 'var']:
                    raise KeyError(f'{keys} must be 2 valid keys, currently {k} in {keys} is invalid')
                fkey, __, __ = self._check_key_valid(k)

            ret_state = True

        return ret_state

    def __getitem__(self, key):

        key, return_state, return_df = self._get_return_state(key)
        # intermediate = self._check_if_intermediate(key) Done in each step instead.

        if return_state == 'err':
            self._feature_err_for(key)
            matrix = self.data[key]

        elif return_state == 'uv':
            matrix = self._uv(key)

        elif return_state == 'erv':
            matrix = self._erv(key)
            matrix = self._threshold(matrix)

        elif return_state == 'mask':
            matrix = self._erv(key)
            matrix = self._mask(matrix)

        elif return_state == 'sign':
            matrix = self._sign(key)

        return self._to_df(matrix, transform=return_df)

    def __setitem__(self, key, value):

        if (key not in ['var', 'err_base']) or (key not in self._valid_keys):
            raise KeyError(f'{key} can not be set directly')
        self.data[key] = value

    def _compute_intermediate_matrix(self, keys):

        skey = keys[1]
        fkey = keys[0]

        berr = self['err_base']
        out_mask = self[skey + '_mask']
        in_mask = self[fkey + '_mask']
        in_err = self[fkey]

        all_base_err = []
        all_input_err = []
        for mas, err in zip(in_mask.T, in_err.T):
            nnz = _torch.mul(out_mask.T, mas).T
            ber = _torch.mul(nnz.T, berr).T
            ier = _torch.mul(nnz.T, err).T

            all_base_err.append(ber.sum(0))
            all_input_err.append(ier.sum(0))
        all_base_err = _torch.stack(all_base_err).T
        all_input_err = _torch.stack(all_input_err).T

        return all_input_err, all_base_err

    def _feature_err_for(self, key):

        intermediate = self._check_if_intermediate(key)

        if intermediate:
            if key not in self.data:
                self.compute_intermediate_loss_for(key)
        else:
            key, __, __ = self._check_key_valid(key)
            if key not in self.data:
                self.compute_feature_loss_for(key)

    def _sign(self, key):
        """Sign of interaction."""
        intermediate = self._check_if_intermediate(key)
        self._feature_err_for(key)

        if intermediate:
            return _torch.sign(self[key].T - self['err_base'].sum()).T
        else:
            return _torch.sign(self[key].T - self['err_base']).T

    def _uv(self, key):
        """Unexplained variance ratio."""

        self._feature_err_for(key)
        return ((self['err_base'] / self.data[key].T).T)

    def _erv(self, key):
        """Relative explained variance"""
        return _torch.nan_to_num(1 - self._uv(key), nan=0.0)

    def _mask(self, erv):

        ervrm, relmax = self._relmax_mask(erv)
        if relmax < 1:
            mask = (ervrm > relmax).int()
        else:
            mask = (ervrm >= relmax).int()

        return mask

    def _to_df(self, matrix, transform=False):

        if not transform:
            return matrix
        else:

            if isinstance(matrix, tuple):
                mm = []
                for m in matrix:
                    m = _pd.DataFrame(m if isinstance(m, _np.ndarray) else m.cpu().numpy())
                    m = m.replace(-0.0, 0.0)
                    mm.append(m)

                mm = tuple(mm)
            else:
                mm = _pd.DataFrame(matrix if isinstance(matrix, _np.ndarray) else matrix.cpu().numpy())
                mm = mm.replace(-0.0, 0.0)

            return mm

    def _relmax_matrix(self, erv):

        ervrm = (erv.T / erv.max(1)[0]).T
        ervrm = _torch.nan_to_num(ervrm, nan=0.0, neginf=0.0)

        return ervrm

    def _relmax_mask(self, erv):

        relmax = self.relmax

        if relmax:
            ervrm = self._relmax_matrix(erv)
        else:
            ervrm = erv
            relmax = 0  # THIS USED TO BE EPS!!!!

        return ervrm, relmax

    def _threshold(self, matrix, eps=None):

        if eps is None:
            eps = self.eps

        if self.threshold:
            matrix[matrix < eps] = 0
        return matrix


def get_valid_layers(model):

    train_nodes, validation_nodes = _get_graph_node_names(model)
    keys = ['.'.join(i.split('.')[:-1]) for i in validation_nodes]
    keys = list(dict.fromkeys(keys))
    if keys[0] == '':
        keys = keys[1:]

    return keys


@_torch.inference_mode()
def prune_layer(model, new_mask, key, fill_zeroed=False, verbose=False):
    """
    Based on a given model and a mask will set the corresponding model parameters to 0
    and exclude them from training.

    Arguments:
        model (supirfactor.models model):
        new_mask (torch.tensor): binary matrix, (Dim; output x input).
        key (str): String identifying the layer to be pruned.
            Available keys can be found by running the function `get_valid_layers(model)`.
        fill_zeroed (bool): If rows that are all zero should be trainable , default False.
        verbose (bool): To print some step information.
    """

    #     key2 (str): if an intermediate layer will determine what layer to prune.
    #         If None (default None), will try to infer from key1.

    module = model.get_submodule(key)

    if _prune.is_pruned(module.linear):
        old_mask = module.linear.weight_mask.data.detach().clone()
        new_mask = _torch.mul(new_mask, old_mask)
        if verbose:
            print('removing previous mask and combining with new mask')
            print(f'Old mask density: {old_mask.sum().item() / _torch.prod(_torch.tensor(old_mask.shape)):.3f}')
            print(f'New mask density: {new_mask.sum().item() / _torch.prod(_torch.tensor(new_mask.shape)).item():.3f}')

        _prune.remove(module.linear, 'weight')

    indx = (new_mask.sum(1) == 0)
    if fill_zeroed and (indx.sum() > 0):
        if verbose:
            print(f'Found zeroed outputs. Filling in {indx.sum()} genes.')
        new_mask[indx, :] = 1

    __ = _prune.custom_from_mask(module.linear, 'weight', new_mask)


def prune_w_swa_loop(key1, model, loader, key2=None, swa_model=None, resamples=3, relmax=0):
    mask = 1

    if swa_model is None:
        raise ValueError("swa_model needs to be provided.")

    for i in range(resamples):
        swa_model.sample(1)
        model_erv = ERV(swa_model.base, loader, relmax=relmax)
        if key2 is None:
            mask = model_erv[f'{key1.split(".")[-1]}_mask'].mul(mask)
        else:
            mask = model_erv[f'{key1.split(".")[-1]}.{key2.split(".")[-1]}_mask'].mul(mask)

    prune_layer(model, mask, key1, verbose=True)
