import numpy as _np
import torch as _torch
from torch import nn as _nn
import time as _time
from supirfactor.swag import utils as _swagu
from .utils import fast_gradient_method as _fast_gradient_method


def fit(model, train_loader, optimizer, scheduler_fn, lossf,
        device=None,
        validation_loader=None,
        scheduler_kwargs={'T_max': 10},
        epochs=100,
        swa_start=0.9,
        swa_model=None,
        swa_scheduler=None,
        swa_scheduler_kwargs={'anneal_strategy': "cos", 'anneal_epochs': 10},
        val_lossf=_nn.MSELoss,
        val_lossf_kwargs={},
        verbose=False,
        ):

    verboseprint = print if verbose else lambda *a, **k: None
    val_lossf = val_lossf(**val_lossf_kwargs)

    if device is None:
        device = next(model.parameters()).device
    else:
        device = device
        model = model.to(device)

    start_time = _time.time()

    lr_scheduler = scheduler_fn(optimizer, **scheduler_kwargs)

    # swa_start = _np.inf
    if swa_model is not None:
        swa_lr = optimizer.param_groups[0]['lr']
        swa_start = int(epochs * swa_start + 1)
        swa_scheduler = swa_scheduler(optimizer, **swa_scheduler_kwargs, swa_lr=swa_lr)

    var = None
    train_loss = []
    # old_tr_loss = _np.inf
    for epoch in range(epochs):

        losses = {}
        losses['epoch'] = epoch

        tr_loss = train_epoch(model, train_loader, optimizer, device, lossf=lossf)

        if (epoch > swa_start) and (swa_model is not None):
            swa_scheduler.step()
            swa_model.collect_model(model)
        else:
            lr_scheduler.step()

        if validation_loader is not None:
            val_loss, R2, var = validation_epoch(model, validation_loader, device, lossf=val_lossf, init_var=var)
        else:
            val_loss, R2, var = (_np.nan, _np.nan, _np.nan)

        train_loss.append([epoch, tr_loss, val_loss, R2])

        if swa_model is not None:
            swa_model.sample(0.0)
            _swagu.bn_update(train_loader, swa_model, device=device)

        _extime = _time.time() - start_time

        curr = _time.ctime()
        verboseprint(f"epoch: {epoch+1}/{epochs}, {((epoch+1)/epochs)*100:.2f}%, t-loss = {tr_loss:6.3g}, v-loss = {val_loss:6.3g}, v-R2 = {R2:4.3g}, run time: {_extime/60:.2f}min, " + curr + ' ' * 5, end='\r')

    return train_loss


def train_epoch(model, data_loader, optimizer, device, lossf=_nn.MSELoss(), profiler=None):
    '''
    Parameters
    ----------
    model: supirfactor.model
        A pytorch model. Specifically a supirfactor model.
    data_loader: SupirFactor dataloader
        description
    optimizer: type
        description
    lossf: type
        description
    device: type
        description
    profiler: type
        description

    '''

    model = model.train()

    loss = _torch.tensor(0., device=device)
    for batch_features in data_loader:
        inputs = batch_features.inp.to(device, non_blocking=True)
        targets = batch_features.tgt.to(device, non_blocking=True)

        # compute reconstructions
        outputs = model(inputs, **batch_features.metadata)

        # compute training reconstruction loss
        train_loss = lossf(outputs, targets)

        optimizer.zero_grad()

        # compute accumulated gradients
        train_loss.backward()

        # perform parameter update based on current gradients
        optimizer.step()

        if profiler is not None:
            profiler.step()

        # add the mini-batch training loss to epoch loss
        loss += train_loss  # .item()

    # compute the epoch training loss
    loss = (loss / len(data_loader)).detach()

    return loss.item()


@_torch.inference_mode()
def validation_epoch(model, data_loader, device, lossf=_nn.MSELoss(), init_var=None, profiler=None, rss=False):

    loss = _torch.tensor(0., device=device)

    if init_var is None:
        var = _torch.tensor(0., device=device)
    else:
        var = init_var

    model = model.eval()

    for batch_features in data_loader:
        inputs = batch_features.inp.to(device, non_blocking=True)
        targets = batch_features.tgt.to(device, non_blocking=True)

        outputs = model(inputs, **batch_features.metadata)

        val_loss = lossf(outputs, targets)

        loss += val_loss

        if init_var is None:
            if not rss:
                var += (targets**2).mean()
            else:
                var = 1

        if profiler is not None:
            profiler.step()

    if not rss:
        loss = (loss / len(data_loader)).detach()
    else:
        loss = loss.detach()

    if init_var is None:
        var = var / len(data_loader)

    R2 = 1 - (loss / var).detach()

    return loss.item(), R2.item(), var


def contrastive_epoch(model, data_loader1, data_loader2, optimizer,
                      device, contrast_on='W', contrast=-1, lossf=_nn.MSELoss(), clossf=_nn.CosineEmbeddingLoss(margin=0.5), profiler=None):
    '''
    Parameters
    ----------
    model (supirfactor.model):
        A pytorch model. Specifically a supirfactor model.
    data_loader1 (SupirFactor dataloader):
        A supirfactor dataloader where the prior key is specified in the metadata.
    data_loader2 (SupirFactor dataloader):
        A supirfactor dataloader where the prior key is specified in the metadata.
        Must be an independant set from data_loader2
    optimizer: type
        description
    device: type
        description
    contrast: (int, dict, pandas.DataFrame): The contrast that should be used between
        tasks. If a dict or DataFrame is passed then each key combination needs to
        specify a specific contrast. If -1 the tasks are considered to be separated,
        if 1 they are considered to be similar.
    lossf: (torch.nn.{LossFunction}): The loss function to be used when optimizing,
        instantiated. Default nn.MSELoss().
    clossf: (torch.nn.{LossFunction}): The loss function to be used for contrastive loss,
        when optimizing, instantiated. Default nn.CosineEmbeddingLoss(margin=0.5).
    profiler: type
        description
    '''

    model = model.train()

    loss = _torch.tensor(0., device=device)
    loss1 = _torch.tensor(0., device=device)
    loss2 = _torch.tensor(0., device=device)
    closs = _torch.tensor(0., device=device)

    for batch_features1 in data_loader1:
        task1 = batch_features1.metadata['priors']

        data_l2 = iter(data_loader2)
        task2 = task1
        while task2 == task1:
            try:
                batch_features2 = next(data_l2)
            except StopIteration as e:
                raise StopIteration("Found no other tasks. Check the provided dataloaders.") from e

            task2 = batch_features2.metadata['priors']

        if isinstance(contrast, (int, float)):
            use_contrast = _torch.tensor(contrast)
        else:
            use_contrast = _torch.tensor(contrast[task1][task2])

        # Forward pass first batch
        inputs = batch_features1.inp.to(device, non_blocking=True)
        targets1 = batch_features1.tgt.to(device, non_blocking=True)
        # compute reconstructions
        outputs1 = model(inputs, **batch_features1.metadata)
        # compute training reconstruction loss
        train_loss1 = lossf(outputs1, targets1)

        if contrast_on is not None:
            outputs1 = model.LA(inputs, **batch_features1.metadata, output=contrast_on)

        # Forward pass second batch
        inputs = batch_features2.inp.to(device, non_blocking=True)
        targets2 = batch_features2.tgt.to(device, non_blocking=True)
        # compute reconstructions
        outputs2 = model(inputs, **batch_features2.metadata)
        # compute training reconstruction loss
        train_loss2 = lossf(outputs2, targets2)

        if contrast_on is not None:
            outputs2 = model.LA(inputs, **batch_features1.metadata, output=contrast_on)

            min_dim = _torch.min(_torch.tensor([outputs1.shape[0], outputs2.shape[0]]))

            con_loss = clossf(outputs1[:min_dim, :].mean(0),
                              outputs2[:min_dim, :].mean(0),
                              use_contrast
                              )
        else:
            con_loss = _torch.tensor(0., device=device)

        train_loss = train_loss1 + train_loss2 + con_loss

        optimizer.zero_grad()
        # compute accumulated gradients
        train_loss.backward()

        # perform parameter update based on current gradients
        optimizer.step()

        if profiler is not None:
            profiler.step()

        # add the mini-batch training loss to epoch loss
        loss += train_loss  # .item()
        loss1 += train_loss1
        loss2 += train_loss2
        closs += con_loss
        # e_count += 1

    # compute the epoch training loss
    loss = (loss / len(data_loader1)).detach()
    loss1 = (loss1 / len(data_loader1)).detach()
    loss2 = (loss2 / len(data_loader1)).detach()
    closs = (closs / len(data_loader1)).detach()

    return loss.item(), closs.item(), loss1.item(), loss2.item()


def fit_jrl_with_contrast(model, train_loader, contrast_loader, optimizer, scheduler_fn, lossf,
                          contrast=-1,
                          contrast_on='W',
                          device=None,
                          validation_loader=None,
                          scheduler_kwargs={'T_max': 10},
                          epochs=100,
                          swa_start=0.9,
                          swa_model=None,
                          swa_scheduler=None,
                          swa_scheduler_kwargs={'anneal_strategy': "cos",
                                                'anneal_epochs': 10},
                          verbose=False,
                          ):
    """
    https://ieeexplore.ieee.org/document/9529219,
    https://www.sciencedirect.com/science/article/pii/S0957417422020693
    """
    verboseprint = print if verbose else lambda *a, **k: None

    if device is None:
        device = next(model.parameters()).device
    else:
        device = device
        model = model.to(device)

    start_time = _time.time()

    lr_scheduler = scheduler_fn(optimizer, **scheduler_kwargs)

    # swa_start = _np.inf
    if swa_model is not None:
        swa_lr = optimizer.param_groups[0]['lr']
        swa_start = int(epochs * swa_start + 1)
        swa_scheduler = swa_scheduler(optimizer, **swa_scheduler_kwargs, swa_lr=swa_lr)

    var = None
    train_loss = []
    for epoch in range(epochs):

        losses = {}
        losses['epoch'] = epoch

        tr_loss, con_loss, tr1_loss, tr2_loss = contrastive_epoch(model, train_loader,
                                                                  contrast_loader,
                                                                  optimizer,
                                                                  device,
                                                                  lossf=lossf,
                                                                  contrast=contrast,
                                                                  contrast_on=contrast_on)

        if (epoch > swa_start) and (swa_model is not None):
            swa_scheduler.step()
            swa_model.collect_model(model)
        else:
            lr_scheduler.step()

        if validation_loader is not None:
            val_loss, R2, var = validation_epoch(model, validation_loader, device, lossf=lossf, init_var=var)
        else:
            val_loss, R2, var = (_np.nan, _np.nan, _np.nan)

        train_loss.append([epoch, tr_loss, con_loss, tr1_loss, tr2_loss, val_loss, R2])

        if swa_model is not None:
            swa_model.sample(0.0)
            _swagu.bn_update(train_loader, swa_model, device=device)

        _extime = _time.time() - start_time

        curr = _time.ctime()
        verboseprint(f"epoch: {epoch+1}/{epochs}, {((epoch+1)/epochs)*100:.2f}%, t-loss = {tr_loss:6.3g}, con-loss = {con_loss:6.3g}, v-loss = {val_loss:6.3g}, v-R2 = {R2:4.3g}, run time: {_extime/60:.2f}min, " + curr + ' ' * 5, end='\r')

    return train_loss


def adv_train_with_fgm(model, data_loader, optimizer, device, lossf=_nn.MSELoss(), profiler=None, **kwargs):
    '''
    Parameters
    ----------
    model (supirfactor.model):
        A pytorch model. Specifically a supirfactor model.
    data_loader (SupirFactor dataloader):
        description
    optimizer: type
        description
    lossf: type
        description
    device: type
        description
    profiler: type
        description
    kwargs: supplied to use fast_gradient_method.

    '''

    model = model.train()

    loss = _torch.tensor(0., device=device)
    for batch_features in data_loader:
        inputs = batch_features.inp.to(device, non_blocking=True)
        targets = batch_features.tgt.to(device, non_blocking=True)

        outputs = model(inputs, **batch_features.metadata)
        train_loss = lossf(outputs, targets)
        optimizer.zero_grad()
        train_loss.backward()
        optimizer.step()

        # add the mini-batch training loss to epoch loss
        loss += train_loss

        xs = _fast_gradient_method(model, inputs, y=targets,
                                   metadata=batch_features.metadata, loss_fn=lossf, **kwargs)
        outputs = model(xs, **batch_features.metadata)
        train_loss = lossf(outputs, targets)
        optimizer.zero_grad()
        train_loss.backward()
        optimizer.step()

        # add the mini-batch training loss to epoch loss
        loss += train_loss

    # compute the epoch training loss
    loss = (loss / (2 * len(data_loader))).detach()

    return loss.item()


@_torch.inference_mode()
def compute_out_feature_loss(model, data_loader, device, lossf=_nn.MSELoss(reduction='none'), init_var=None, silence=None):
    """
    Compute gene wise loss and variance.

    output:
    loss, rss, var

    R2 can be computed as 1 - (loss / var)
    """

    model = model.eval()

    loss = 0
    rss = 0

    if init_var is None:
        var = 0
    else:
        var = init_var

    if silence is None:
        silence = (None, None)

    for batch_features in data_loader:

        inputs = batch_features.inp.to(device, non_blocking=True)
        targets = batch_features.tgt.to(device, non_blocking=True)

        outputs = model(inputs, mask_input=silence[1], on=silence[0], **batch_features.metadata)

        if isinstance(outputs, tuple):
            outputs = outputs[0]

        val_loss = lossf(outputs, targets)

        loss += val_loss.mean(0)
        rss += val_loss.sum(0)

        if init_var is None:
            if loss.dim() > 0:
                var += (targets**2).sum(0)
            else:
                var += (targets**2).mean()

    loss = (loss / len(data_loader)).detach()

    if loss.dim() > 0 and (init_var is None):
        var = (var / len(data_loader)).detach()

    return loss, rss.detach(), var


def fit_w_mirror(model, train_loader, optimizer, scheduler_fn, lossf,
                 device=None,
                 validation_loader=None,
                 scheduler_kwargs={'T_max': 10},
                 epochs=100,
                 swa_start=0.9,
                 swa_model=None,
                 swa_scheduler=None,
                 swa_scheduler_kwargs={'anneal_strategy': "cos", 'anneal_epochs': 10},
                 val_lossf=_nn.MSELoss,
                 val_lossf_kwargs={},
                 verbose=False,
                 ):

    verboseprint = print if verbose else lambda *a, **k: None
    val_lossf = val_lossf(**val_lossf_kwargs)

    if device is None:
        device = next(model.parameters()).device
    else:
        device = device
        model = model.to(device)

    start_time = _time.time()

    lr_scheduler = scheduler_fn(optimizer, **scheduler_kwargs)

    # swa_start = _np.inf
    if swa_model is not None:
        swa_lr = optimizer.param_groups[0]['lr']
        swa_start = int(epochs * swa_start + 1)
        swa_scheduler = swa_scheduler(optimizer, **swa_scheduler_kwargs, swa_lr=swa_lr)

    var = None
    train_loss = []
    # old_tr_loss = _np.inf
    for epoch in range(epochs):

        losses = {}
        losses['epoch'] = epoch

        tr_loss, mirr_loss = train_epoch_w_mirror(model, train_loader, optimizer, device, lossf=lossf)

        if (epoch > swa_start) and (swa_model is not None):
            swa_scheduler.step()
            swa_model.collect_model(model)
        else:
            lr_scheduler.step()

        if validation_loader is not None:
            val_loss, R2, var = validation_epoch_w_mirror(model, validation_loader, device, lossf=val_lossf, init_var=var)
        else:
            val_loss, R2, var = (_np.nan, _np.nan, _np.nan)

        train_loss.append([epoch, tr_loss, val_loss, R2])

        if swa_model is not None:
            swa_model.sample(0.0)
            _swagu.bn_update(train_loader, swa_model, device=device)

        _extime = _time.time() - start_time

        curr = _time.ctime()
        verboseprint(f"epoch: {epoch+1}/{epochs}, {((epoch+1)/epochs)*100:.2f}%, t-loss = {tr_loss:6.3g}, m-loss = {mirr_loss:6.3g}, v-loss = {val_loss:6.3g}, v-R2 = {R2:4.3g}, run time: {_extime/60:.2f}min, " + curr + ' ' * 5, end='\r')

    return train_loss


def train_epoch_w_mirror(model, data_loader, optimizer, device, lossf=_nn.MSELoss(), profiler=None):
    '''
    Parameters
    ----------
    model: supirfactor.model
        A pytorch model. Specifically a supirfactor model.
    data_loader: SupirFactor dataloader
        description
    optimizer: type
        description
    lossf: type
        description
    device: type
        description
    profiler: type
        description

    '''

    model = model.train()

    loss = _torch.tensor(0., device=device)
    mloss = _torch.tensor(0., device=device)
    for batch_features in data_loader:
        inputs = batch_features.inp.to(device, non_blocking=True)
        targets = batch_features.tgt.to(device, non_blocking=True)

        # compute reconstructions
        outputs, mirr_out = model(inputs, **batch_features.metadata)

        # compute training reconstruction loss
        train_loss = lossf(outputs, targets)

        mirr_loss = lossf(mirr_out[:, model.get_reporters()], targets[:, model.get_reporters()])

        tot_loss = train_loss + mirr_loss

        optimizer.zero_grad()

        # compute accumulated gradients
        tot_loss.backward()

        # perform parameter update based on current gradients
        optimizer.step()

        if profiler is not None:
            profiler.step()

        # add the mini-batch training loss to epoch loss
        loss += train_loss  # .item()
        mloss += mirr_loss  # .item()

    # compute the epoch training loss
    loss = (loss / len(data_loader)).detach()
    mloss = (mloss / len(data_loader)).detach()

    return loss.item(), mloss.item()


@_torch.inference_mode()
def validation_epoch_w_mirror(model, data_loader, device, lossf=_nn.MSELoss(), init_var=None, profiler=None, rss=False):

    loss = _torch.tensor(0., device=device)

    if init_var is None:
        var = _torch.tensor(0., device=device)
    else:
        var = init_var

    model = model.eval()

    for batch_features in data_loader:
        inputs = batch_features.inp.to(device, non_blocking=True)
        targets = batch_features.tgt.to(device, non_blocking=True)

        outputs, mirr_out = model(inputs, **batch_features.metadata)

        val_loss = lossf(outputs, targets)

        loss += val_loss

        if init_var is None:
            if not rss:
                var += (targets**2).mean()
            else:
                var = 1

        if profiler is not None:
            profiler.step()

    if not rss:
        loss = (loss / len(data_loader)).detach()
    else:
        loss = loss.detach()

    if init_var is None:
        var = var / len(data_loader)

    R2 = 1 - (loss / var).detach()

    return loss.item(), R2.item(), var

