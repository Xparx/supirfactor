import os as _os
import torch as _torch
import pandas as _pd
import numpy as _np
import scipy as _sp
from anndata import AnnData as _AnnData, concat as _adata_concat
from torch import Tensor
from torch.utils.data import Dataset
from . import features as sfpp
from .. import _DTYPE
from . import shuffle2d
import time as _time
from . import sparse_data as _sd


_PREPROCESSOR = 'RobustMinScaler'
_PREPROCESSORKW = {'with_centering': False,
                   'quantile_range': (1, 99),
                   'unit_variance': True}
_SEED = int(''.join(str(_time.time()).split('.')))


def split_samples(data, validation_size=0.5, test_size=0.0, rng=_np.random.default_rng(_SEED), use_geosketch=False, **kwargs):
    """
    Function splits range of samples into training, validation and test sets.

    Args:
        data: any dataformat that has the shape atribute with dimension 0 as samples.
        validation_size (float): [0, 1[ fraction of samples in the validation set (default 0.5).
        test_size (float): [0, 1[ fraction of samples in the test set (default 0.0).
        rng (np.random.default_rng): a random number generator state (default np.random.default_rng(42)).
        use_geosketch (bool): If true will use geosketch to split train and validation.
            For this test sets will not be availible.
        kwargs: Will be sent to geosketch.

    Returns:
        {'_train': train, '_validate': validate, '_test': test}:
            samples assigned to each set as a dictionary.
    """

    samples = _np.arange(data.shape[0])
    if use_geosketch and (validation_size != 0):
        from ..external.utils import geosketch
        validate = geosketch(data, num_cells=validation_size, return_index=True, **kwargs)
        train = _np.delete(samples, validate)
        validate = samples[validate]
        test = _np.array([])
    else:
        rng.shuffle(samples)

        train, validate, test = _np.split(samples, [int((1 - validation_size - test_size) * len(samples)), int((1 - test_size) * len(samples))])

    return {'_train': train, '_validate': validate, '_test': test}


def setup_loaders(data, batch_size,
                  use_layer=None,
                  Y=None,
                  priors=None,
                  epimask=None,
                  _train=None,
                  _validate=_np.array([]),
                  _test=_np.array([]),
                  shuffle_train=True,
                  rng=_np.random.default_rng(_SEED),
                  randomize_data=False,
                  shuffle_samples=False,
                  bootstrap=False,
                  device=None,
                  preprocessor=_PREPROCESSOR,
                  preprocessorkw=_PREPROCESSORKW,
                  num_workers=0,
                  verbose=False,
                  astype=_DTYPE):
    """

    Arguments:
        data (pandas DataFrame, AnnData, numpy array):
            The data container. Ordered (samples x features).
        batch_size (int): Batch size
    Optional:
        use_layer (str, None): If another layer than X should be used if data is an AnnData object (default None).
        priors (numpy.array, list): Sample to prior mapping.
        epimask: matrix used to mask X. default None
        _train (np.array, None): what samples should be used for training, if None use
            all samples, default None.
        _validate (np.array): what samples to use for validation, default _np.array([]).
        _test (np.array): what samples to use for testing, default _np.array([]) (currently unused).
        shuffle_train (bool): Should the training data be fed in random order to the model
            during training. Needs to be set to false to align with annotations if used in
            post processing (default True).
        rng (numpy.random.default_rng): provided random number generator,
            default _np.random.default_rng(_SEED).
        randomize_data (bool): Should the data be randomized, default False.
        shuffle_samples (bool): default False,
        bootstrap (bool): Should samples be bootstrapped, default False.
        device (torch.device, None): what device should the data be put on.
            If None (reccomended) the batches will be handled at runtime, default None,
        preprocessor (str): see supirfactor.setup.features module for options.
            Outside of a few options these are the methods availible from the
            sklearn.preprocessing module (default 'RobustMinScaler').
        preprocessorkw (dict): Key word arguments for the preprocessor (default
            {'with_centering': False, 'quantile_range': (1, 99)}).
        num_workers (int): Number of workers used to feed data (default 0 (reccomended)).
        astype (dtype): What dtype to use for data, (default torch.float32).

    Returns:
        (train_loader, validation_loader, test_loader): supirfactor data loaders.
    """

    X, priors, epimask = _get_layer_data(data, priors=priors, use_layer=use_layer, epimask=epimask, shuffle_samples=shuffle_samples, copy=True)

    X = X.A if _sp.sparse.issparse(X) else X
    if epimask is not None:
        epimask = epimask.A if _sp.sparse.issparse(epimask) else X

    if randomize_data:
        X = shuffle2d(X, rng=rng)

    Xval, priorsv, epimaskv = handle_preprocess(X, _validate, Y=Y, preprocessor=preprocessor, priors=priors, epimask=epimask, verbose=verbose, **preprocessorkw)
    validation_loader = None
    if Xval is not None:
        validation_loader = GenerateDataFeed(Xval, priors=priorsv, epimask=epimaskv, device=device, batch_size=max(int(batch_size), 1), pin_memory=True, num_workers=num_workers, shuffle=False)

    Xtest, priorst, epimaskt = handle_preprocess(X, _test, Y=Y, preprocessor=preprocessor, priors=priors, epimask=epimask, verbose=verbose, **preprocessorkw)
    test_loader = None
    if Xtest is not None:
        test_loader = GenerateDataFeed(Xtest, priors=priorst, epimask=epimaskt, device=device, batch_size=max(int(batch_size), 1), pin_memory=True, num_workers=num_workers, shuffle=False)

    if _train is None:
        _train = _np.arange(X.shape[0])
    X, priors, epimask = handle_preprocess(X, _train, Y=Y, preprocessor=preprocessor, priors=priors, epimask=epimask, verbose=verbose, **preprocessorkw)

    if bootstrap:
        bs = rng.integers(0, X.shape[0], X.shape[0])
        X = X[bs, :]

        if priors is not None:
            priors = priors[bs]
        if epimask is not None:
            epimask = epimask[bs, :]

    train_loader = None
    if X is not None:
        train_loader = GenerateDataFeed(X, priors=priors, epimask=epimask, device=device, batch_size=max(int(batch_size), 1), pin_memory=True, num_workers=num_workers, shuffle=shuffle_train)

    return train_loader, validation_loader, test_loader


def _get_layer_data(data, priors=None, use_layer=None, epimask=None, shuffle_samples=False, rng=_np.random.default_rng(_SEED), copy=True):
    """
    Parse input data depending on format and returns a numpy array.

    """

    if isinstance(data, _AnnData):
        if (use_layer == 'X') or (use_layer is None):
            X = data.X.copy()
        elif use_layer == 'raw':
            X = data.raw.X.copy()
        else:
            X = data.layers[use_layer].copy()

        if epimask is not None:
            if isinstance(epimask, str):
                epimask = data.layers[epimask].copy()

    elif isinstance(data, _pd.DataFrame):
        X = data.values.copy()

    else:
        X = data.copy()

    if epimask is not None:
        if isinstance(epimask, _pd.DataFrame):
            epimask = epimask.values.copy()

    if shuffle_samples:
        s = _torch.arange(X.shape[0])
        rng.shuffle(s)

        X = X[s, :]
        if epimask is not None:
            epimask = epimask[s, :]

        if priors is not None:
            priors = priors[s]

    return X, priors, epimask


def handle_preprocess(X, samples,
                      Y=None,
                      preprocessor=None,
                      priors=None,
                      epimask=None,
                      verbose=False,
                      **preprocessorkw):

    if samples.size == 0:
        return (None, None, None)

    if verbose:
        print(f"Using {preprocessor}, with kw: {preprocessorkw}")
    if isinstance(preprocessor, str):
        if hasattr(sfpp, preprocessor):
            preprocessor = getattr(sfpp, preprocessor)
        else:
            raise ValueError(f"{preprocessor} does not exist in the supirfactor.setup.features module.")

    if preprocessor is not None:
        scaler = preprocessor(**preprocessorkw).fit(X[samples, :].A.copy() if _sp.sparse.issparse(X) else X[samples, :].copy())

        X = scaler.transform(X[samples, :].A.copy() if _sp.sparse.issparse(X) else X[samples, :].copy())
        if Y is not None:
            Y = scaler.transform(Y[samples, :].A.copy() if _sp.sparse.issparse(Y) else Y[samples, :].copy())

    else:
        X = X[samples, :].A.copy() if _sp.sparse.issparse(X) else X[samples, :].copy()
        if Y is not None:
            Y = Y[samples, :].A.copy() if _sp.sparse.issparse(Y) else Y[samples, :].copy()

    priors_out = None
    epimask_out = None
    if priors is not None and (samples.size > 0):
        priors = _np.array(priors)
        priors_out = priors[samples]
    if epimask is not None and (samples.size > 0):
        epimask_out = epimask[samples, :].copy()

    if Y is None:
        return (X, priors_out, epimask_out)
    else:
        return ((X, Y), priors_out, epimask_out)


def feature_pp(X, preprocessor=None, verbose=False, **preprocessorkw):

    if verbose:
        print(f"Using {preprocessor}, with kw: {preprocessorkw}")
    if isinstance(preprocessor, str):
        if hasattr(sfpp, preprocessor):
            preprocessor = getattr(sfpp, preprocessor)
        else:
            raise ValueError(f"{preprocessor} does not exist in the supirfactor.setup.features module.")

    if preprocessor is not None:
        scaler = preprocessor(**preprocessorkw).fit(X)

        X = scaler.transform(X)

    return X


def task_data_loaders(data, tasks, batch_size=256, **kwargs):
    """
    Generates a task data_loader with ordered samples in each task.
    This can be useful when computing embeddings related to sub tasks.

    Parameters
    ----------
    data (AnnData, DataFrame):
        An anndata or pandas dataframe object.
    tasks (list, Array, Series):
        A list, numpy array, or pandas Series.
    batch_size (int): batch size (defualt 256).
    kwargs: key value pairs,
        Additional parameters for setting up loaders.

    """
    labels, indxs = _np.unique(tasks, return_inverse=True)

    for lab, ix in zip(labels, range(labels.shape[0])):

        samples = _np.where(indxs == ix)[0]
        loader, __, __ = setup_loaders(data,
                                       batch_size,
                                       _train=samples,
                                       shuffle_train=False,
                                       **kwargs)

        yield lab, loader


def post_processing_loader(data, batch_size=256, shuffle_train=False, **kwargs):
    """The post processing loader setup use some defaults that can be useful when
        embedding new data that needs to maintain order among samples.

    Parameters
    ----------
    data: DataFrame, AnnData
        A dataframe or anndata object to generate loader from.
    batch_size: int (default: 256)
        The batch size.
    shuffle_train: bool (default: False)
        if data should be shuffled when used. Should be kept as False for relible results.
    kwargs: key, value
        Key value pairs passed to the setup loader function.

    Returns
    ----------
        description:
        type:
    """

    samples = _np.arange(data.shape[0])
    loader, __val, __test = setup_loaders(data, batch_size, _train=samples, shuffle_train=shuffle_train, **kwargs)

    return loader, __val, __test


class GenerateDataFeed(object):
    """Documentation for DataFeeder
    https://stackoverflow.com/questions/60725571/batches-of-points-with-the-same-label-on-pytorch

    """
    def __init__(self, X, batch_size=256, device=None, astype=_DTYPE, priors=None, epimask=None, shuffle=True, num_workers=0, pin_memory=False):
        super().__init__()

        if isinstance(priors, list):
            priors = _np.array(priors)
        self.priors = priors

        epimask_t = None
        if epimask is not None:
            epimask_t = _torch.tensor(epimask, device=device, dtype=astype)

        if isinstance(X, tuple):
            for x in X:
                n = x.shape[0]
                self._N = n
                break
        else:
            n = X.shape[0]
            self._N = n
            X = tuple((X, ))

        dataset = SupirFactorDataset(*(_torch.tensor(x, device=device, dtype=astype) for x in X), priors=priors, epimask=epimask_t)

        if priors is not None:
            assert (n == len(priors))

            priors_contexts = _np.unique(priors)
            class_inds = [_np.where(priors == context)[0] for context in priors_contexts]

            loaders = [
                _torch.utils.data.DataLoader(
                    dataset=_torch.utils.data.Subset(dataset, inds),
                    batch_size=int(batch_size),
                    shuffle=shuffle,
                    num_workers=num_workers,
                    pin_memory=pin_memory,
                    collate_fn=sf_collate_wrapper,
                )
                for inds in class_inds]

            self._N_batches = _np.sum([len(loader) for loader in loaders])

        else:
            loaders = _torch.utils.data.DataLoader(dataset, batch_size=int(batch_size), shuffle=shuffle, num_workers=num_workers, pin_memory=pin_memory, collate_fn=sf_collate_wrapper)
            self._N_batches = len(loaders)

        self.loaders = loaders

    def __iter__(self):

        if isinstance(self.loaders, list):
            self.loader = list(map(iter, self.loaders))
        else:
            self.loader = iter(self.loaders)

        return self

    def __next__(self):

        iterators = self.loader
        if isinstance(iterators, list):
            while iterators:
                iterator = _np.random.choice(iterators)
                try:
                    return next(iterator)

                except StopIteration:
                    iterators.remove(iterator)

            raise StopIteration

        else:
            return next(iterators)

        raise StopIteration

    def __len__(self):
        # return self._N
        return self._N_batches


class SupirFactorDataset(Dataset):
    """A pytorch Dataset class specifically designed for SupirFactor inputs.

    Assigns values for prior per sample if provided.

    """

    def __init__(self, *tensors: Tensor, priors=None, epimask=None) -> None:
        assert all(tensors[0].size(0) == tensor.size(0) for tensor in tensors), "Size mismatch between tensors."

        if epimask is not None:
            assert (tensors[0].size(0) == epimask.shape[0]), "Size mismatch between tensors and epimask."
            if len(tensors) > 1:
                tensors = tuple([_torch.mul(tensors[0].clone().detach(), epimask.clone().detach()), tensors[1].clone().detach()])
            else:
                tensors = tuple([_torch.mul(tensors[0].clone().detach(), epimask.clone().detach()), tensors[0].clone().detach()])

        self.tensors = tensors
        self.samples = _np.arange(tensors[0].size(0))

        self.priors = None
        if priors is not None:
            assert (tensors[0].size(0) == len(priors)), "Size mismatch between tensors and priors metadata."
            self.priors = priors

    def __getitem__(self, index):
        data = tuple(tensor[index] for tensor in self.tensors)

        metadata = (self.priors[index] if self.priors is not None else None, self.samples[index])
        return data, metadata

    def __len__(self):
        return self.tensors[0].size(0)


class CustomBatch:
    """
    inps = torch.arange(10 * 5, dtype=torch.float32).view(10, 5)
    tgts = torch.arange(10 * 5, dtype=torch.float32).view(10, 5)
    dataset = SupirFactorDataset(inps, tgts, priors=None)

    loader = DataLoader(dataset, batch_size=2, collate_fn=collate_wrapper, pin_memory=True)

    for batch_ndx, sample in enumerate(loader):
        print(sample.inp.is_pinned())
        print(sample.tgt.is_pinned())

    """

    def __init__(self, data):

        metadata = {'priors': None}

        samples_idx = []
        priors = []
        inp = []
        tgt = []
        for sample in data:
            if len(sample[0]) == 1:
                ip = sample[0][0]
                tg = ip
            if len(sample[0]) == 2:
                ip, tg = sample[0]

            inp.append(ip)
            tgt.append(tg)

            if sample[1][0] is None:
                priors = None
            else:
                priors.append(sample[1][0])

            if sample[1][1] is None:
                samples_idx = None
            else:
                samples_idx.append(sample[1][1])

        inp = _torch.stack(inp)
        tgt = _torch.stack(tgt)
        metadata['priors'] = self._check_prior(priors)

        self.inp = inp
        self.tgt = tgt
        self.metadata = metadata
        self.sample_idx = samples_idx

    def _check_prior(self, priors):

        # print(priors, len(priors))
        if priors is not None:
            if isinstance(priors, (_np.ndarray, list, Tensor)):
                if all(x == priors[0] for x in priors):
                    priors = priors[0]
                else:
                    import warnings
                    warnings.warn("All submitted prior IDs in batch do not match. Using default prior: None.")
                    priors = None

        return priors

    # custom memory pinning method on custom type
    def pin_memory(self):
        self.inp = self.inp.pin_memory()
        self.tgt = self.tgt.pin_memory()
        return self


def sf_collate_wrapper(batch):
    return CustomBatch(batch)


def load_expression_data(data_path):
    """
    load an expression dataset from path `data_path`.
    Assumes genes on columns and sample rows.
    """

    from anndata import read_h5ad

    if data_path.endswith(('.tsv.gz', '.tsv')):
        sep = '\t'
        data = _pd.read_csv(data_path, sep=sep, index_col=0).astype(_np.float32)
        data = _AnnData(data, dtype=data.values.dtype)

    elif data_path.endswith(('.csv.gz', '.csv')):
        sep = ','
        data = _pd.read_csv(data_path, sep=sep, index_col=0).astype(_np.float32)
        data = _AnnData(data, dtype=data.values.dtype)

    elif data_path.endswith(('.h5ad')):
        sep = None
        data = read_h5ad(data_path)

    else:
        print(f'error, unsuported file type for file {_os.path.split(data_path)[-1]}\nSupported file endings are (".tsv", ".csv", ".h5ad")')

    return data


def normalize_per_task(adata, tasks,
                       preprocessor=_PREPROCESSOR,
                       preprocessorkw=_PREPROCESSORKW,
                       resparsify=True,
                       ):

    """
    Helper function for splitting an adata object and normalize each task
    then stitching it back toghether again.

    Arguments:
        adata (AnnData): the data.
        tasks (str): The label in obs that the data should be split on.
        preprocessor (str): the preprocessor to use,
            Default supirfactor.setup.dataloader._PREPROCESSOR.
        preprocessorkw (dict): the key word arguments for the preprocessor
            Default supirfactor.setup.dataloader._PREPROCESSORKW.
        resparsify (bool): if convert back to sparse if original data was sparse,
            Default True.
    """

    from scipy.sparse import csr_matrix

    var = adata.var.copy()
    new_adata = []
    for k, df in adata.obs.groupby(tasks):
        subd = adata[df.index, :].copy()

        was_sparse = _sp.sparse.issparse(subd.X)
        if was_sparse:
            X = subd.X.A.copy()
        else:
            X = subd.X.copy()

        X, __, __ = handle_preprocess(X, _np.arange(X.shape[0]), preprocessor=preprocessor, **preprocessorkw)

        if was_sparse & resparsify:
            X = csr_matrix(X)

        subd.layers['localnorm'] = X

        new_adata.append(subd)

    new_adata = _adata_concat(new_adata, axis=0, join='outer')
    new_adata.var = var

    return new_adata


def setupMMloaders(gene, peak, batch_size,
                   use_layer=None,
                   Y=None,
                   priors=None,
                   epimask=None,
                   _train=None,
                   _validate=_np.array([]),
                   _test=_np.array([]),
                   shuffle_train=True,
                   to_sparse=False,
                   rng=_np.random.default_rng(_SEED),
                   randomize_data=False,
                   shuffle_samples=False,
                   bootstrap=False,
                   device=None,
                   num_workers=0,
                   verbose=False,
                   astype=_DTYPE):
    """
    TODO!
    """

    validation_loader = None
    if _validate.size != 0:
        dataset = MMDataset(gene[_validate, :], peak[_validate, :], to_sparse=to_sparse)
        validation_loader = _torch.utils.data.DataLoader(dataset, batch_size=batch_size, collate_fn=sf_collate_mm_wrapper, pin_memory=True, shuffle=False)

    test_loader = None
    if _test.size != 0:
        dataset = MMDataset(gene[_test, :], peak[_test, :], to_sparse=to_sparse)
        validation_loader = _torch.utils.data.DataLoader(dataset, batch_size=batch_size, collate_fn=sf_collate_mm_wrapper, pin_memory=True, shuffle=False)

    if _train is None:
        _train = _np.arange(gene.shape[0])
    dataset = MMDataset(gene[_train, :], peak[_train, :], to_sparse=to_sparse)
    train_loader = _torch.utils.data.DataLoader(dataset, batch_size=batch_size, collate_fn=sf_collate_mm_wrapper, pin_memory=True, shuffle=shuffle_train)

    return train_loader, validation_loader, test_loader


class MMDataset(Dataset):
    """
    Documentation for SparseMMDataset

    """

    def __init__(self, stensor1, stensor2, **kwargs):
        super().__init__()

        # self.tinput = _sd.convert2sparse_tensor(stensor1, **kwargs).to_dense()
        # self.ttarget = _sd.convert2sparse_tensor(stensor2, **kwargs).to_dense()
        X = _sd.convert2sparse_tensor(stensor1, **kwargs)
        X = feature_pp(X.numpy(), preprocessor=_PREPROCESSOR, verbose=True, **_PREPROCESSORKW)

        self.tinput = _torch.tensor(X)
        self.ttarget = _sd.convert2sparse_tensor(stensor2, **kwargs)

        assert (self.ttarget.shape[0] == self.tinput.shape[0]), "Number of samples in gene data does not match to number of samples in peak."

        self.n_samples = self.tinput.shape[0]
        self.samples = _np.arange(self.n_samples)

    def __getitem__(self, index):
        data = tuple([self.tinput[index], self.ttarget[index]])

        return data, self.samples[index]

    def __len__(self):
        return self.n_samples


def sf_collate_mm_wrapper(batch):
    return CustomMMBatch(batch)


class CustomMMBatch:
    """
    dataset = MMDataset(gene, peak)

    loader = DataLoader(dataset, batch_size=2, collate_fn=sf_collate_mm_wrapper, pin_memory=True)

    for batch_ndx, sample in enumerate(loader):
        print(sample.inp.is_pinned())
        print(sample.tgt.is_pinned())

    """

    def __init__(self, data):

        samples_idx = []
        inp = []
        tgt = []
        for (gene, peak), indx in data:
            inp.append(gene)
            tgt.append(peak)
            samples_idx.append(indx)

        # inp = _scat(inp, 1)
        # tgt = _scat(tgt, 1)
        self.inp = _torch.stack(inp)
        self.tgt = _torch.stack(tgt)

        # self.inp = inp.to_dense()
        # self.tgt = tgt.to_dense()
        self.sample_idx = samples_idx

    # custom memory pinning method on custom type
    def pin_memory(self):
        self.inp = self.inp.pin_memory()
        self.tgt = self.tgt.pin_memory()
        return self
