from sklearn.preprocessing import *
from sklearn.preprocessing import RobustScaler, Normalizer
from sklearn.base import BaseEstimator, TransformerMixin
import numpy as _np


class RobustMinScaler(RobustScaler):
    """Applies the RobustScaler and then adjust minimum value to 0.

    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def fit(self, X, y=None):

        super().fit(X, y=y)

        # self.min_ = 0 - data_min * self.scale_

        return self

    def transform(self, X):

        X = super().transform(X)
        data_min = _np.nanmin(X, axis=0)
        X -= data_min
        return X


class FNormalizer(Normalizer):
    """Same as sklearn Normalizer but feature wise

    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def fit(self, X, y=None):

        super().fit(X.T, y=y)

        return self

    def transform(self, X):

        X = super().transform(X.T).T
        return X


class Expm1(TransformerMixin, BaseEstimator):
    """A wrapper for numpy.expm1 to be used as a preprocessing tool.

    """
    def __init__(self, **kwargs):
        super().__init__()

    def fit(self, X, y=None):

        return self

    def transform(self, X):

        return _np.expm1(X)
