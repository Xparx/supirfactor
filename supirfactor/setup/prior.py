import os as _os
import warnings as _warnings
import pandas as _pd
import scanpy as _sc
import numpy as _np
from . import shuffle2d as _randomize
import time as _time
_SEED = int(''.join(str(_time.time()).split('.')))


def norm_priors(prior, factor=2, nom='sum'):

    def normal_col(prior, factor, nom):
        if nom == 'sum':
            return (prior / prior.sum(0) / factor).fillna(0)
        elif nom == 'norm':
            return (prior / _np.linalg.norm(prior, axis=0) / factor).fillna(0)

    if isinstance(prior, dict):
        for k, v in prior.items():
            prior[k] = normal_col(v, factor, nom)
    else:
        prior = normal_col(prior, factor, nom)

    return prior


def split(prior,
          gold_standard=None,
          cv_prior=False,
          fraction_gs=0.1,
          prior_map=None,
          randomize_prior=False,
          rng=_np.random.default_rng(_SEED),
          ):
    """
    Setup up a cross validation set of prior information. If provided a gold standard, will make sure there are no cross-talk between gold-standard and prior.

    Args:
        prior (pandas.DataFrame): (Genes x TFs), (rows, cols).
        gold_standard (pandas.DataFrame): (Genes x TFs), (rows, cols).
        cv_prior (bool): Should genes be taken out of the prior (Default False).
        fraction_gs (float): What fraction of genes should be held out if cv_prior=True, (Default 0.1).
        prior_map (dict, None): If dict will assume tasks as dict keys and priors as dict values for each task, Default None.
        randomize_prior (bool): Should we randomize the prior, default False.
        rng (np.random.rng): The random state that should be used for randomness, Default np.random.default_rng(_SEED).
            With _SEED being int(''.join(str(time.time()).split('.'))).
    Returns:
        prior (pandas.DataFrame): (Genes x TFs), (rows, cols). A prior without selected rows from gs.
        gs (pandas.DataFrame): (Genes x TFs), (rows, cols). A gold_standard with only elements not in the prior.
        use_prior_map (dict): a dictionary of priors with elements from the gold standard removed.

    """

    gs = gold_standard.copy() if gold_standard is not None else gold_standard

    if cv_prior:
        prior, gs = _split_prior(prior, gold_standard=gs, rng=rng, fraction_gs=fraction_gs)
    else:
        if gs is not None:  # TODO: Create a function that aligns the gs and prior columns. This needs to be done as the prior might have been merged. So gs needs to be merged as the prior.
            gs = gs.reindex_like(prior).fillna(0)
            gs = gs.iloc[(gs.sum(1) != 0).tolist(), (gs.sum(0) != 0).tolist()].copy()

    use_prior_map = fix_multi_prior_split(gs, prior_map, cv_prior=cv_prior, randomize_prior=randomize_prior, fraction_gs=fraction_gs)

    if randomize_prior:
        prior = _randomize(prior)

    return prior, gs, use_prior_map


def _split_prior(prior,
                 gold_standard=None,
                 fraction_gs=0.1,
                 rng=_np.random.default_rng(_SEED)):
    """
    Splits a prior into a training and a validation set by gene (rows) while maintaining
    at least 1 connection / TF.
    """

    if (prior.astype(bool).sum(0) == 0).sum() != 0:
        _warnings.warn('Prior have 0s in TF, can not split.\nReturning without change')

        return prior.copy(), gold_standard

    if fraction_gs == 0 and (gold_standard is not None):
        # Only use non-overlapping links for gold standard
        use_prior, use_gs = _gold_standard_from_difference(prior, gold_standard)

        return use_prior, use_gs

    use_prior = prior.loc[(prior.astype(bool).sum(1) != 0), :].copy()

    rows_prior, rows_gs = _find_full_rank_split(use_prior, fraction_gs=fraction_gs, rng=rng)

    if gold_standard is not None:
        use_gs = gold_standard.reindex_like(use_prior).fillna(0).astype(bool).astype(int)
    else:
        use_gs = use_prior.astype(bool).astype(int).copy()

    indices = use_prior.index

    use_gs.loc[indices[rows_prior], :] = 0
    use_gs = use_gs.loc[use_gs.astype(bool).sum(1) > 0, use_gs.astype(bool).sum(0) > 0].copy()
    use_prior.loc[indices[rows_gs], :] = 0

    if use_prior.loc[use_gs.index, :].astype(bool).sum().sum() > 0:
        _warnings.warn('Overlap between prior and gold standard exist.')

    if (use_prior.sum(0) == 0).sum():
        _warnings.warn('Prior is not full rank. Something is wrong!')

    use_prior = use_prior.reindex(index=prior.index, fill_value=0)

    return use_prior, use_gs


def _find_full_rank_split(use_prior, fraction_gs=0.1, rng=_np.random.default_rng(_SEED)):

    rows, cols = use_prior.shape
    the_rows = rows * fraction_gs

    numzero = 1
    counter = 0
    while numzero > 0:

        rowids = _np.arange(rows)
        rng.shuffle(rowids)

        rows_gs = sorted(rowids[:int(the_rows)])
        rows_prior = sorted(rowids[int(the_rows):])

        tmp = use_prior.copy()
        tmp.iloc[rows_gs, :] = 0
        numzero = (tmp.astype(bool).sum(0) == 0).sum()

        counter += 1

        if not (counter % 100):
            print(f'Attemts to create full prior: {counter}', end='\r')

    return rows_prior, rows_gs


def fix_multi_prior_split(gold_standard, prior_map, cv_prior=False, randomize_prior=False, fraction_gs=0.1):

    if prior_map is None:
        prior_map = {}

    if isinstance(prior_map, dict):
        use_prior_map = {}
        for k, v in prior_map.items():
            subp = v.copy()
            if cv_prior:
                if fraction_gs > 0:
                    nnz_rows = gold_standard.index[(gold_standard.astype(bool).sum(1) > 0)].tolist()
                    subp.loc[nnz_rows, :] = 0
                else:
                    gsame = gold_standard.reindex_like(subp)
                    subp = subp * ((subp.astype(bool).astype(int) - gsame.astype(bool).astype(int)) > 0)
            if randomize_prior:
                subp = _randomize(subp)
            use_prior_map[k] = subp
    elif isinstance(prior_map, (set, list, tuple)):
        use_prior_map = prior_map

    return use_prior_map


def _gold_standard_from_difference(prior, gold_standard):

    # raise NotImplementedError('This function is not correct yet.')

    tmp_prior = prior.copy().reindex_like(gold_standard).fillna(0)
    use_gs = gold_standard.copy()

    tmp = (use_gs.astype(bool).astype(int) - tmp_prior.astype(bool).astype(int))
    use_gs = (tmp > 0).astype(float)
    if use_gs.sum().sum() == 0:
        _warnings.warn('No gold standard could be differed from the prior.\nReturning without change')
        return prior, gold_standard

    tmp = use_gs.reindex_like(prior).fillna(0).copy()
    use_prior = ((prior - tmp) > 0).astype(bool).astype(int)
    use_prior = use_prior.loc[:, (use_prior.sum() != 0)].copy()
    if use_prior.shape[0] < prior.shape[0]:
        _warnings.warn(f'After removing GS, number of tfs in the prior are reduced with {prior.shape[0] - use_prior.shape[0]}.')

    use_gs = use_gs.reindex_like(prior).copy()
    use_gs = use_gs.loc[~(use_gs.sum(1) == 0), ~(use_gs.sum(0) == 0)].copy()
    return use_prior, use_gs


def split_by_context(prior, adata, context):
    # UNFINISHED see /notebook/setup_supirfactor_config_ciona.ipynb

    for C, df in adata.obs.groupby(context):
        context_adata = adata[df.index.tolist(), :].copy()
        _sc.pp.filter_genes(context_adata, min_cells=max([context_adata.shape[0] // 10, 10]))


def get_tfs_in_prior(prior, tfs, split_str=';'):
    """
    A simple helper function to get all TFs in a prior from a flat list of TFs.
    Assuming some TFs in the prior are concatenated by a string spliting IDs.

    Args:
        prior (pandas.DataFrame): (Genes x TFs), (rows, cols).
        tfs (list): The TFs that should be looked for in prior.columns.
        split_str (str): String to split strings on, default ';'.

    Returns:
        tf_in_prior (list(bool)): len(tf_in_prior) == cols.
            list of Booleans for columns in prior with TFs from tfs.
    """

    prior_tfs = prior.columns.str.split(split_str)

    tf_in_prior = []
    for ptf in prior_tfs:
        tf_i = False
        for tf in tfs:
            if tf in ptf:
                tf_i = True
                break
        tf_in_prior.append(tf_i)

    return tf_in_prior


def from_prior_get_tfs(prior, split_str=';'):
    """
    Get a flat list of prior TFs.
    """

    tfs_in_prior = prior.columns.str.split(split_str)
    tfs_in_prior = [item for sublist in tfs_in_prior for item in sublist]

    return tfs_in_prior


def max_vote(arr, axis=1, frac=0.9):
    """
    Selects fraction of agreeing columns in an array and returns what rows
    have a larger fraction that that.
    """

    row, cols = arr.shape
    arr = arr.astype(bool).astype(int).copy()
    arrs = arr.sum(axis)
    if frac == 0:
        return ((arrs / cols) > frac).astype(int)
    else:
        return ((arrs / cols) >= frac).astype(int)


def aggregate_cols_by_similarity(df, distance_threshold=0.25, n_clusters=None, metric='jaccard', linkage='single', pooling_func=max_vote, set_output='pandas', **kwargs):
    """
    Aggregating columns in a pandas DataFrame depending on distance, in some metric
    between them.

    Args:
        df: (pandas.DataFrame) (rows, cols). cols will be reduced based on distances.
        distance_threshold: (float, in [0, 1], default 0.25). What distance to use as
        threshold for merging columns. Lower value means closer with 0=no difference.
        n_clusters: (int, default None). Number of clusters to reduce to.
        metric: (str, default 'jaccard'). What metric to use as distance measure.
            See; sklearn.cluster.FeatureAgglomeration documentation.
        linkage: (str, default 'single'). See; sklearn.cluster.FeatureAgglomeration
            documentation.
        pooling_func: (str, default max_vote). Function for pooling columns.
            See max_vote in this module.
        set_output: (str, default 'pandas'). What output format. Only setting this to 'pandas' will have any effect, See; sklearn.cluster.FeatureAgglomeration
            documentation.
    """

    from sklearn.cluster import FeatureAgglomeration

    faggl = FeatureAgglomeration(n_clusters=n_clusters,
                                 metric=metric,
                                 linkage=linkage,
                                 distance_threshold=distance_threshold,
                                 pooling_func=pooling_func,
                                 **kwargs,
                                 )

    faggl.set_output(transform=set_output).fit(df)
    joined_tfs = []
    for i, label in enumerate(set(faggl.labels_)):
        features_with_label = [j for j, lab in enumerate(faggl.labels_) if lab == label]
        joined_tfs.append(";".join(_np.sort(faggl.feature_names_in_[features_with_label])))

    sub_df = faggl.transform(df)

    if set_output == 'pandas':
        sub_df.columns = joined_tfs

    return sub_df


def merge_duplicated_rows(df, idsep=';', verbose=False, other_df=None):
    """
    # SHOULD BE DEPRECATED IN FAVOUR OF aggregate_cols_by_similarity.
    Accepts a pandas DataFrame and merges duplicated rows and concatenate their indices.
    Optionally can take a second dataframe other_df and will merge corresponding rows in that dataframe.

    df is the prior matrix with (TF x Genes)
    """

    dupdf = df[df.duplicated(keep=False)]
    if other_df is not None:
        other_df = other_df.reindex_like(df).fillna(0)

    if dupdf.shape[0] == 0:
        if verbose:
            print('No redundancy among rows found. returning input.')
        return df, other_df

    dupdfix = dupdf.index.values

    merged = []
    other_merged = []
    for k, val in dupdf.groupby(dupdf.columns.tolist()):
        inx = val.index.tolist()
        i = idsep.join(inx)
        v = val.max(0)
        v.name = i
        merged.append(v)

        if other_df is not None:
            ov = other_df.loc[other_df.index.isin(inx), :].max(0)
            ov.name = i
            other_merged.append(ov)

    df = df.drop(dupdfix)
    df = _pd.concat([df, _pd.concat(merged, axis=1).T])

    if other_df is not None:
        other_df = other_df.drop(dupdfix)
        other_df = _pd.concat([other_df, _pd.concat(other_merged, axis=1).T])
        other_df.reindex(index=df.index, fill_value=0)

    return df, other_df


def _align_priors(priors, gene_names):
    """
    Takes a list of pandas dataframes and alignes their columns.
    Joint indices needs to be supplied as well.

    Args:
        priors (list(pandas.DataFrame)): priors to be aligned.
        gene_names (list): list of genes to be part of the prior.
    """

    cols = []
    for p in priors:
        cols.extend(list(p.columns.tolist()))
    cols = list(set(cols))
    # inds = list([list(p.index) for p in priors])
    cols = _np.sort(cols).tolist()

    priors = [prior.reindex(index=gene_names, columns=cols, fill_value=0) for prior in priors]

    return priors


def prior_loader(prior_path, gene_names):
    """
    Handles single and multiple priors.
    """

    if isinstance(prior_path, str) and _os.path.isfile(prior_path):
        prior = _load_single_prior(prior_path, gene_names)
        # prior, __ = merge_duplicated_rows(prior)

        return prior, {}

    elif isinstance(prior_path, str) and _os.path.isdir(prior_path):
        from glob import glob
        prior_path = glob(_os.path.join(prior_path, '*'))

    priors = []
    pr_name = []
    for pr in prior_path:
        name = _os.path.splitext(_os.path.splitext(_os.path.basename(pr))[0])[0]
        prior = _load_single_prior(pr, gene_names)
        pr_name.append(name)
        priors.append(prior)

    priors = _align_priors(priors, gene_names=gene_names)
    priors = {n: p for n, p in zip(pr_name, priors)}

    if 'default' in priors:
        prior = priors['default'].copy()
        del priors['default']
    else:
        prior = sum([prior.astype(bool) for key, prior in priors.items()]).astype(bool).astype(int)

    return prior, priors


def _load_single_prior(prior_path, gene_names):

    if prior_path.endswith(('.tsv.gz', '.tsv')):
        sep = '\t'
        prior = _pd.read_csv(prior_path, sep=sep, index_col=0).astype(_np.float32)
    elif prior_path.endswith(('.csv.gz', '.csv')):
        sep = ','
        prior = _pd.read_csv(prior_path, sep=sep, index_col=0).astype(_np.float32)
    else:
        raise ValueError(f"not a valid prior path {prior_path}")

    prior = prior.reindex(index=gene_names, fill_value=0)
    prior = prior.loc[:, (prior.sum(0) != 0)]

    return prior
