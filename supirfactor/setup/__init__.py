import numpy as _np
import pandas as _pd
import torch as _torch
from .. import _DTYPE, get_device as _gd
import time as _time
_SEED = int(''.join(str(_time.time()).split('.')))


def shuffle2d(data, astype=_np.float32, rng=_np.random.default_rng(_SEED)):

    sh = data.shape
    wasdf = False
    if isinstance(data, _pd.DataFrame):
        wasdf = True
        df = data.copy()
        data = data.values

    data = data.ravel().copy()
    rng.shuffle(data)
    data = data.reshape(sh).astype(astype)

    if wasdf:
        data = _pd.DataFrame(data, index=df.index, columns=df.columns)
        # df.values = data
        # data = df.copy()

    return data


def _get_model_class(model_class):
    """
    Helper function accepting a string or class
    and returns a class from supirfactor models.
    """

    from .. import models as sfmodels

    if isinstance(model_class, str):
        if hasattr(sfmodels, model_class):
            model_class = getattr(sfmodels, model_class)
        else:
            raise ValueError(f"{model_class} does not exist in the supirfactor module.")

    return model_class


def model(prior,
          model_class='Hierarchical',
          initial_state=None,
          device=None,
          general_activations=None,
          **kwargs):
    """
    Args:
        prior (pandas DataFrame):
            Pandas dataframe of prior variance constraint (Gene x TF).
    Optional:
        model_class (class, str):
            The class of the model to be trainined, default 'Hierarchical'.
        initial_state (suprifactor.model.state_dict, None):
            A state_dict that can be mapped to variables in the model, default None.
            Will be used to initialise the model with.
        general_activations (dict, None): If set will override all activations with these
            settings in the model and any keywords associated with activation.
            Should be used as a convenience feature, default None.
        kwargs:
            key value pairs sent to the model construction.
            See: supirfactor.models.Hierarchical for a list of arguments.

    Returns:
       model (supirfactor.model): A supirfactor model.

    """

    device = _gd(device)

    model_class = _get_model_class(model_class)

    if general_activations is not None:
        act = general_activations
        activations = {'w_activation': act, 'theta_activation': act, 'pi_activation': act}
        kwargs = {**activations, **kwargs}

    model = model_class(prior, **kwargs)

    if initial_state is not None:
        model.load_state_dict(initial_state, strict=False)

    model.to(device)

    return model


@_torch.inference_mode()
def swag(model, prior,
         prior_map=None,
         max_num_models=10,
         no_cov_mat=True,
         eps=None,
         astype=_DTYPE,
         **kwargs):
    """
    Args:
        model (suprifactor.models Model) : Base model for the swag framework.
        prior (pandas DataFrame) : Pandas dataframe of prior variance constraint (Gene x TF).
    Optional:
        max_num_models (int) : number of models to use for the swag estimate, (default 10).
        no_cov_mat (bool) : If we should not compute covariance of swag parameters (default True).
        eps (float, None) : Variance clamping threshold (default torch.finfo(torch.float32).eps).
        astype (type) : What precisions should be used (default torch.float32).

    Returns:
       swag_model (supirfactor.swag Model) : Swag model to be used to collect model parameters.
    """

    from copy import deepcopy as _deepcopy
    from ..swag.posteriors.swag import SWAG

    model_class = _get_model_class(model._get_name())

    eps = _torch.finfo(astype).eps if eps is None else eps

    if prior_map is None:
        prior_map = {}

    swag_model = SWAG(model_class,
                      max_num_models=max_num_models,
                      no_cov_mat=no_cov_mat,
                      model_args=[prior],
                      var_clamp=eps,
                      model_kwargs={**kwargs, **{'prior_map': prior_map}})

    device = next(model.parameters()).device
    swag_model.to(device)  # To be able to collect from the model. Might be fixed with something in the swag module however, current solutions reqire the model to be bumped back and forth between gpu and cpu (NOT GOOD!).
    swag_model.base.load_state_dict(_deepcopy(model.state_dict()), strict=False)

    return swag_model
