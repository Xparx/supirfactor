import torch as _torch
from torch import nn as _nn
from torch.optim import swa_utils as _swau


def defaults(model,
             optimizer_class='Adam',
             optimizerkw={},
             optimizer_paramskw={},
             loss_function=_nn.MSELoss,
             loss_kwargs={},
             lr=1e-3,
             weight_decay=1e-6,
             scheduler_fn='CosineAnnealingLR',
             swa_scheduler='SWALR',
             ):
    """
    Setup run configurations the can be initialised before training.

    model: (suprifactor.models Model),
        Base model for the swag framework.

    optimizer_class: (str) (default 'Adam'),
        Name of a pytorch optimizer or a call to the class itself.
    optimizerkw: (dict) (default {}),
        key word arguments provided to the setup of the optimizer.
    optimizer_paramskw: (dict) (default {}),
        key value pair of parameters with associated specific optimizer options.
    loss_function: (torch.nn) (default _nn.MSELoss),
        function to use as loss function.
    loss_kwargs: (dict) (default {}),
        key word arguments provided to the loss_function.
    lr: (float) (default 1e-3),
        learning rate applied during training.
    weight_decay: (float) (default 1e-6),
        Weight decay peanalty provided during training.
    scheduler_fn: (str) (default 'CosineAnnealingLR'),
        Name of a torch.optim.lr_scheduler
    swa_scheduler: (str) (default 'SWALR'),
        Name of a torch.optim.swa_utils.
        Note that there might be other things in swa_utils than schedulers.
    """

    if isinstance(optimizer_class, str):
        if hasattr(_torch.optim, optimizer_class):
            optimizer_class = getattr(_torch.optim, optimizer_class)
        else:
            raise ValueError(f"{optimizer_class} does not exist in the _torch.optim module.")

    paramlist = fix_optimizer_parameter_args(model, optimizer_paramskw)
    optimizer = optimizer_class(paramlist, lr=lr, weight_decay=weight_decay, **optimizerkw)

    if isinstance(scheduler_fn, str):
        if hasattr(_torch.optim.lr_scheduler, scheduler_fn):
            scheduler_fn = getattr(_torch.optim.lr_scheduler, scheduler_fn)
        else:
            raise ValueError(f"{scheduler_fn} does not exist in the _torch.optim.lr_scheduler module.")

    if isinstance(swa_scheduler, str):
        if hasattr(_swau, swa_scheduler):
            swa_scheduler = getattr(_swau, swa_scheduler)
        else:
            raise ValueError(f"{swa_scheduler} does not exist in the _torch.optim.swa_utils module.")

    loss_function = loss_function(**loss_kwargs)

    return {'optimizer': optimizer, 'lossf': loss_function, 'scheduler_fn': scheduler_fn, 'swa_scheduler': swa_scheduler}


def fix_optimizer_parameter_args(model, optimizer_paramskw):

    if not optimizer_paramskw:
        paramlist = model.parameters()
    else:
        paramlist = []
        for k, v in model.named_parameters():
            key = k.split('.')[0]
            if key in optimizer_paramskw:
                paramlist.append({'params': v, **optimizer_paramskw[key]})
            else:
                paramlist.append({'params': v})

    return paramlist
