import torch as _torch
import numpy as _np
import pandas as _pd
from anndata import AnnData as _ad
from scipy import sparse as _scprs
from torch.utils.data import Dataset
from torch_sparse import SparseTensor as _ST, cat as _scat
from .. import _DTYPE
import time as _time

_SEED = int(''.join(str(_time.time()).split('.')))


def convert2sparse_tensor(X, dtype=_DTYPE, device=None, to_sparse=False):

    if isinstance(X, _np.ndarray):
        X = _scprs.coo_array(X)

    elif _scprs.issparse(X):
        X = X.tocoo()

    elif isinstance(X, _pd.DataFrame):
        return convert2sparse_tensor(X.values)

    elif isinstance(X, _ad):
        return convert2sparse_tensor(X.X)

    if to_sparse:
        row = _torch.tensor(X.row, dtype=_torch.long, device=device)
        col = _torch.tensor(X.col, dtype=_torch.long, device=device)
        value = _torch.tensor(X.data, dtype=dtype, device=device, requires_grad=False)
        sprs_data = _ST(row=row, col=col, value=value, sparse_sizes=(int(row.max() + 1), int(col.max() + 1)))
    else:
        return _torch.tensor(X.toarray(), device=device, dtype=dtype)

    return sprs_data


def setupMMloaders(gene, peak, batch_size,
                   use_layer=None,
                   Y=None,
                   priors=None,
                   epimask=None,
                   _train=None,
                   _validate=_np.array([]),
                   _test=_np.array([]),
                   shuffle_train=True,
                   rng=_np.random.default_rng(_SEED),
                   randomize_data=False,
                   shuffle_samples=False,
                   bootstrap=False,
                   device=None,
                   num_workers=0,
                   verbose=False,
                   astype=_DTYPE):
    """
    TODO!
    """

    validation_loader = None
    if _validate.size != 0:
        dataset = SparseMMDataset(gene[_validate, :], peak[_validate, :])
        validation_loader = _torch.utils.data.DataLoader(dataset, batch_size=batch_size, collate_fn=sf_collate_wrapper, pin_memory=True, shuffle=False)

    test_loader = None
    if _test.size != 0:
        dataset = SparseMMDataset(gene[_test, :], peak[_test, :])
        validation_loader = _torch.utils.data.DataLoader(dataset, batch_size=batch_size, collate_fn=sf_collate_wrapper, pin_memory=True, shuffle=False)

    if _train is None:
        _train = _np.arange(gene.shape[0])
    dataset = SparseMMDataset(gene[_train, :], peak[_train, :])
    train_loader = _torch.utils.data.DataLoader(dataset, batch_size=batch_size, collate_fn=sf_collate_wrapper, pin_memory=True)

    return train_loader, validation_loader, test_loader


class SparseMMDataset(Dataset):
    """
    Documentation for SparseMMDataset

    """

    def __init__(self, stensor1, stensor2, **kwargs):
        super().__init__()

        self.tinput = convert2sparse_tensor(stensor1, **kwargs)
        self.ttarget = convert2sparse_tensor(stensor2, **kwargs)

        assert (self.ttarget.sizes()[0] == self.tinput.sizes()[0]), "Number of samples in gene data does not match to number of samples in peak."

        self.n_samples = self.tinput.sizes()[0]
        self.samples = _np.arange(self.n_samples)

    def __getitem__(self, index):
        data = tuple([self.tinput[index, :], self.ttarget[index, :]])

        return data, self.samples[index]

    def __len__(self):
        return self.n_samples


def sf_collate_wrapper(batch):
    return CustomSparseBatch(batch)


class CustomSparseBatch:
    """
    dataset = SparseMMDataset(gene, peak)

    loader = DataLoader(dataset, batch_size=2, collate_fn=collate_wrapper, pin_memory=True)

    for batch_ndx, sample in enumerate(loader):
        print(sample.inp.is_pinned())
        print(sample.tgt.is_pinned())

    """

    def __init__(self, data):

        samples_idx = []
        inp = []
        tgt = []
        for (gene, peak), indx in data:
            inp.append(gene)
            tgt.append(peak)
            samples_idx.append(indx)

        inp = _scat(inp, 1)
        tgt = _scat(tgt, 1)
        # inp = _torch.stack(inp)
        # tgt = _torch.stack(tgt)

        self.inp = inp.to_dense()
        self.tgt = tgt.to_dense()
        self.sample_idx = samples_idx

    # custom memory pinning method on custom type
    def pin_memory(self):
        self.inp = self.inp.pin_memory()
        self.tgt = self.tgt.pin_memory()
        return self
