import torch as _torch
import torch.nn as _nn
from supirfactor.training import extract_weights as _extract_weights
import matplotlib.pyplot as _plt
import pandas as _pd


def extract_model(trained_model, name=None):

    if not isinstance(trained_model, dict):
        trained_model = trained_model._models

    if name is None:
        name = list(trained_model.keys())[0]

    if isinstance(name, str):
        model = trained_model[name]
    else:
        model = trained_model[list(trained_model.keys())[name]]

    return model


def versus_confidence(trained_model, name=None, weight2use='grn.linear.weight', ax=None, style_label='seaborn-poster', figsize=(8, 6), dpi=None, facecolor='w', edgecolor='k', constrained_layout=True, scale='log', **kwargs):

    model = extract_model(trained_model, name=name)

    confs = model[weight2use + "_confidence"].cpu()

    if weight2use in model:
        weights = model[weight2use].cpu()
    else:
        weights = model[weight2use + "_mean"].cpu()

    selected = (weights != 0).int() + (confs > 0).int()
    confs = confs[selected == 2]
    weights = weights[selected == 2]

    with _plt.style.context(style_label):

        if ax is None:
            fig = _plt.figure(figsize=figsize, dpi=dpi, facecolor=facecolor, edgecolor=edgecolor, constrained_layout=constrained_layout)
            ax = fig.add_subplot(1, 1, 1)

        else:
            fig = None

        ax.scatter(weights.numpy(), confs.numpy(), **kwargs)

        ax.set_yscale(scale)

        ax.set_xlabel('weight')
        ax.set_ylabel('confidence')

    return fig, ax


def conf_distribution(trained_model, name=None, weight2use='grn.linear.weight', ax=None, style_label='seaborn-poster', figsize=(8, 6), dpi=None, facecolor='w', edgecolor='k', constrained_layout=True, scale='log', bins=100, orientation='horizontal', **kwargs):

    model = extract_model(trained_model, name=name)
    confs = model[weight2use + "_confidence"].cpu()
    confs = confs[confs > 0]

    with _plt.style.context(style_label):

        if ax is None:
            fig = _plt.figure(figsize=figsize, dpi=dpi, facecolor=facecolor, edgecolor=edgecolor, constrained_layout=constrained_layout)
            ax = fig.add_subplot(1, 1, 1)

        else:
            fig = None

        ax.hist(confs.log10().numpy() if scale == 'log' else confs.numpy(), bins=bins, orientation=orientation, **kwargs)
        if orientation == 'vertical':
            ax.set_xlabel(f"{scale} confidence")
            ax.set_ylabel(f"frequency")
        else:
            ax.set_ylabel(f"{scale} confidence")
            ax.set_xlabel(f"frequency")

    return fig, ax


def conf_above_threshold(trained_model, name=None, weight2use='grn.linear.weight', ax=None, style_label='seaborn-poster', figsize=(8, 6), dpi=None, facecolor='w', edgecolor='k', constrained_layout=True, scale='log', factor=1, **kwargs):

    model = extract_model(trained_model, name=name)
    confs = model[weight2use + "_confidence"].cpu().log10()

    nabove = float('inf')

    above = 1
    abvl = {}
    while nabove > 0:

        nabove = (confs > above).sum()

        abvl[above] = nabove

        above = above + 1

    with _plt.style.context(style_label):

        if ax is None:
            fig = _plt.figure(figsize=figsize, dpi=dpi, facecolor=facecolor, edgecolor=edgecolor, constrained_layout=constrained_layout)
            ax = fig.add_subplot(1, 1, 1)

        else:
            fig = None

        ax.bar([str(i) for i in abvl.keys()], abvl.values(), **kwargs)

        ax.set_xlabel('log confidence threshold')
        ax.set_ylabel('# links above')
        ax.set_yscale(scale)

    return fig, ax


def conf_range_per_feature(trained_model, prior=None, name=None, weight2use='grn.linear.weight', ax=None, style_label='default', figsize=(40, 6), dpi=None, facecolor='w', edgecolor='k', constrained_layout=True, scale='log', factor=1, **kwargs):

    model = extract_model(trained_model, name=name)
    confs = model[weight2use + "_confidence"].cpu().log10().numpy()

    confs = _pd.DataFrame(confs, columns=prior if prior is None else prior.columns, index=prior if prior is None else prior.index)

    r, c = confs.shape

    if r < c:
        confs = confs.T

    with _plt.style.context(style_label):

        if ax is None:
            fig = _plt.figure(figsize=figsize, dpi=dpi, facecolor=facecolor, edgecolor=edgecolor, constrained_layout=constrained_layout)
            ax = fig.add_subplot(1, 1, 1)

        else:
            fig = None

        confs.plot(kind='box', rot=90, ax=ax)
        ax.set_xlabel('TF')
        ax.set_ylabel('log confidence')

    return fig, ax
