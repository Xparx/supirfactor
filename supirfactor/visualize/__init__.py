import os as _os


def save_figure(fig, fdir, fname='', pixel=True, vector=True, pdf=False, dpi=150, bbox_inches='tight', transparent=False):

    if not _os.path.exists(fdir):
        _os.makedirs(fdir, exist_ok=True)

    if not fname.endswith('_'):
        fname = fname + '_'

    figf = []
    if pixel:
        f = _os.path.join(fdir, fname + "figure.png")
        fig.savefig(f, transparent=transparent, dpi=dpi, bbox_inches=bbox_inches)
        figf.append(f)
    if vector:
        f = _os.path.join(fdir, fname + "figure.svg")
        fig.savefig(f, transparent=transparent, bbox_inches=bbox_inches)
        figf.append(f)
    if pdf:
        f = _os.path.join(fdir, fname + "figure.pdf")
        fig.savefig(f, transparent=transparent, bbox_inches=bbox_inches)
        figf.append(f)

    return figf
