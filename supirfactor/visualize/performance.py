import matplotlib.pyplot as _plt
import seaborn as _sns
from supirfactor.postprocessing import get_non_hyperp


def get_optimal_across(p_stats, performance_,
                       groupby='type',
                       metric='AUPR',
                       on='mean',
                       varying_cols=['type', 'weight_decay', 'l1_lambda']
                       ):

    import pandas as pd

    opt_df_all_methods = []
    opt_vals = p_stats.iloc[p_stats.groupby(groupby)[[(metric, on)]].idxmax().values.flatten()]
    for optind, vals in opt_vals.iterrows():
        opt_p_df = performance_[(performance_[varying_cols] == vals[varying_cols].values).all(1)]
        opt_df_all_methods.append(opt_p_df)

    opt_df_all_methods = pd.concat(opt_df_all_methods, ignore_index=True)


def performance(_performance, metrics=['OPT_MCC', 'R2'], nothyperp=get_non_hyperp(), style_label='seaborn-poster', figsize=(10, 6), dpi=None, facecolor='w', edgecolor='k', constrained_layout=True, xscale='log', yscale='linear', ax=None, c=['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w'], bbox_to_anchor=(1.01, 0.6), x='weight_decay', plot_separate='type', s=50, **kwargs):

    performance = _performance.copy().dropna(1, how='all').fillna(0).replace([None], ['none'])
    indx = performance.columns[~performance.columns.isin(nothyperp)]
    if isinstance(x, str):
        indx = indx[~(indx == x)]
    else:
        indx = indx[~(indx.isin(x))]
    indx = indx[~(indx == plot_separate)].tolist()

    # print(indx)
    # return indx

    figs = []
    axs = []
    for groupid, df in performance.groupby(indx):

        with _plt.style.context(style_label):

            for metric in metrics:

                if xscale == 'log' and ((df[x] == 0).sum() == df.shape[0]):
                    continue

                if ax is None:
                    fig = _plt.figure(figsize=figsize, dpi=dpi, facecolor=facecolor, edgecolor=edgecolor, constrained_layout=constrained_layout)
                    axpl = fig.add_subplot(1, 1, 1)

                    figs.append(fig)
                    axs.append(axpl)

                else:
                    fig = None
                    axpl = ax

                pal = c[:df[plot_separate].unique().shape[0]]
                _sns.lineplot(x=x, y=metric, style=plot_separate, hue=plot_separate, data=df, ax=axpl, n_boot=100, palette=pal)
                _sns.scatterplot(x=x, y=metric, hue=plot_separate, data=df, ax=axpl, palette=pal, s=s)

                handles, labels = axpl.get_legend_handles_labels()

                labels = [" ".join(i.split('_')) for i in labels]

                nh = len(handles)

                l = axpl.legend(
                    handles[0:nh // 2], labels[0:nh // 2],
                    title=plot_separate,
                    handletextpad=0, columnspacing=1,
                    loc="best", frameon=True)  # , bbox_to_anchor=bbox_to_anchor)
                l.get_title().set_fontsize(16)
                axpl.set_xscale(xscale)
                axpl.set_yscale(yscale)
                axpl.grid()
                # axpl.set_ylabel('Performance')
                ax2 = axpl.twinx()
                ax2.set_ylabel('\n'.join([str(i) + ": " + str(j) for i, j in zip(indx, groupid)]), rotation=0, ha='left', fontsize=12)
                ax2.set_yticks([])
                ax2.yaxis.set_label_coords(1.1, 0.9)

    return figs, axs


def performance_2d(_performance_stats,
                   hues=['AUPR'],
                   size='R2',
                   subfield='mean',
                   nothyperp=get_non_hyperp(),
                   ax=None,
                   style_label='seaborn-poster',
                   figsize=(10, 6), dpi=None,
                   facecolor='w', edgecolor='k', ax_facecolor='xkcd:white',
                   constrained_layout=True,
                   xscale='log',
                   yscale='log',
                   bbox_to_anchor=(1, 0.5),
                   x=['weight_decay', 'l1_lambda'],
                   plot_separate='type',
                   sizes=(20, 100),
                   hue_norm=(0, 1), **kwargs):

    import numpy as np

    __ = [i for i, j in _performance_stats.columns]

    if plot_separate is not None:
        nothyperp.append(plot_separate)

    nothyperp.extend(x)
    not_select = np.array([i in nothyperp for i in __])
    hparams = _performance_stats.columns[not_select]
    indx = _performance_stats.columns[~_performance_stats.columns.isin(hparams)].tolist()

    figs = []
    axs = []
    for groupid, df in _performance_stats.groupby(indx):

        for model_type, mdf in df.groupby(plot_separate):

            if mdf.shape[0] <= 1:
                continue

            mdf.columns = [' '.join(col).strip() for col in mdf.columns.values]
            with _plt.style.context(style_label):

                for hue in hues:

                    if ax is None:
                        fig = _plt.figure(figsize=figsize, dpi=dpi, facecolor=facecolor, edgecolor=edgecolor, constrained_layout=constrained_layout)
                        axpl = fig.add_subplot(1, 1, 1)

                        figs.append(fig)
                        axs.append(axpl)

                    else:
                        fig = None
                        axpl = ax

                    g = _sns.scatterplot(x=x[0], y=x[1], hue=f"{hue} {subfield}", size=f"{size} {subfield}", ax=axpl, data=mdf, hue_norm=hue_norm, sizes=sizes, **kwargs)
                    axpl.set_xscale(xscale)
                    axpl.set_yscale(yscale)
                    # axpl.grid(which='major')
                    axpl.grid()
                    axpl.set_facecolor(ax_facecolor)
                    axpl.minorticks_off()

                    g.set(aspect='equal', title=model_type.upper())
                    _sns.despine(left=True, bottom=True)
                    g.margins(.02)
                    axpl.legend(loc='center left', bbox_to_anchor=bbox_to_anchor)

    return figs, axs


def optimal_across(p_stats, performance_,
                   groupby='type',
                   optimize_metric='AUPR',
                   metric='AUPR',
                   on='mean',
                   varying_cols=['type', 'weight_decay', 'l1_lambda'],
                   ax=None,
                   hue=None,
                   dodge=.2,
                   style_label='seaborn-poster',
                   figsize=(4, 6), dpi=None,
                   facecolor='w', edgecolor='k',
                   ax_facecolor='xkcd:white',
                   constrained_layout=True,
                   bbox_to_anchor=(1, 0.5),
                   c=['b', 'r', 'g', 'c', 'm', 'y', 'k', 'w'],
                   ):

    opt_vals, opt_df_all_methods = get_optimal_across(p_stats, performance_, groupby=groupby, metric=optimize_metric, on=on, varying_cols=varying_cols)

    with _plt.style.context(style_label):
        if ax is None:
            fig = _plt.figure(figsize=figsize, dpi=dpi, facecolor=facecolor, edgecolor=edgecolor, constrained_layout=constrained_layout)
            axpl = fig.add_subplot(1, 1, 1)

        else:
            fig = None
            axpl = ax

        bar_pal = ['grey'] * performance_[hue].unique().shape[0]
        point_pal = ['lightgray'] * performance_[hue].unique().shape[0]
        mean_pal = c[:performance_[hue].unique().shape[0]]

        ax = _sns.pointplot(x=groupby, y=metric, data=opt_df_all_methods, color='lightgray',
                            palette=bar_pal, markers='',
                            hue=hue, join=False, ci='sd', ax=axpl, dodge=dodge, errwidth=3)
        _plt.setp(ax.lines, zorder=-100, label="")
        _plt.setp(ax.collections, zorder=-100, label="")

        _sns.stripplot(x=groupby, y=metric, hue=hue,
                       data=opt_df_all_methods, alpha=1,
                       zorder=-200, jitter=0.2, palette=point_pal, ax=axpl, dodge=dodge, edgecolor='lightgray')

        ax = _sns.pointplot(x=groupby, y=metric,
                            hue=hue, data=opt_df_all_methods,
                            markers='_', errwidth=0, join=False, ax=axpl, zorder=10,
                            palette=mean_pal, dodge=dodge)

        ax.set_xticklabels([t.get_text().upper() for t in ax.get_xticklabels()])
        ax.tick_params(axis='y', length=0.0)

        handles, labels = axpl.get_legend_handles_labels()

        # labels = [" ".join(i.split('_')) for i in labels]

        # labels = ['shuffled' if (i == 'True') else 'regular' for i in labels]

        nh = len(handles)

        leg = axpl.legend(
            handles[nh // 2:], labels[nh // 2:],
            # title=hue.replace('_', ' '),
            title=hue,
            handletextpad=0,
            columnspacing=1,
            loc="best",
            frameon=True,
            # bbox_to_anchor=bbox_to_anchor
        )

        leg.get_title().set_fontsize(16)

        _sns.despine(left=True, offset=0.1)
        ax.grid(axis='y', zorder=-1000, linestyle='--', linewidth=1)
        ax.set_xlabel('SupirFactor output')

    return fig, axpl
