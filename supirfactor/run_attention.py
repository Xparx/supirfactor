import numpy as _np
import torch as _torch
from torch import nn as _nn
import time as _time
from supirfactor.swag import utils as _swagu


def fit(model, train_loader, optimizer, scheduler_fn, lossf,
        device=None,
        validation_loader=None,
        scheduler_kwargs={'T_max': 10},
        epochs=100,
        swa_start=0.9,
        swa_model=None,
        swa_scheduler=None,
        swa_scheduler_kwargs={'anneal_strategy': "cos", 'anneal_epochs': 10},
        val_lossf=_nn.MSELoss,
        val_lossf_kwargs={},
        verbose=False,
        ):

    verboseprint = print if verbose else lambda *a, **k: None
    val_lossf = val_lossf(**val_lossf_kwargs)

    if device is None:
        device = next(model.parameters()).device
    else:
        device = device
        model = model.to(device)

    start_time = _time.time()

    lr_scheduler = scheduler_fn(optimizer, **scheduler_kwargs)

    # swa_start = _np.inf
    if swa_model is not None:
        swa_lr = optimizer.param_groups[0]['lr']
        swa_start = int(epochs * swa_start + 1)
        swa_scheduler = swa_scheduler(optimizer, **swa_scheduler_kwargs, swa_lr=swa_lr)

    var = None
    train_loss = []
    for epoch in range(epochs):

        losses = {}
        losses['epoch'] = epoch

        tr_loss = train_epoch(model, train_loader, optimizer, device, lossf=lossf)

        if (epoch > swa_start) and (swa_model is not None):
            swa_scheduler.step()
            swa_model.collect_model(model)
        else:
            lr_scheduler.step()

        if validation_loader is not None:
            val_loss, R2, var = validation_epoch(model, validation_loader, device, lossf=val_lossf, init_var=var)
        else:
            val_loss, R2, var = (_np.nan, _np.nan, _np.nan)

        train_loss.append([epoch, tr_loss, val_loss, R2])

        if swa_model is not None:
            swa_model.sample(0.0)
            _swagu.bn_update(train_loader, swa_model, device=device)

        _extime = _time.time() - start_time

        curr = _time.ctime()
        verboseprint(f"epoch: {epoch+1}/{epochs}, {((epoch+1)/epochs)*100:.2f}%, t-loss = {tr_loss:6.3g}, v-loss = {val_loss:6.3g}, v-R2 = {R2:4.3g}, run time: {_extime/60:.2f}min, " + curr + ' ' * 5, end='\r')

    return train_loss


def train_epoch(model, data_loader, optimizer, device, lossf=_nn.BCELoss()):
    '''
    Parameters
    ----------
    model: supirfactor.model
        A pytorch model. Specifically a supirfactor model.
    data_loader: SupirFactor dataloader
        description
    optimizer: type
        description
    lossf: type
        description
    device: type
        description
    profiler: type
        description

    '''

    model = model.train()

    loss = _torch.tensor(0., device=device)
    for batch_features in data_loader:
        inputs = batch_features.inp.to(device, non_blocking=True)
        targets = batch_features.tgt.to(device, non_blocking=True)

        # compute reconstructions
        outputs = model(inputs, targets)

        # compute training reconstruction loss
        # train_loss = lossf(outputs, targets)
        train_loss = lossf(outputs, inputs)

        optimizer.zero_grad()

        # compute accumulated gradients
        train_loss.backward()

        # perform parameter update based on current gradients
        optimizer.step()

        # add the mini-batch training loss to epoch loss
        loss += train_loss  # .item()

    # compute the epoch training loss
    loss = (loss / len(data_loader)).detach()

    return loss.item()


@_torch.inference_mode()
def validation_epoch(model, data_loader, device, lossf=_nn.MSELoss(), init_var=None, rss=False):

    loss = _torch.tensor(0., device=device)

    if init_var is None:
        var = _torch.tensor(0., device=device)
    else:
        var = init_var

    model = model.eval()

    for batch_features in data_loader:
        inputs = batch_features.inp.to(device, non_blocking=True)
        targets = batch_features.tgt.to(device, non_blocking=True)

        outputs = model(inputs, targets)
        # val_loss = lossf(outputs, targets)
        val_loss = lossf(outputs, inputs)

        loss += val_loss

        if init_var is None:
            if not rss:
                var += (targets**2).mean()
            else:
                var = 1

    if not rss:
        loss = (loss / len(data_loader)).detach()
    else:
        loss = loss.detach()

    if init_var is None:
        var = var / len(data_loader)

    R2 = 1 - (loss / var).detach()

    return loss.item(), R2.item(), var
