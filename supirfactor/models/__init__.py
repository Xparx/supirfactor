from torch import nn as _nn
from . import modules, utils, sparse_modules

_do_mask = utils._check_match
_ACTIVATION = 'ReLU'
_ACT_KWARGS = {}
ACTIVATION = {'activation': _ACTIVATION, 'kwargs': _ACT_KWARGS}


class Hierarchical(_nn.Module):
    """
    Hierarchical SupirFactor model. Uses ReLU activation for all layers by default.

    Args:
        mask: (pandas.DataFrame),
            a prior constraing (genes x TFs) that defines the dimensions of the input (genes)
            and first latent layer (TFs)
        prior_map: (dict of pandas.DataFrame) (default None),
            A dictionary where the keys define subtask masks provided to their own task
            specific encoders.
            Diminsions should match mask and needs to be handled externally.
        pi_structure: (list) (default [64]),
            A list or list like where each element determines the width of a corresponding
            layer and the length of the list determines the depth of the model.
        mask_grn: (pandas.DataFrame) (default None),
            An optional mask provided to the output layer. Dimensions needs to match
            last hidden layer output (genes x last layer width).
        [pW, pTheta, pPi]: (default 0.0),
            Dropout rate for each module.
        {X}_activation: (dict) (default {'activation': 'ReLU', 'kwargs': None}),
            activation functions for the corresponding modules.
        bias: (bool) (default False),
            if a bias should be used for each layer.
        set_prior_as_init_weights: (bool) (default False),
            Should the masks provided be used as is with elements as weights.
            If False pytorch initialisation is done as default.
        mirror_init_weights: (bool) (default False),
            Only works if output (TFs) dimension equals input dimension of final layer.
            Not recommended in Hierarchical model.
        encode: (torch module) (default supirfactor.models.modules.JREncoder),
            Recommended to keep as is.
        decoder: (torch module) (default supirfactor.models.modules.Decoder),
            Recommended to keep as is.
        **kwargs: unused.

    Examples::

        >>> from supirfactor.models import Hierarchical
        >>> model = Hierarchical(prior)

    """

    def __init__(self, mask, prior_map=None, pi_structure=[64], mask_grn=None, pW=0.0, pTheta=0.0, pPi=0.0, w_activation=ACTIVATION, theta_activation=ACTIVATION, pi_activation=ACTIVATION, bias=False, set_prior_as_init_weights=False, mirror_init_weights=False, encoder=modules.JREncoder, decoder=modules.Decoder, **kwargs):

        super().__init__()

        input_shape, tf_shape = mask.shape

        self._n_latent_features = tf_shape
        self._n_input_features = input_shape

        if w_activation is None:
            w_activation = {'activation': None, 'kwargs': None}
        if theta_activation is None:
            theta_activation = {'activation': None, 'kwargs': None}
        if pi_activation is None:
            pi_activation = {'activation': None, 'kwargs': None}

        self.W = encoder(mask, prior_map=prior_map, bias=bias, p=pW, activation=w_activation['activation'], act_kwargs=w_activation['kwargs'], set_prior_as_weights=set_prior_as_init_weights)

        __ = [tf_shape]
        __.extend(pi_structure)
        pi_structure = __
        del __
        its = range(len(pi_structure))

        self.Pi = _nn.ModuleList([modules.Intermediate(output_shape=pi_structure[i],
                                                       input_shape=pi_structure[i - 1],
                                                       p=pPi,
                                                       bias=bias,
                                                       activation=pi_activation['activation'],
                                                       act_kwargs=pi_activation['kwargs'],
                                                       )
                                  for i in its[1:]])

        self.Theta = decoder(output_shape=input_shape, input_shape=pi_structure[-1], bias=bias, p=pTheta, activation=theta_activation['activation'], act_kwargs=theta_activation['kwargs'], mask=mask_grn)

        if mirror_init_weights:
            mask = self.W.prior_mask.bool().T
            self.Theta.linear.weight.data[mask] = self.W.linear.weight.data.detach().clone().T[mask]

    def LA(self, x, priors=None, output=None, **kwargs):
        """
        Computing latent activity for a given stage of the model.
        Basically a workaround untill the following issue is resolved;
        https://github.com/pytorch/vision/issues/6817#issuecomment-1501226056
        Replaces the forward function.
        """

        x = self.W(x, priors=priors)
        if output == 'W':
            return x
        for i, layer in enumerate(self.Pi):
            x = layer(x)
            if output.endswith('.' + str(i)):
                return x
        x = self.Theta(x)

        return x

    def forward(self, x, priors=None, mask_input=None, on=None):

        x = self.W(x, priors=priors, mask_input=_do_mask(on, 'W', mask=mask_input))
        for i, layer in enumerate(self.Pi):
            x = layer(x, mask_input=_do_mask(on, i, mask=mask_input))
        x = self.Theta(x, mask_input=_do_mask(on, 'Theta', mask=mask_input))
        return x


class Shallow(Hierarchical):
    """
    Shallow SupirFactor model. Can still be used as a deep model, however by default does not include layers in Pi. Linear activation by default.

    See documentation for supirfactor.model.Hierarchical for full list of arguments.
    """

    def __init__(self, *args, pi_structure=[], w_activation=None, theta_activation=None, pi_activation=None, **kwargs):
        super().__init__(*args, pi_structure=pi_structure, w_activation=w_activation, theta_activation=theta_activation, pi_activation=pi_activation, **kwargs)

    def forward(self, x, mask_input=None, on=None, priors=None):

        x = super().forward(x, mask_input=mask_input, on=on, priors=priors)
        return x


class HwD(Hierarchical):
    """
    SupirFactor model with a linear decay term.

    See documentation for supirfactor.model.Hierarchical for full list of arguments.
    """

    def __init__(self, mask, *args, **kwargs):
        super().__init__(mask, *args, **kwargs)

        input_shape, tf_shape = mask.shape

        self.decay = modules.Decay(input_shape)

    def LA(self, x, output=None, **kwargs):

        if output == 'decay':
            # Only works here as decay is not needed for any other activity.
            return self.decay(x)

        return super().LA(x, output=output, **kwargs)

    def forward(self, x, mask_input=None, on=None, priors=None):

        y = self.decay(x)
        x = super().forward(x, mask_input=mask_input, on=on, priors=priors)

        return x - y


class HwRes(Hierarchical):
    """Documentation for HwRes

    """
    def __init__(self, *args, res_depth=3, bottle_size=10, res_activation=ACTIVATION, **kwargs):
        super().__init__(*args, **kwargs)

        # self.WRes = modules.ResBlock(self._n_latent_features, depth=res_depth, activation=res_activation['activation'], act_kwargs=res_activation['kwargs'])
        self.WRes = modules.ResBottle(self._n_latent_features, bottle_size=bottle_size, activation=res_activation['activation'], act_kwargs=res_activation['kwargs'])

    def LA(self, x, priors=None, output=None, **kwargs):
        """
        Computing latent activity for a given stage of the model.
        Basically a workaround untill the following issue is resolved;
        https://github.com/pytorch/vision/issues/6817#issuecomment-1501226056
        Replaces the forward function.
        """

        x = self.W(x, priors=priors)
        if output == 'W':
            return x

        x = self.WRes(x)
        if output == 'WRes':
            return x

        for i, layer in enumerate(self.Pi):
            x = layer(x)
            if output.endswith('.' + str(i)):
                return x
        x = self.Theta(x)

        return x

    def forward(self, x, priors=None, mask_input=None, on=None):

        x = self.W(x, priors=priors, mask_input=_do_mask(on, 'W', mask=mask_input))
        x = self.WRes(x)  # (Becouse resnet should map identity I think this should be the right way)
        # OR: x = self.WRes(x, mask_input=_do_mask(on, 'WRes', mask=mask_input))
        for i, layer in enumerate(self.Pi):
            x = layer(x, mask_input=_do_mask(on, i, mask=mask_input))
        x = self.Theta(x, mask_input=_do_mask(on, 'Theta', mask=mask_input))
        return x


class HwMirror(Hierarchical):
    """Documentation for HwMirror

    """
    def __init__(self, mask, *args, structured_decoder=sparse_modules.SparseDecoder, theta_activation=ACTIVATION, **kwargs):
        super().__init__(mask, *args, **kwargs)

        self.mirror_decoder = structured_decoder(
            mask,
            activation=theta_activation['activation'],
            act_kwargs=theta_activation['kwargs'], **kwargs)

    def get_reporters(self):

        return self.mirror_decoder.reporters()

    def LA(self, x, priors=None, output=None, **kwargs):
        """
        Computing latent activity for a given stage of the model.
        Basically a workaround untill the following issue is resolved;
        https://github.com/pytorch/vision/issues/6817#issuecomment-1501226056
        Replaces the forward function.
        """

        x = self.W(x, priors=priors)
        if output == 'W':
            return x

        if output == 'mirror':
            return self.mirror_decoder(x)

        for i, layer in enumerate(self.Pi):
            x = layer(x)
            if output.endswith('.' + str(i)):
                return x
        x = self.Theta(x)

        return x

    def forward(self, x, priors=None, mask_input=None, on=None):

        x = self.W(x, priors=priors, mask_input=_do_mask(on, 'W', mask=mask_input))
        y = self.mirror_decoder(x)
        for i, layer in enumerate(self.Pi):
            x = layer(x, mask_input=_do_mask(on, i, mask=mask_input))
        x = self.Theta(x, mask_input=_do_mask(on, 'Theta', mask=mask_input))
        return x, y


class HAttention(Hierarchical):
    """
    Documentation for HAttention

    """

    def __init__(self, *args, pa=0.1, n_heads=None, encoder=sparse_modules.JREncoder, bias=False, a_activation=ACTIVATION, **kwargs):
        super().__init__(*args, encoder=encoder, bias=bias, **kwargs)

        self.Ahead = modules.LuongBlock(self._n_latent_features, n_heads=n_heads, p=pa, bias=bias, activation=a_activation['activation'], act_kwargs=a_activation['kwargs'])

    def LA(self, x, priors=None, output=None, **kwargs):

        x = self.W(x, priors=priors)
        if output == 'W':
            return x

        scores = self.Ahead.score_fun(x)
        if output == 'Ascores':
            return scores

        att = self.Ahead.attention(scores, x)
        if output == 'Aattention':
            return att

        x = self.Ahead.activation(att + x)
        if output == 'Ahead':
            return x

        for i, layer in enumerate(self.Pi):
            x = layer(x)
            if output.endswith('.' + str(i)):
                return x

        x = self.Theta(x)

        return x

    def forward(self, x, priors=None, mask_input=None, on=None):

        x = self.W(x, priors=priors, mask_input=_do_mask(on, 'W', mask=mask_input))
        x = self.Ahead(x, mask_input=_do_mask(on, 'Ahead', mask=mask_input))
        for i, layer in enumerate(self.Pi):
            x = layer(x, mask_input=_do_mask(on, i, mask=mask_input))
        x = self.Theta(x, mask_input=_do_mask(on, 'Theta', mask=mask_input))
        return x


class HDotA(Hierarchical):
    """
    Documentation for HAttention

    """

    def __init__(self, *args, pa=0.0,
                 encoder=sparse_modules.JRAttentionEncoder,
                 attention=modules.DotAttention, bias=False,
                 a_activation=ACTIVATION, **kwargs):
        super().__init__(*args, encoder=encoder, bias=bias, **kwargs)

        self.attention = attention(self._n_latent_features, p=pa, bias=bias,
                                   activation=a_activation['activation'],
                                   act_kwargs=a_activation['kwargs'])

    def LA(self, x, priors=None, output=None, **kwargs):

        x = self.W(x, priors=priors)
        if output == 'W':
            x, key, query = x
            return x

        if output == 'Key':
            x, key, query = x
            return key

        if output == 'Query':
            x, key, query = x
            return query

        scores = self.attention.score(*x)
        if output == 'score':
            return scores

        x, key, query = x
        x = self.attention.activation(x + x @ scores)
        if output == 'attention':
            return x

        for i, layer in enumerate(self.Pi):
            x = layer(x)
            if output.endswith('.' + str(i)):
                return x

        x = self.Theta(x)

        return x

    def forward(self, x, priors=None, mask_input=None, on=None):

        x = self.W(x, priors=priors, mask_input=_do_mask(on, 'W', mask=mask_input))
        x = self.attention(x, mask_input=_do_mask(on, 'attention', mask=mask_input))
        for i, layer in enumerate(self.Pi):
            x = layer(x, mask_input=_do_mask(on, i, mask=mask_input))
        x = self.Theta(x, mask_input=_do_mask(on, 'Theta', mask=mask_input))
        return x
