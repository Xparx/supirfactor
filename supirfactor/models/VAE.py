import torch as _torch
from torch import nn as _nn
from supirfactor.models import BaseModel as _bm, get_device
from supirfactor.models.supirfactor import Decoder


class Encoder(_bm):

    def __init__(self, input_shape=100, hidden_width=10, p=0.0, bias=False, dobnorm=True, mask=None, device=get_device()):
        super(Encoder, self).__init__()

        self.mask = mask
        self.dobnorm = dobnorm
        self.dop = _nn.Dropout(p=p)
        self.bnorm = _nn.BatchNorm1d(input_shape)
        self.linear = _nn.Linear(in_features=input_shape, out_features=hidden_width, bias=bias)
        self.linearvar = _nn.Linear(in_features=input_shape, out_features=hidden_width, bias=bias)

        weight = self.linear.weight.to(device)
        self.linear.weight = _nn.parameter.Parameter(weight.mul(self.mask))
        self.linear.weight.register_hook(self.mask_prior)
        self.linear.weight.register_hook(self.mask_zeros)

    def reparameterize(self, mu, logvar):
        std = _torch.exp(0.5 * logvar)
        eps = _torch.randn_like(std)
        return mu + eps * std

    def forward(self, x):

        if self.dobnorm:
            x = self.bnorm(x)

        x = self.dop(x)
        mu = self.linear(x)
        logvar = self.linearvar(x)
        z = self.reparameterize(mu, logvar)
        return z, mu, logvar


class VAE(_nn.Module):
    """Documentation for VAE

    """
    def __init__(self, input_shape, hidden_width, mask=None, verbose=False, bias=False, pin=0.0, ptf=0.0, dobnorm=True, device=get_device()):

        super(VAE, self).__init__()

        self.encoder = Encoder(input_shape=input_shape, hidden_width=hidden_width, bias=bias, p=pin, dobnorm=dobnorm, mask=mask, device=device)

        self.decoder = Decoder(output_shape=input_shape, hidden_width=hidden_width, bias=bias, p=ptf)

    def forward(self, x):

        z, mu, var = self.encoder(x)
        xp = self.decoder(z)

        return xp, mu, var


def vaeloss(xp, x, mu, var, base_criterion=_nn.functional.mse_loss):

    loss = base_criterion(xp, x, reduction='sum')

    # see Appendix B from VAE paper:
    # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
    # https://arxiv.org/abs/1312.6114
    # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD = -0.5 * _torch.sum(1 + var - mu.pow(2) - var.exp())

    return loss + KLD


