import torch as _torch
import torch.nn as _nn
from torch_sparse import SparseTensor as _ST, spmm as _spmm
# from torch_geometric.utils import softmax as _ssoftmax
import numpy as _np
import pandas as _pd
from anndata import AnnData as _ad
from scipy import sparse as _scprs
# from torch import tensor as _tensor
from torch.fx import wrap as _fxwrap
from .utils import _DROPOUT, activity as _activity, silence as _silence
from .. import _DTYPE
from torch.nn import init as _init


def fix_sparse_mask(prior, random_init=True, norm_rows=False, device=None, dtype=_DTYPE):

    if isinstance(prior, (_np.ndarray, _pd.DataFrame)):

        # prior = prior.T.copy()

        X = _scprs.coo_array(prior
                             if isinstance(prior,
                                           _np.ndarray)
                             else prior.values.copy())

        X.eliminate_zeros()

        nrows, ncols = X.shape
        out_factor = X.sum(1)

    elif _scprs.issparse(prior):

        X = prior.tocoo()
        X.eliminate_zeros()
        out_factor = X.sum(1).A1
        nrows, ncols = X.shape

    elif isinstance(prior, _ad):

        # return fix_sparse_mask(prior.X.T.copy())
        return fix_sparse_mask(prior.X)

    if norm_rows:
        X = (X.T / out_factor / 2).T

    row = _torch.tensor(X.row, dtype=_torch.long, device=device)
    col = _torch.tensor(X.col, dtype=_torch.long, device=device)
    value = _torch.tensor(X.data, dtype=dtype, device=device, requires_grad=True)
    if random_init:
        bound = 1 / _np.sqrt(ncols)
        _init.uniform_(value, -bound, bound)
    out_factor = _torch.tensor(out_factor, requires_grad=False, dtype=dtype, device=device)
    return row, col, value, out_factor, nrows, ncols


@_fxwrap
def spmm(rowcols: tuple, W, nrows: int, ncols: int, x):
    """
    nrows, ncols refers to the dimensions of the sparse matrix.
    x is typically the batch that needs to be transposed for the
    spmm routine of pytorch_sparse.
    """
    # return _spmm((row, col), W, x)
    return _spmm(rowcols, W, nrows, ncols, x)


@_fxwrap
def mulifavail(x, y=None):
    """
    Multiply y if y availible else do nothing.
    """

    if y is None:
        return x
    else:
        if isinstance(y, _ST):
            # return y.mul_nnz(x[y.coo()[0], y.coo()[1]], layout='coo')
            return y.to_dense().mul(x)
        else:
            return y.mul(x)


# class LinearSparse(_torch.jit.ScriptModule): # How do?
class LinearSparse(_nn.Module):
    """Documentation for LinearSparse

    """
    def __init__(self, mask, factory_kwargs={}, norm_rows=True, dtype=_DTYPE, bias=True):
        super().__init__()

        factory_kwargs = {**factory_kwargs, 'dtype': dtype}
        row, col, value, norm_factor, n_rows, n_cols = fix_sparse_mask(mask, norm_rows=norm_rows, **factory_kwargs)

        self.num_input = n_cols
        self.num_output = n_rows

        self.register_buffer('row', row)
        self.register_buffer('col', col)
        self.weight = _nn.Parameter(value)
        # self.weight.data = self.weight.data.abs()

        if bias:
            self.bias = _nn.Parameter(_torch.ones(self.num_output))
            _init.uniform_(self.bias, -0.01, 0.01)
        else:
            self.bias = bias

        self.shape = _torch.Size([self.num_output, self.num_input])

    # @_torch.jit.script_method
    def forward(self, x):

        x = spmm((self.row, self.col), self.weight, self.num_output, self.num_input, x.T).T
        return x


class SparseEncoder(_nn.Module):
    """
    Expects a prior mask with TFs on columns and genes on rows.
    """

    def __init__(self, mask,
                 p=0.0, dropoutf=_DROPOUT,
                 activation=None, act_kwargs={},
                 dtype=_DTYPE,
                 bias=False,
                 set_prior_as_weights=False,
                 factory_kwargs={},
                 norm_rows=True,
                 **kwargs):
        super().__init__()

        factory_kwargs = {**factory_kwargs, 'dtype': dtype}
        super().__init__()

        mask = mask.T.copy()

        self.linear = LinearSparse(mask, factory_kwargs=factory_kwargs, dtype=dtype, bias=bias, norm_rows=norm_rows)
        self.linear.weight.data = self.linear.weight.data.abs()

        # row, col, value, norm_factor, n_rows, n_cols = fix_sparse_mask(mask, norm_rows=norm_rows, **factory_kwargs)
        # self.num_input = n_cols
        # self.num_output = n_rows

        # self.register_buffer('row', row)
        # self.register_buffer('col', col)
        # self.weight = _nn.Parameter(value)
        # self.weight.data = self.weight.data.abs()

        # # self.bias = _nn.Parameter(_nn.Parameter(_torch.ones(self.num_output)) * -0.1)

        self.pdo = p
        if p > 0:
            self.dropout = dropoutf(p=p)
        else:
            self.dropout = _nn.Identity()

        self.activation = _activity(activation, act_kwargs)

    def forward(self, x, mask_input=None):

        x = self.dropout(x)
        x = _silence(x, mask_input=mask_input)
        x = self.linear(x)
        # x = spmm((self.row, self.col), self.weight, self.num_output, self.num_input, x.T).T
        x = self.activation(x)

        return x


class SparseDecoder(_nn.Module):
    """
    Expects a prior mask with TFs on columns and genes on rows.
    """

    def __init__(self, mask,
                 p=0.0, dropoutf=_DROPOUT,
                 activation=None, act_kwargs={},
                 dtype=_DTYPE,
                 set_prior_as_weights=False,
                 factory_kwargs={},
                 norm_rows=True,
                 **kwargs):
        super().__init__()

        factory_kwargs = {**factory_kwargs, 'dtype': dtype}
        super().__init__()

        self.linear = LinearSparse(mask, factory_kwargs=factory_kwargs, dtype=dtype, bias=False, norm_rows=norm_rows)
        self.linear.weight.data = self.linear.weight.data.abs()

        # row, col, value, norm_factor, n_rows, n_cols = fix_sparse_mask(mask, norm_rows=norm_rows, **factory_kwargs)
        # self.num_input = n_cols
        # self.num_output = n_rows

        # self.register_buffer('row', row)
        # self.register_buffer('col', col)
        # self.weight = _nn.Parameter(value)
        # self.weight.data = self.weight.data.abs()

        self.pdo = p
        if p > 0:
            self.dropout = dropoutf(p=p)
        else:
            self.dropout = _nn.Identity()

        self.activation = _activity(activation, act_kwargs)

        # self.reporters = self.linear.row.unique()

    def reporters(self):

        return self.linear.row.unique()

    def forward(self, x, mask_input=None):

        x = self.dropout(x)
        x = _silence(x, mask_input=mask_input)
        x = self.linear(x)
        # x = spmm((self.row, self.col), self.weight, self.num_output, self.num_input, x.T).T
        x = self.activation(x)

        return x


class JREncoder(SparseEncoder):
    """
    Documentation for JRSEncoder

    """
    def __init__(self, mask, prior_map=None, encoder=SparseEncoder, **kwargs):
        super().__init__(mask, **kwargs)

        if prior_map and (prior_map is not None):
            if isinstance(prior_map, dict):
                self.context = _nn.ModuleDict({name: encoder(prior, **kwargs) for name, prior in prior_map.items()})
            elif isinstance(prior_map, (tuple, list, set)):
                self.context = _nn.ModuleDict({name: encoder(mask, **kwargs) for name in prior_map})
        else:
            self.context = {}

    def forward(self, x, priors=None, mask_input=None):

        if (priors is None) or (priors not in self.context):
            x = super().forward(x, mask_input=mask_input)
        else:
            x = self.context[priors](x, mask_input=mask_input)

        return x


class SparseAttentionEncoder(_nn.Module):
    """
    Expects a prior mask with TFs on columns and genes on rows.
    """

    def __init__(self, mask,
                 p=0.0, dropoutf=_DROPOUT,
                 activation=None, act_kwargs={},
                 dtype=_DTYPE,
                 set_prior_as_weights=False,
                 factory_kwargs={},
                 norm_rows=True,
                 **kwargs):
        super().__init__()

        factory_kwargs = {**factory_kwargs, 'dtype': dtype}
        super().__init__()

        mask = mask.T.copy()

        row, col, value, norm_factor, n_rows, n_cols = fix_sparse_mask(mask,
                                                                       norm_rows=norm_rows, **factory_kwargs)
        self.num_input = n_cols
        self.num_output = n_rows

        self.register_buffer('norm_factor', norm_factor ** 0.5)
        self.register_buffer('row', row)
        self.register_buffer('col', col)
        # self.Wq = _nn.Parameter(value)
        # _init.uniform_(self.Wq, -0.01, 0.01)
        self.Wq = _nn.Parameter(value.abs())
        # self.Wq = _nn.Parameter(value.abs() / self.num_output ** 2)
        # self.Wq.data.zero_()
        # self.Wk = _nn.Parameter(value)
        # _init.uniform_(self.Wk, -0.01, 0.01)
        self.Wk = _nn.Parameter(value.abs())
        # self.Wk = _nn.Parameter(value.abs() / self.num_output ** 2)
        # self.Wk.data.zero_()
        self.Wv = _nn.Parameter(value.abs())

        # self.bias = _nn.Parameter(_nn.Parameter(_torch.ones(self.num_output)) * -0.1)

        self.pdo = p
        if p > 0:
            self.dropout = dropoutf(p=p)
        else:
            self.dropout = _nn.Identity()

        self.activation = _activity(activation, act_kwargs)

    def forward(self, x, mask_input=None):

        x = self.dropout(x)
        x = _silence(x, mask_input=mask_input)
        key = spmm((self.row, self.col), self.Wk, self.num_output, self.num_input, x.T).T
        query = spmm((self.row, self.col), self.Wq, self.num_output, self.num_input, x.T).T
        x = spmm((self.row, self.col), self.Wv, self.num_output, self.num_input, x.T).T

        # key = self.activation(key)
        # query = self.activation(query)
        # x = (mulifavail(self.mapper.matmul(x), y).T / self.norm_factor).T

        x = self.activation(x)
        # return query, key, query
        return x, key, query


class JRAttentionEncoder(SparseAttentionEncoder):
    """
    Documentation for JRAttentionEncoder
    """

    def __init__(self, mask, prior_map=None, encoder=SparseAttentionEncoder, **kwargs):
        super().__init__(mask, **kwargs)

        if prior_map and (prior_map is not None):
            if isinstance(prior_map, dict):
                self.context = _nn.ModuleDict({name: encoder(prior, **kwargs) for name, prior in prior_map.items()})
            elif isinstance(prior_map, (tuple, list, set)):
                self.context = _nn.ModuleDict({name: encoder(mask, **kwargs) for name in prior_map})
        else:
            self.context = {}

    def forward(self, x, priors=None, mask_input=None):

        if (priors is None) or (priors not in self.context):
            x, key, query = super().forward(x, mask_input=mask_input)
            # norm_factor = self.norm_factor
        else:
            x, key, query = self.context[priors](x, mask_input=mask_input)
            # norm_factor = self.context[priors].norm_factor

        return x, key, query


class PeakAttention(_nn.Module):

    """
    Expects a peak 2 gene map with genes on columns and peaks on rows.

    """

    def __init__(self, mapper_structure, p=0.0, dropoutf=_DROPOUT, device=None, dtype=_DTYPE, activation=None, act_kwargs={}, factory_kwargs={}, **kwargs):
        factory_kwargs = {**factory_kwargs, 'device': device, 'dtype': dtype}
        super().__init__()

        row, col, value, norm_factor, nrows, ncols = fix_sparse_mask(mapper_structure, **factory_kwargs)
        # self.num_output = int(_torch.max(row) + 1)
        # self.num_input = int(_torch.max(col) + 1)
        self.num_output = nrows
        self.num_input = ncols

        # self.register_buffer('norm_factor', norm_factor ** 0.5)
        self.register_buffer('row', row)
        self.register_buffer('col', col)
        self.Wa = _nn.Parameter(value.abs())
        # self.Wa.data.zero_()
        # self.bias = _nn.Parameter(_nn.Parameter(_torch.ones(self.num_output)) * -0.1)

        # act_kwargs = {'dim': 1, **act_kwargs}
        self.softmax = _activity('Softmax', {'dim': 1})
        self.activation = _activity(activation, act_kwargs)

        self.pdo = p
        if p > 0:
            self.dropout = dropoutf(p=p)
        else:
            self.dropout = _nn.Identity()

    def forward(self, x, y=None, mask_input=None):

        x = self.dropout(x)
        # x = silence(x, mask_input=mask_input)
        x = spmm((self.row, self.col), self.Wa, self.num_output, self.num_input, x.T).T
        # x = self.softmax(((mulifavail(x, y) + self.bias) / self.norm_factor))
        # x = self.softmax(((mulifavail(x, y)) / self.norm_factor))
        # x = self.softmax(mulifavail(x, y))
        # x = self.softmax(mulifavail(x, y=None))
        # x = self.softmax(x)
        x = self.activation(x)
        # x = self.softmax(x)
        return x


class G2P2G(_nn.Module):
    """
    Documentation for G2P2G

    """
    def __init__(self, mapper_structure, p=0.0, dropoutf=_DROPOUT, device=None, dtype=_DTYPE, activation=None, act_kwargs={}, factory_kwargs={}, **kwargs):
        factory_kwargs = {**factory_kwargs, 'device': device, 'dtype': dtype}
        super().__init__()

        row, col, value, norm_factor, nrows, ncols = fix_sparse_mask(mapper_structure, **factory_kwargs)
        self.num_output = nrows
        self.num_input = ncols

        # self.register_buffer('norm_factor', norm_factor ** 0.5)
        self.register_buffer('row', row)
        self.register_buffer('col', col)
        self.Wi = _nn.Parameter(value.abs() / self.num_input**0.5)
        self.Wo = _nn.Parameter(value.abs() / self.num_output**0.5)

        self.activation = _activity(activation, act_kwargs)

        self.pdo = p
        if p > 0:
            self.dropout = dropoutf(p=p)
        else:
            self.dropout = _nn.Identity()

    def LA(self, x, y=None, output=None, **kwargs):

        x = spmm((self.row, self.col), self.Wi, self.num_output, self.num_input, x.T).T

        x = self.activation(x)
        x = mulifavail(x, y)

        if output == 'Wi':
            return x

        x = spmm((self.col, self.row), self.Wo, self.num_input, self.num_output, x.T).T
        x = self.activation(x)
        return x

    def forward(self, x, y=None, mask_input=None):

        # x = self.dropout(x)
        x = spmm((self.row, self.col), self.Wi, self.num_output, self.num_input, x.T).T

        x = mulifavail(x, y)
        x = self.activation(x)

        x = self.dropout(x)
        x = spmm((self.col, self.row), self.Wo, self.num_input, self.num_output, x.T).T
        x = self.activation(x)
        return x


class LocalAttentionWeighting(_nn.Module):
    """
    Documentation for LocalAttentionWeighting, Minhyeok 2022.

    """

    def __init__(self, dim, bias=False, device=None, dtype=_DTYPE):

        # factory_kwargs = {'device': device, 'dtype': dtype}
        super().__init__()

        self.S = _torch.tensor(dim, dtype=int)
        self.vp = _nn.Parameter(_torch.empty(dim, dtype=dtype))
        self.Wp = _nn.Linear(in_features=dim, out_features=dim, bias=bias, dtype=dtype)
        self.tanh = _activity('Tanh')
        act_kwargs = {'dim': 0}
        self.softmax = _activity('Softmax', act_kwargs)

    # def _apply(self, fn):
    #     super()._apply(fn)
    #     self.encoder.mapper = fn(self.encoder.mapper)
    #     return self

    def forward(self, x):

        x = self.Wp(x)
        x = self.S * (self.tanh(x).T @ self.vp).T
        x = (- (self.S - x) / (2 * self.S**2)).exp()
        return x
