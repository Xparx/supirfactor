import torch as _torch
from torch import nn as _nn
# import numpy as _np
# from torch.fx import wrap as _fxwrap
# from .. import nn as _sfnn
import torch.nn.utils.prune as _prune
from .. import _DTYPE
# from ..setup import shuffle2d as _randomize
from .utils import fix_mask, _DROPOUT, activity as _activity, silence as _silence


class Encoder(_nn.Module):
    """
    Expects a prior mask with TFs on columns and genes on rows.
    """

    def __init__(self, mask, p=0.0, bias=False, activation=None, dtype=_DTYPE, act_kwargs={}, set_prior_as_weights=False):
        super().__init__()

        mask = mask.T.copy()

        self.register_buffer('prior_mask', fix_mask(mask, dtype=dtype, weighted=set_prior_as_weights))

        output_shape, input_shape = mask.shape

        self.pdo = p
        if p > 0:
            self.dropout = _DROPOUT(p=p)
        else:
            self.dropout = _nn.Identity()

        self.linear = _nn.Linear(in_features=input_shape, out_features=output_shape, bias=bias)
        if bias:
            self.linear.bias.data.zero_()
        # self.linear.weight.data = self.linear.weight.data.abs()
        self.linear.weight.data = self.linear.weight.data.mul(self.prior_mask.bool()).abs()
        if set_prior_as_weights:
            # Somehow this is not working for the weight after pruning. There is a lag to get weights from weights_orig until training has occured.
            self.linear.weight.data = self.prior_mask.data.detach()
            # self.linear.weight_orig.data = self.prior_mask.data.detach()

        _prune.custom_from_mask(self.linear, 'weight', self.prior_mask)
        # self.linear.weight.data[self.linear.weight.data == -0] = 0

        self.activation = _activity(activation, act_kwargs)

    def forward(self, x, mask_input=None):

        x = self.dropout(x)
        x = _silence(x, mask_input=mask_input)
        x = self.linear(x)
        x = self.activation(x)

        return x


class JREncoder(Encoder):
    """
    The joint representation encoder for SupirFactor.
    creates a task specific encoder as a dictionary where
    batches are distributed to the specific task based on supplied key.
    """

    def __init__(self, mask, prior_map=None, encoder=Encoder, **kwargs):
        super().__init__(mask, **kwargs)

        if prior_map and (prior_map is not None):
            if isinstance(prior_map, dict):
                self.context = _nn.ModuleDict({name: encoder(prior, **kwargs) for name, prior in prior_map.items()})
            elif isinstance(prior_map, (tuple, list, set)):
                self.context = _nn.ModuleDict({name: encoder(mask, **kwargs) for name in prior_map})
        else:
            self.context = {}

    def forward(self, x, priors=None, mask_input=None):

        if (priors is None) or (priors not in self.context):
            x = super().forward(x, mask_input=mask_input)
        else:
            x = self.context[priors](x, mask_input=mask_input)

        return x


class Decoder(_nn.Module):
    """
    SupirFactor output layer.

    """

    def __init__(self, output_shape=None, input_shape=None, bias=False, p=0.0, activation=None, mask=None, act_kwargs={}, dtype=_DTYPE):
        super().__init__()

        bias = False

        # if (output_shape is None) or (input_width is None):

        self.register_buffer('grn_mask', fix_mask(mask, dtype=dtype))
        self.pdo = p
        if p > 0:
            self.dropout = _DROPOUT(p=p)
        else:
            self.dropout = _nn.Identity()

        self.linear = _nn.Linear(in_features=input_shape, out_features=output_shape, bias=bias)
        if bias:
            self.linear.bias.data.zero_()

        self.linear.weight.data = self.linear.weight.data.abs()

        self.activation = _activity(activation, act_kwargs)

        if self.grn_mask is not None:
            self.linear.weight.data = self.linear.weight.data.mul(self.grn_mask)
            self.linear.weight.data[self.linear.weight.data == -0] = 0
            _prune.custom_from_mask(self.linear, 'weight', self.grn_mask)
        else:
            _prune.custom_from_mask(self.linear, 'weight', _torch.ones_like(self.linear.weight.data))

    def forward(self, x, mask_input=None):

        x = self.dropout(x)
        x = _silence(x, mask_input=mask_input)
        x = self.linear(x)
        x = self.activation(x)

        return x


class Intermediate(_nn.Module):
    """
    Intermediate block components of SupirFactor.
    """

    def __init__(self, output_shape=None, input_shape=None, bias=False, p=0.0, activation=None, act_kwargs={}, mask=None, dtype=_DTYPE):
        super().__init__()

        # self.register_buffer('tftf_mask', fix_mask(mask, dtype=dtype))
        # bias = False

        if p > 0:
            self.dropout = _DROPOUT(p=p)
        else:
            self.dropout = _nn.Identity()

        self.linear = _nn.Linear(in_features=input_shape, out_features=output_shape, bias=bias)
        if bias:
            self.linear.bias.data.zero_()

        self.activation = _activity(activation, act_kwargs)

        if mask is None:
            _prune.custom_from_mask(self.linear, 'weight', _torch.ones_like(self.linear.weight.data))
        else:
            _prune.custom_from_mask(self.linear, 'weight', mask)

    def forward(self, x, mask_input=None):

        x = self.dropout(x)
        x = _silence(x, mask_input=mask_input)
        x = self.linear(x)
        x = self.activation(x)

        return x


class ResBlock(_nn.Module):
    """Documentation for ResBlock.

    """
    def __init__(self, shape, depth=3, bias=False, activation=None, act_kwargs={}, **kwargs):
        super().__init__()
        self.depth = depth

        self.block = _nn.ModuleList([_nn.Linear(in_features=shape,
                                                out_features=shape, bias=bias)
                                     for i in range(depth)])

        self.activation = _activity(activation, act_kwargs)
        self.dropout = _DROPOUT(p=25)

    def forward(self, x):

        xx = x
        for i, layer in enumerate(self.block):
            x = self.dropout(x)
            x = layer(x)
            x = self.activation(x)

        return xx + x


class ResBottle(_nn.Module):
    """Documentation for ResBlock.

    """
    def __init__(self, shape, bottle_size=10, bias=False, activation=None, act_kwargs={}, **kwargs):
        super().__init__()

        self.block = _nn.ModuleList([_nn.Linear(in_features=shape,
                                                out_features=shape // bottle_size, bias=bias),
                                     _nn.Linear(in_features=shape // bottle_size,
                                                out_features=shape, bias=bias)])

        self.activation = _activity(activation, act_kwargs)

        self.dropout = _DROPOUT(p=0.25)

    def forward(self, x):

        xx = x
        for i, layer in enumerate(self.block):
            x = self.dropout(x)
            x = layer(x)
            x = self.activation(x)

        x = self.activation(xx - x)

        return x


class Decay(_nn.Module):
    """NOT READY.

    """
    def __init__(self, in_features, activation='ReLU', act_kwargs={}):
        super().__init__()

        self.weight = _nn.Parameter(_torch.Tensor(in_features))
        self.weight.data.fill_(0.01)
        _prune.custom_from_mask(self, 'weight', _torch.ones_like(self.weight.data))  # For simplicity.

        self.activation = _activity(activation, act_kwargs)

    def forward(self, x):

        x = -x
        x = self.weight.mul(x)
        x = self.activation(x)

        return x


class LuongBlock(_nn.Module):
    """Documentation for LuongAttention

    """
    def __init__(self, self_dim, p=0.0, n_heads=None, dropoutf=_DROPOUT, bias=False,
                 activation=None, act_kwargs={}):
        super().__init__()

        out_features = n_heads if n_heads is not None else self_dim
        self.bilinear = _nn.Bilinear(self_dim, self_dim, out_features, bias=bias)
        self.bilinear.weight.data = _torch.zeros_like(self.bilinear.weight.data)
        self.softmax = _activity('Softmax', act_kwargs={'dim': 1})

        self.Wc1 = _nn.Linear(self_dim, self_dim, bias=False)
        self.Wc1.weight.data = _torch.zeros_like(self.Wc1.weight.data)
        self.Wc2 = _nn.Linear(out_features, self_dim, bias=bias)
        self.Wc2.weight.data = _torch.zeros_like(self.Wc2.weight.data)
        self.tanh = _activity('Tanh', {})

        self.pdo = p
        if p > 0:
            self.dropout = dropoutf(p=p)
        else:
            self.dropout = _nn.Identity()
        self.activation = _activity(activation, act_kwargs)

    def score_fun(self, x):

        x = self.bilinear(x, x)
        return self.softmax(x)

    def attention(self, x, value):

        x = self.Wc1(value) + self.Wc2(x)
        return self.tanh(x)

    def forward(self, x, mask_input=None):

        x = self.dropout(x)
        x = _silence(x, mask_input=mask_input)
        value = x
        x = self.score_fun(x)
        x = self.attention(x, value)

        return self.activation(value + x)


class DotAttention(_nn.Module):
    """
    Documentation for DotAttention

    """
    def __init__(self, self_dim, p=0.0, dropoutf=_DROPOUT, bias=False,
                 activation=None, act_kwargs={}):
        super().__init__()

        self.n_correction = self_dim ** 0.5

        # self.softmax = _activity('Softmax', act_kwargs={'dim': 0})
        # self.softmax = _activity('Softmax', act_kwargs={})
        self.activation = _activity(activation, act_kwargs)

    def score(self, x, key, query):
        # return self.softmax(query.T @ key / self.n_correction)
        # return self.softmax(query.T @ key)
        # return _torch.heaviside(query.T @ key, _torch.tensor(0.0))
        return self.activation(query.T @ key)

    def forward(self, x, mask_input=None):

        (x, key, query) = x

        key = _silence(key, mask_input=mask_input)
        query = _silence(query, mask_input=mask_input)
        x = _silence(x, mask_input=mask_input)

        scores = self.score(x, key, query)
        att = x @ scores
        return att


class DotWeightedAttention(_nn.Module):
    """
    Documentation for DotWeightedAttention
    """

    def __init__(self, self_dim, p=0.0,
                 dropoutf=_DROPOUT, bias=False,
                 activation=None, act_kwargs={}):
        super().__init__()

        self.n_correction = self_dim ** 0.5

        self.linear = _nn.Linear(self_dim, self_dim, bias=bias)
        # self.linear.weight.data = self.linear.weight.data.abs()
        # self.linear.weight.data = _torch.eye(self_dim)
        # self.softmax = _activity('Softmax', act_kwargs={'dim': 0})
        self.softmax = _activity('Sigmoid')
        self.activation = _activity(activation, act_kwargs)

        self.pdo = p
        if p > 0:
            self.dropout = dropoutf(p=p)
        else:
            self.dropout = _nn.Identity()

    def score(self, x, key, query):

        # key = self.linear(key)
        return self.softmax(query.T @ key)

    def forward(self, x, mask_input=None):

        (x, key, query) = x

        key = self.dropout(key)
        key = _silence(key, mask_input=mask_input)

        query = self.dropout(query)
        query = _silence(query, mask_input=mask_input)

        # x = self.dropout(x)
        x = _silence(x, mask_input=mask_input)

        scores = self.score(x, key, query)
        att = x @ scores
        return self.activation(x + att)


class hNLPCA(_nn.Module):
    """
    Documentation for hNLPCA

    """
    def __init__(self, input_shape=10, bottle_size=5,
                 bias=False,
                 activation=None,
                 act_kwargs={},
                 ):
        super().__init__()

        self.encode = _nn.Linear(in_features=input_shape, out_features=bottle_size, bias=bias)

        self.decode = _nn.Linear(out_features=input_shape, in_features=bottle_size, bias=bias)

        self.activation = _activity(activation, act_kwargs)
        self.bottle_activation = _activity('Sigmoid', act_kwargs={})

        self.n_factors = bottle_size
        # self._default_mask = _torch.zeros(self.n_factors)
        self.register_buffer('default_mask', self._hierarchic_idx(self.n_factors))

    def _hierarchic_idx(k):
        idx = _torch.zeros((k, (k + 1)))
        for i in range(k):
            idx[0:i + 1, i] = 1

        if k > 1:
            idx[1:k, -1] = 1  # for zero mean in component one

        return idx

    # def h_out(self, x):

    #     x = self.encode(x)
    #     x = self.bottle_activation(x)

    #     for i in range(self.n_factors):

    #         x = self.decode(x)
    #         x = self.activation(x)
    def _mask(self, x, i=-2):

        return x * self.default_mask[:, i]

    def forward(self, x, mask=-2):

        x = self.encode(x)
        x = self.bottle_activation(x)
        x = self._mask(x, i=mask)
        x = self.decode(x)
        x = self.activation(x)

        return x
