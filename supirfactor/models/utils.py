import torch as _torch
import hashlib as _hashlib
from torch import nn as _nn
import numpy as _np
from torch.fx import wrap as _fxwrap
from .. import nn as _sfnn
from ..setup import shuffle2d as _randomize


_DROPOUT = _nn.Dropout1d


def fix_mask(prior, randomize_mask=False, dtype=_torch.float32, weighted=False):

    if prior is not None:
        if not isinstance(prior, _torch.Tensor):
            prior = prior.copy() if isinstance(prior, _np.ndarray) else prior.values.copy()
            if randomize_mask:
                prior = _randomize(prior)

            if weighted:
                mask = _torch.tensor(prior, dtype=dtype)
            else:
                mask = _torch.tensor(prior.astype(bool).astype(int), dtype=dtype)
        else:
            if weighted:
                mask = _torch.tensor(prior.detach(), dtype=dtype)
            else:
                mask = _torch.tensor(prior.detach().bool().int(), dtype=dtype)
    else:
        mask = None

    return mask


# @_fxwrap
def activity(activation=None, act_kwargs={}):

    if activation is not None:
        activation = (getattr(_sfnn, activation) if isinstance(activation, str) else activation)(**act_kwargs)
    else:
        activation = _nn.Identity()

    return activation


@_fxwrap
def silence(x, mask_input=None):
    """
    Only works in eval mode due to the direct edit of the data
    as to interfer with the compute graph (I think).
    """

    if mask_input is not None:
        x[:, mask_input] = 0

    return x


# @_torch.fx.wrap
@_fxwrap
def _check_match(label1, label2, mask=None):

    if isinstance(label1, str):
        if str(label1) == str(label2):
            return mask
    elif isinstance(label1, (list, set, tuple)):
        if label2 in label1:
            if isinstance(mask, (list, set, tuple)):
                return _np.array(mask)[(_np.array(label1) == label2)][0]
            else:
                return mask
    else:
        return None


@_torch.inference_mode()
def extract_prior_weights(model):

    model = model.eval()
    state = _hashlib.sha1(str(model.state_dict()).encode('utf-8')).hexdigest()[:8]

    model_prior_map = {}
    if hasattr(model.W, 'context'):
        for k, v in model.W.context.items():
            vv = v.linear.weight.data.clone().detach().cpu().numpy()
            vv[vv == -0] = 0
            model_prior_map[k] = vv

    P = model.W.linear.weight.data.clone().detach().cpu().numpy()
    P[P == -0] = 0
    weighted_P = P

    return state, weighted_P, model_prior_map


def add_prior_weights2collection(weighted_P, stats=None, state=None, model_prior_map={}, collection={}):

    if state is not None:

        collection[state] = {}
        collection[state]['prior_map'] = {}
        if stats is not None:
            collection[state]['stats'] = stats

        collection[state]['w_prior'] = weighted_P
    else:
        collection['stats'] = stats
        collection['prior_map'] = {}
        collection['w_prior'] = weighted_P

    for k, v in model_prior_map.items():
        if state is not None:
            collection[state]['prior_map'][k] = v
        else:
            collection['prior_map'][k] = v

    return collection


def aggregate_collection(collection):

    avg_prior = []
    prior_map = {}
    for k, v in collection.items():
        # print(v['w_prior'].max())
        avg_prior.append(v['w_prior'])
        for k, v in v['prior_map'].items():
            if k not in prior_map:
                prior_map[k] = []

            prior_map[k].append(v)

    avg_prior = sum(avg_prior) / len(avg_prior)

    avg_prior_map = {k: sum(v) / len(v) for k, v in prior_map.items()}

    return avg_prior, avg_prior_map
