# from scanpy.preprocessing._normalization import normalize_total
from scanpy.preprocessing._simple import log1p
import scipy as _sp
from scipy.sparse import issparse as _issparse, csr as _csr
from sklearn.utils import check_array as _check_array, sparsefuncs as _sparsefuncs
from typing import Union
import numpy as _np
import torch as _torch
from torch.utils.data import Dataset as _Dataset
from scipy.sparse import (coo_matrix as _coo_matrix,
                          csr_matrix as _csr_matrix,
                          vstack as _vstack)


def _normalize_data(X, after=None, copy=False):

    counts_per_cell = X.sum(1)
    counts = _np.ravel(counts_per_cell)

    X = X.copy() if copy else X
    if issubclass(X.dtype.type, (int, _np.integer)):
        X = X.astype(_np.float32)  # TODO: Check if float64 should be used
    counts = _np.asarray(counts)  # dask doesn't do medians
    after = _np.median(counts[counts > 0], axis=0) if after is None else after
    counts += counts == 0
    counts = counts / after
    if _issparse(X):
        _sparsefuncs.inplace_row_scale(X, 1 / counts)
    else:
        _np.divide(X, counts[:, None], out=X)
    return X


_RESCALE = {_normalize_data: {"after": None, "copy": True}, log1p: {'copy': True}}


def _X_mcv_split(X, rescaler=_RESCALE):

    X = _check_array(X, accept_sparse=['csr', 'csc', 'coo'], force_all_finite=True)

    X_masked, X_target = _binomial(X.copy())
    X_masked = _rescale(X_masked, rescaler=rescaler)
    X_target = _rescale(X_target, rescaler=rescaler)

    return X_masked, X_target


def _rescale(X, rescaler=_RESCALE):

    for scalefun, params in rescaler.items():
        if "sparse" in params and params['sparse']:
            P = params.copy()
            __ = P.pop('sparse', None)
            X.data = scalefun(X.data, **P).copy()
        else:
            X = scalefun(X, **params)

    return X


def _binomial(X, p=0.5):
    """Split the data as in noise2self (https://www.biorxiv.org/content/10.1101/786269v1) into one masked and one target data matrix using binomial splitting strategy.

    :param X: ndarray, csr_matrix
    :returns: X_masked, X_target
    :rtype: ndarray, csr_matrix

    """

    binom = _sp.random.binomial
    if _issparse(X):
        X_target = binom(X.data.astype(int), p)

        X_masked = X.data - X_target

        X_target = _csr.csr_matrix((X_target, X.nonzero()))
        X_masked = _csr.csr_matrix((X_masked, X.nonzero()))

        X_target.eliminate_zeros()
        X_masked.eliminate_zeros()
        X_masked.data = X_masked.data.astype(float)
        X_target.data = X_target.data.astype(float)

    else:
        X_target = _sp.array([])
        for x in X:
            y = (binom(x, p)).reshape((1, -1))
            if X_target.size == 0:
                X_target = y
            else:
                X_target = _sp.append(X_target, y, 0)

        X_masked = X - X_target

        X_masked = X_masked.astype(float)
        X_target = X_target.astype(float)

    return X_masked, X_target


class SparseDataset(_Dataset):
    """
    Custom Dataset class for scipy sparse matrix,
    sourced: https://discuss.pytorch.org/t/dataloader-loads-data-very-slow-on-sparse-tensor/117391

    Example usage:
    from tqdm import tqdm

    X = random(800000, 300, density=0.25)
    # y = np.arange(800000)
    train_ds = SparseDataset(X)
    train_dl = DataLoader(train_ds,
                          batch_size=1024,
                          collate_fn=sparse_batch_collate)

    for x_batch, y_batch in tqdm(train_dl):
        pass
    """

    def __init__(self, data: Union[_np.ndarray, _coo_matrix, _csr_matrix],
                 transform: bool = False):

        # Transform data coo_matrix to csr_matrix for indexing
        if type(data) == _coo_matrix:
            self.data = data.tocsr()
        else:
            self.data = data

        self.transform = transform  # Can be removed

        if self.transform:
            mean, var = _sparsefuncs.mean_variance_axis(self.data, 0)
            self.mean = mean
            std = _np.sqrt(var)
            std[std == 0] = 1
            self.std = std

    def __getitem__(self, index: int):

        if self.transform:
            data = self.data[index] - self.mean
            data /= self.std
            return data
        else:
            return self.data[index]

    def __len__(self):
        return self.data.shape[0]


def sparse_coo_to_tensor(coo: _coo_matrix):
    """
    Transform scipy coo matrix to pytorch sparse tensor. This has
    to be done as a last step if any as iterating and or
    normalizing this type is not trivial.

    """

    if type(coo) != _coo_matrix:
        coo = coo.tocoo()

    values = coo.data
    indices = _np.vstack((coo.row, coo.col))
    shape = coo.shape

    i = _torch.LongTensor(indices)
    v = _torch.FloatTensor(values)
    s = _torch.Size(shape)

    return _torch.sparse.FloatTensor(i, v, s)


def sparse_batch_collate(data_batch: Union[_np.ndarray, _coo_matrix, _csr_matrix]):
    """
    Collate function which to transform scipy coo matrix to pytorch sparse tensor
    """
    # data_batch, targets_batch = zip(*batch)

    if _issparse(data_batch[0]):
        data_batch = _vstack(data_batch)
        # print(data_batch)
        return _torch.FloatTensor(data_batch.A)
    else:
        data_batch = _np.vstack(data_batch)
        return _torch.FloatTensor(data_batch)
